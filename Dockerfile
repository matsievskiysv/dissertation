FROM debian:bullseye AS base

# tex source directory shell be mounted here
WORKDIR /data
VOLUME /data

# set UTF encoding
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 TERM=xterm

# install fonts and basic programs
RUN echo 'deb http://deb.debian.org/debian/ buster contrib' >> /etc/apt/sources.list.d/msfonts.list \
    && echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
    && apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive \
		      apt-get install -qy --no-install-recommends \
		      make \
		      wget \
		      unzip \
		      perl \
		      fonts-liberation \
		      fonts-cmu \
		      fontconfig \
		      ca-certificates \
    && DEBIAN_FRONTEND=noninteractive \
		      apt-get install -qy --no-install-recommends ttf-mscorefonts-installer \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && fc-cache -f -v


FROM base AS vanilla

# configure and run install-tl
# echo 'O\nL\n\n\n\nR\nS\ne\nR\nI\n' for minimal installation
RUN wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz -O install.tar.gz \
    && tar -xf install.tar.gz \
    && find . -maxdepth 1 -iname "install-tl-*" -type d -exec mv {} installer \; \
    && cd installer \
    && echo -n 'O\nL\n\n\n\nR\nI\n' | ./install-tl \
    && luaotfload-tool --update --force \
    && fc-cache -f -v \
    && cd .. \
    && rm -rf installer install.tar.gz \
    && tlmgr init-usertree

FROM vanilla AS pscyr

# # cannot use installer with sh
RUN wget https://github.com/AndreyAkinshin/Russian-Phd-LaTeX-Dissertation-Template/raw/master/PSCyr/pscyr0.4d.zip -O pscyr.zip \
    && unzip pscyr.zip \
    && cd pscyr \
    && TEXMF=$(kpsewhich -expand-var='$TEXMFMAIN') \
    && mkdir -p $TEXMF/dvips \
    && mv -t $TEXMF/dvips dvips/* \
    && mkdir -p $TEXMF/tex/latex/pscyr \
    && mv -t $TEXMF/tex/latex/pscyr tex/latex/pscyr/* \
    && mkdir -p $TEXMF/fonts/tfm/public/pscyr \
    && mv -t $TEXMF/fonts/tfm/public/pscyr fonts/tfm/public/pscyr/* \
    && mkdir -p $TEXMF/fonts/vf/public/pscyr \
    && mv -t $TEXMF/fonts/vf/public/pscyr fonts/vf/public/pscyr/* \
    && mkdir -p $TEXMF/fonts/type1/public/pscyr \
    && mv -t $TEXMF/fonts/type1/public/pscyr fonts/type1/public/pscyr/* \
    && mkdir -p $TEXMF/fonts/afm/public/pscyr \
    && mv -t $TEXMF/fonts/afm/public/pscyr fonts/afm/public/pscyr/* \
    && mkdir -p $TEXMF/doc/fonts/pscyr \
    && mv -t $TEXMF/doc/fonts/pscyr LICENSE doc/README.koi doc/PROBLEMS \
    && VARTEXFONTS=$(kpsewhich -expand-var='$VARTEXFONTS') \
    && rm -f $VARTEXFONTS/pk/modeless/public/pscyr/* \
    && mktexlsr \
    && rm -rf *

# USER 1000:1000 # does not work with lualatex
ENTRYPOINT ["make"]
