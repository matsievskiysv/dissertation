В \underline{\textbf{третьей главе}} исследуется гибридный ЛУЭ,
состоящий из секций на~СВ и~БВ~\cite{authorConn}.
Рассмотрена схема питания ЛУЭ с секцией на~БВ с отрицательной дисперсией~(\mbox{КДВ-М}),
реализованная при помощи аттенюатора~(\cref{fig:hyb_power-att}) или направленного
ответвителя~(\cref{fig:hyb_power-bridge}).
%
\begin{figure}[!htb]
	\centerfloat{
		\hfill
		\subcaptionbox{Схема с аттенюатором\label{fig:hyb_power-att}}{%
                        \ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{scheme-att-syn}}{}%
			\input{images/scheme/hybrid/scheme-att-syn.tikz}}
		\hfill
		\subcaptionbox{Схема с направленным ответвителем\label{fig:hyb_power-bridge}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{scheme-bridge-syn}}{}%
				\input{images/scheme/hybrid/scheme-bridge-syn.tikz}}
		\hfill
        }
	\legend{
		1 "--- СВЧ генератор,
		2 "--- секция на~БВ,
		3 "--- поглощающая нагрузка,
		4 "--- волноводный мост,
		5 "--- секция на~СВ,
		6 "--- аттенюатор
	}
	\caption{Схема питания гибридного ЛУЭ с секцией на~БВ с отрицательной дисперсией}
\end{figure}

На основе анализа баланса энергий и мощностей в секциях ускорителя в диапазоне токов пучка
\SIrange{0.1}{0.5}{\A} при помощи МЭС определены оптимальные длины
секций~(\cref{fig:st_vs_tr}) и отражение от входа УС~(\cref{fig:s11_vs_n}).
Определены оптимальные длины секций, при которых не требуется использования специальной развязки
генератора с УС; найдены длительности импульсов тока пучка, при которых КПД гибридного ЛУЭ, с учётом
задержки импульса тока, больше КПД ускорителей со сходными параметрами на~СВ и~БВ\@.
%
\begin{figure}[!htb]
	\centerfloat{
		\hfill
		\subcaptionbox{Оптимальные количества ячеек в секциях на~СВ и~БВ\label{fig:st_vs_tr}}{%
                        \ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_st_vs_tr-syn}}{}%
                        \input{images/plot/hybrid/sections_tr_st_vs_tr_syn.tikz}}
		\hfill
		\subcaptionbox{Модуль коэффициента отражения от секций\label{fig:s11_vs_n}}{%
                                        \ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{conn_s11-syn}}{}%
                                        \input{images/plot/hybrid/conn_s11_syn.tikz}}
		\hfill
	}
	\caption{Зависимости параметров гибридного ЛУЭ в зависимости от количества ячеек в секции
                на~СВ}
\end{figure}

Для импульсного тока пучка~\SI{0.1}{\A} при энергии на выходе~\SI{10}{\MeV}
проведены моделирование и оптимизация характеристик трёхэлектродной электронной пушки,
группирователя на~СВ~(\cref{fig:sec-group}) и секции на~БВ~(\cref{fig:sec-tr}).
Проведено экспериментальное исследование макета группирователя на~СВ\@.
%
\begin{figure}[!htb]
	\centerfloat{
		\hfill
		\subcaptionbox{Группирователь\label{fig:sec-group}}{%
			\includegraphics[height=0.22\textheight]{model/buncher_full_power}}
		\hfill
		\subcaptionbox{Секция на~БВ\label{fig:sec-tr}}{%
			\includegraphics[height=0.22\textheight]{model/dlsm_3_hybrid}}
		\hfill
	}
	\legend{Модель секции на~БВ ограничена тремя ячейками}
	\caption{Сечения моделей оптимизированных секций}
\end{figure}

Ввод мощности в секции на~СВ и~БВ рассчитывался с использованием МЭС\@.
Для секции на~БВ предварительная настройка ячеек ТТВ была
выполнена при помощи аналитических выражений с последующим уточнением при помощи
МКЭ~\cite{authorscopHyb,authorscopHyb2}.
Предварительные результаты оказались близки к оптимальным значениям, что позволяет использовать
разработанную программу для ускорения расчёта характеристик секций на~БВ\@.

% \begin{figure}[!htb]
% 	\centerfloat{%
% 		\includegraphics[width=0.8\linewidth]{plot/hybrid/kdvm_tune}
% 	}
% 	\caption{Зависимость модуля коэффициента отражения ячейки ТТВ от
%           радиуса \texttt{R} и ширины окна связи \texttt{W}\label{fig:hyb/sec/tr/tune_map}}
% \end{figure}

Расчёт динамики частиц в электронной пушке~\cite{authorscopGun} был произведён при помощи программы
\textsc{egun}~\cite{egun}, секции на~СВ и~БВ рассчитывались при помощи программ
семейства \textsc{Beamdulac}~\cite{beamd}.
В таблице приведены параметры Твисса, импульсный ток и средняя энергия пучка
электронов на выходе из секций ускорителя~\cite{authorscopGun, authorscopHyb, authorscopHyb2}.
%
\begin{table}[!hbt]
	\centerfloat{%
                \begin{threeparttable}
                        \captionsetup{labelformat=empty}
                        \caption{Параметры ускоряемого пучка на выходе из секций}
                        \begin{tabular}{lS[table-parse-only]S[table-parse-only]S[table-parse-only]}
                          \toprule
                          \multicolumn{1}{c}{Параметр}                 & Электронная~пушка & Секция~СВ & Секция~БВ \\ \midrule
                          \(\alpha\)                                   & -0.97             & -0,49     & -0,57 \\
                          \(\beta, \si{\cm\per\radian}\)      & 6.45              & 3,46      & 28,6  \\
                          \(\epsilon, \si{\cm\milli\radian}\) & 16.2              & 6,2       & 2,3   \\
                          \(I, \si{\A}\)                          & 0.25              & 0,1       & 0,1   \\
                          \(W, \si{\MeV}\)               & 0.03              & 0,96      & 9,98  \\
                          \bottomrule
                        \end{tabular}
                \end{threeparttable}
	}
\end{table}

Приведены результаты измерения и настройки макета трёхячеечного группирователя~(\cref{fig:measure})~\cite{authorwosMeasure,authorwosMeasure2,authorscopMeasure}.
Описана разработанная программа \textsc{beadpull} для измерительного комплекса \texttt{ПИРС}.
%
\begin{figure}[!htb]
	\centerfloat{%
		\hfill
		\subcaptionbox{В сборе}{%
			\includegraphics[height=0.2\textheight]{measure/assembled}}
		\hfill
		\subcaptionbox{В разобранном виде}{%
			\includegraphics[height=0.2\textheight]{measure/disassembled}}
		\hfill
	}
	\caption{Макет группирователя на~СВ\label{fig:measure}}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../synopsis"
%%% End:
