\section{Распределение электрического поля в секции\label{sec:eq/stationary}}

На \cref{fig:eq/stationary/stationary-struct} изображена секция, состоящая из \(N\)
связанных резонаторов~\cite{knapp}.
В общем виде каждая ячейка имеет комбинированную связь с другими ячейками.
Для УС линейных ускорителей электронов (ЛУЭ) часто хорошим приближением является
наличие связи либо по электрическому, либо по магнитному полю только между соседними ячейками.
ЭС каждой ячейки секции, корме первой и последней, представлена на
\cref{fig:eq/stationary/stationary-scheme}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{УС из N ячеек\label{fig:eq/stationary/stationary-struct}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{stationary_struct}}{}%
			\input{images/scheme/eq/stationary_struct.tikz}
		}
		\hfill
		\subcaptionbox{ЭС одной из ячеек\label{fig:eq/stationary/stationary-scheme}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{stationary-dis}}{}%
			\input{images/scheme/eq/stationary-dis.tikz}
		}
		\hfill
	}
	\caption{Ячейки УС\label{fig:eq/stationary/stationary}}
\end{figure}

ЭС каждой ячейки представляет собой \textit{RLC} контур с собственными ёмкостью \(C\),
индуктивностью \(L\) и сопротивлением \(R\).
При наличии подключённых к ячейке УВМ или поглощающей нагрузки,
их учёт производится путём добавления в схему источника напряжения \(U_{\textup{г}}\) с волновым
сопротивлением \(R_{\textup{г}}\) или сопротивления \(R_{\textup{н}}\) соответственно.
Пролетающий по оси секции пучок представляется в виде дополнительного источника напряжения
\(U_{\textup{п}}\).

Связь соседних ячеек между собой представлена в виде ёмкостей связи \(C_i\) и
индуктивностей связи \(M_i\).
Для учёта связи между ячейками через одну, через две и т.д. можно добавить дополнительные элементы
связи.
В данной главе вывод выражений будет представлен в общем виде, при связи каждой ячейки с каждой;
упрощения модели будут упоминаться отдельно.

Применив метод контурных токов для схемы, представленной на
\cref{fig:eq/stationary/stationary-scheme}, можно записать уравнение
%
\begin{multline}\label{eq:eq/stationary/kirchhoff}
	\sum_{n=1}^{N}
	{
	\left(
		j \omega M_n
		-
		\frac{1}{j \omega C_n}
	\right)
	\dot I_n
	}
	+
	\left(
		R + R_{\textup{н}} + R_{\textup{г}}
		+
		j \omega L
		+
		\frac{1}{j \omega C}
	\right)
	\dot I
	=
	\dot U
	+
	\dot U_{\textup{п}}.
\end{multline}
%
Данное уравнение содержит в себе радиотехнические параметры схемы.
Для дальнейшего анализа требуется исключить все радиотехнические параметры,
заменив их электродинамическими.
Этот переход производится при помощи выражений, приведённых ниже~\cite{kaluzni}.

Введём параметр суммарной эквивалентной ёмкости ячейки с учётом ёмкостей связи с
остальными ячейками
%
\begin{equation}\label{eq:eq/stationary/csumm}
	C_\Sigma = {\left[C^{-1} + \sum_{n=1}^{N}{C_n^{-1}}\right]}^{-1}.
\end{equation}
%
Собственные резонансная частота и добротность контура определяются выражениями
%
\begin{subequations}\label{eq:eq/stationary/omegaQ}
	\begin{gather}
		\omega_0 = \frac{1}{\sqrt{L C_\Sigma}}; \\
		Q_0 = \frac{\omega_0 L}{R} =
		\frac{1}{\omega_0 C_\Sigma R} =
		\frac{1}{R} \sqrt{\frac{L}{C_\Sigma}}.
	\end{gather}
\end{subequations}
%
Напряжение от генератора мощностью \(P_{\textup{г}}\) и выходным сопротивлением
\(R_{\textup{г}}\) можно найти из выражения
%
\begin{equation}
	\label{eq:eq/stationary/generat}
	|\dot U_{\textup{г}}| = \sqrt{8 R_{\textup{г}} P_{\textup{г}}}.
\end{equation}
%
Коэффициенты связи секции с генератором и нагрузкой можно записать в виде
%
\begin{equation}\label{eq:eq/stationary/coup}
	\chi_{\textup{г}} = \frac{R_{\textup{г}}}{R};\;
	\chi_{\textup{н}} = \frac{R_{\textup{н}}}{R},
\end{equation}
%
где \(R_{\textup{г}}\) и \(R_{\textup{н}}\) "--- выходное сопротивление генератора и входное сопротивление
нагрузки соответственно.
\(R\) "--- омические потери в ячейке секции~\cite{sobenin-tech}.

Коэффициенты связи по электрическому и магнитному полям определяются в виде
%
\begin{equation}\label{eq:eq/stationary/coupeh}
	K^E_{m;n} = 2 \frac{\sqrt{C_{\Sigma m} C_{\Sigma n}}}{C_{m;n}};\;
	K^H_{m;n} = 2 \frac{M_{m;n}}{\sqrt{L_m L_n}},
\end{equation}
%
где \(C_{m;n}\) и \(M_{m;n}\) "--- ёмкость и индуктивность связи между ячейками \(m\) и \(n\)
соответственно.
Отметим, что приведённое выражение для коэффициента связи по электрическому полю
верно только при условии превалирования собственной ёмкости ячейки над ёмкостями связи
\(C_i \ll C\)~\cite{Atabekov}.

Запишем преобразованное при помощи
выражений~\cref{eq:eq/stationary/omegaQ, eq:eq/stationary/generat,
	eq:eq/stationary/coup, eq:eq/stationary/coupeh}
уравнение~\cref{eq:eq/stationary/kirchhoff} в виде
%
\begin{equation}\label{eq:eq/stationary/base}
	\sum_{n=1}^{N}\left(a_n^E + a_n^H\right) \dot X_n
	+
	a_0 \dot X
	=
	b_{\textup{г}} + b_{\textup{п}}.
\end{equation}
%
Это выражение можно записать в двух представлениях.
Представление с комплексной амплитудой ячеек, выраженной через ёмкость ячеек
%
\begin{equation}\label{eq:eq/stationary/normс}
	\dot X^C = \frac{\dot I}{\sqrt{C_\Sigma}},
\end{equation}
%
будем называть ёмкостным представлением;
представление с комплексной амплитудой ячеек, выраженной через индуктивность ячеек
%
\begin{equation}\label{eq:eq/stationary/norml}
	\dot X^L = \dot I \sqrt{L},
\end{equation}
%
будем называть индуктивным представлением.

В случае ёмкостного представления компоненты уравнения~\cref{eq:eq/stationary/base}
преобразуются к виду
%
\begin{subequations}\label{eq:eq/stationary/c}
	\begin{align}
		a_0            & =
		1
		-
		\frac{f^2}{f_0^2}
		+
		j
		\frac{f}{f_0}
		\frac{1}{Q}
		\left(
			1
			+
			\chi_{\textup{г}}
			+
			\chi_{\textup{н}}
		\right);                                     \\
		a^E_n          & = -\frac{K^E_n}{2};\;
		a^H_n = -\frac{K^H_n}{2}\frac{f^2}{f_n f_0}; \\
		b_{\textup{г}} & =
		j 4 f
		\sqrt{
			\frac{
				\pi P_{\textup{г}} \chi_{\textup{г}}
			}{
				f_0 Q
			}
		}
		\exp {(j \varphi)},
	\end{align}
\end{subequations}
%
в случае индуктивного представления "--- к виду
%
\begin{subequations}\label{eq:eq/stationary/l}
	\begin{align}
		a_0            & =
		1
		-
		\frac{f_0^2}{f^2}
		-
		j
		\frac{f_0}{f}
		\frac{1}{Q}
		\left(
			1
			+
			\chi_{\textup{г}}
			+
			\chi_{\textup{н}}
		\right);                                    \\
		a^H_n          & = \frac{K^H_n}{2};\;
		a^E_n = \frac{K^E_n}{2}\frac{f_0 f_n}{f^2}; \\
		b_{\textup{г}} & =
		-
		j \frac{2}{f}
		\sqrt{
			\frac{
				f_0 P_{\textup{г}} \chi_{\textup{г}}
			}{
				\pi Q
			}
		}
		\exp {(j \varphi)}.
	\end{align}
\end{subequations}

Для ёмкостного представления можно записать отношение шунтового сопротивления резонатора к
добротности в виде~\cite{KaluznyDD}
%
\begin{equation}\label{eq:eq/stationary/rsh-c}
	\frac{R_{\textup{ш}}}{Q} = \frac{U^2}{\omega_0 W} = \frac{2 C_\Sigma}{\omega_0 C^2},
\end{equation}
%
для индуктивного представления "--- в виде
%
\begin{equation}\label{eq:eq/stationary/rsh-l}
	\frac{R_{\textup{ш}}}{Q} = \frac{U^2}{\omega_0 W} = \frac{2}{\omega_0 L}.
\end{equation}

С использованием~\cref{eq:eq/stationary/c,eq:eq/stationary/rsh-c}
для ёмкостного представления и
\cref{eq:eq/stationary/l,eq:eq/stationary/rsh-l} для индуктивного представления
можно записать выражение связи комплексных
амплитуд \(\dot X\) с амплитудами ускоряющего электрического поля \(\dot U\) в ячейках
%
\begin{equation}\label{eq:eq/stationary/XtoU}
	\dot U = -j \frac{1}{2} \sqrt{\frac{1}{\pi}\frac{f_0}{f^2}\frac{R_{\textup{ш}}}{Q}} \dot X^C =
	j 2 \sqrt{\frac{\pi f^2 Q}{f_0^2 R_{\textup{ш}}}} \dot X^L.
\end{equation}

% Выражение \(b_{\textup{п}}\) уравнения~\cref{eq:eq/stationary/base} рассмотрено ниже в данном
% параграфе.

Систему уравнений, описывающую всю ускоряющую секцию, можно представить в матричной форме
%
\begin{equation}\label{eq:eq/stationary/matrix}
	[A][\dot x]=[b].
\end{equation}
%
Здесь матрица \([A]\) характеризует параметры секции.
Диагональные элементы матрицы \(a_{i;i}\) описывают отдельно взятые ячейки секции и их связь с
подводящим генератором или поглощающей нагрузкой;
связь между ячейками \(i\) и \(j\) определяется элементами \(a_{i;j}, i \ne j\);
вектор \([b]\) характеризует вынуждающие воздействия на устройство: ввод мощности и нагрузку
током.

Для секций с большой фазовой скоростью и учётом связи только между соседними ячейками
матрица \([A]\) будет трёхдиагональной.
Для решения такой системы уравнений можно воспользоваться аналитической рекурсивной формулой
нахождения обратной матрицы~\cite{USMANI1994413}
%
\begin{equation*}
	{(T^{-1})}_{ij}
	=
	\begin{cases}
		{(-1)}^{i+j} b_i \ldots b_{j-1} \frac{d_{j+1} \ldots d_n}{\sigma_i \ldots \sigma_n}
		, i \le j; \\
		{(-1)}^{i+j} c_i \ldots c_{j-1} \frac{d_{j+1} \ldots d_n}{\sigma_i \ldots \sigma_n}
		, i > j,
	\end{cases}
\end{equation*}
%
где итерационные коэффициенты вычисляются по формулам
%
\begin{align*}
	\sigma_i   & = \frac{\theta_i}{\theta_{i-1}};    \\
	d_i        & = \frac{\phi_i}{\phi_{i+1}};        \\
	\theta_0   & = 1, ~ \theta_1 = a_1, ~\theta_i =
	a_i \theta_{i-1} - b_{i-1} c_{i-1} \theta_{i-2}; \\
	\phi_{n+1} & = 1, ~ \phi_n = a_n, ~ \phi =
	a_i \phi_{i+1} - b_i c_i \phi_{i+2}.
\end{align*}
%
Используя численные методы решения СЛАУ, можно найти вектор \([\dot x]\) без введения
дополнительных ограничений на вид матрицы \([A]\)~\cite{lapac}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
