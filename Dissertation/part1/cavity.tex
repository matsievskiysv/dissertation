\section{Нагрузка резонатора ускоряемым пучком\label{sec:eq/cav}}

Для одиночных резонаторов и секций на~СВ важном является определение оптимальных значений
подводимой мощности и связи с генератором.
На \cref{fig:eq/cav/scheme-unloaded,fig:eq/cav/scheme-loaded} представлены
ЭС подключённого к генератору резонатора без и с нагрузкой током ускоряемого пучка
соответственно.
На \cref{fig:eq/cav/scheme-included} изображена схема с эквивалентно вписанным в контур
резонатора генератором.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Без нагрузки током\label{fig:eq/cav/scheme-unloaded}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{single_cavity}}{}%
			\input{images/scheme/eq/single_cavity.tikz}
		}
		\hfill
		\subcaptionbox{С нагрузкой током\label{fig:eq/cav/scheme-loaded}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{single_cavity_loaded}}{}%
			\input{images/scheme/eq/single_cavity_loaded.tikz}
		}
		\hfill
		\subcaptionbox{С нагрузкой током и эквивалентным генератором\label{fig:eq/cav/scheme-included}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{single_cavity_loaded_included-dis}}{}%
			\input{images/scheme/eq/single_cavity_loaded_included-dis.tikz}
		}
		\hfill
	}
	\caption{ЭС резонатора, подключённого к генератору\label{fig:eq/cav/scheme}}
\end{figure}

Ускоряющее напряжение \(V_{\textup{рез}}\) резонатора равно напряжению на
элементе~\(G\)~\cite{perry}.
Шунтовое сопротивление связано с эквивалентной проводимостью выражением:
\(R_{\textup{ш}}=2/G\).
Наличие коэффициента 2 в выражении пояснено в параграфе~\cref{sec:eq/stationary}.

Основные параметры представленной схемы выражаются следующим образом:
%
\[
	\omega_0 = \sqrt{\frac{1}{LC}};\;
	Q_0 = \frac{1}{G}\sqrt{\frac{C}{L}};\;
	P_{\textup{рез}} = \frac{G V^2}{2},
\]
%
внешнюю добротность можно записать в виде
%
\[Q_{\textup{вн}} = \frac{Q_0}{\beta} = \frac{1}{\beta G}\sqrt{\frac{C}{L}}.\]
%
В отличии от предыдущих параграфов, где коэффициент связи обозначался переменной~\(\chi\),
для обозначения коэффициента связи резонатора используется переменная~\(\beta\).
Таким образом подчёркивается их различие.
Коэффициент связи~\(\beta\) "--- коэффициент связи всей секции с подводящим волноводом;
коэффициент связи~\(\chi\) "--- коэффициент связи ячейки ТТВ с подводящим волноводом.
Эти коэффициенты будут равны только для секции, состоящей из одной ячейки.

С учётом упрощающих условий, указанных в параграфе~\cref{sec:eq/stationary},
генерируемые пучком и генератором напряжения можно записать в виде
%
\begin{subequations}
	\begin{align}
		V_{\textup{г}} & = \frac{i_{\textup{г}}}{G (1+\beta)} =
		\frac{2\sqrt{\beta}}{1+\beta}\sqrt{R_{\textup{ш}} P_{\textup{г}}};\label{eq:eq/cav/Vg}                 \\
		V_{\textup{п}} & = \frac{i_{\textup{п}}}{G (1+\beta)} = \frac{I_{\textup{п}} R_{\textup{ш}}}{1+\beta},
	\end{align}
\end{subequations}
%
где \(I_{\textup{п}}\) "--- импульсный ток пучка.
Передача энергии от генератора пучку будет максимальна,
когда \(V_{\textup{г}}\) и \(V_{\textup{п}}\) находятся в противофазе.
Суперпозиция этих напряжений будет определять напряжение в резонаторе
%
\begin{equation}\label{eq:eq/cav/Vc}
	V_{\textup{рез}}=
	V_{\textup{г}} - V_{\textup{п}} =
	\sqrt{R_{\textup{ш}} P_{\textup{г}}}
	\frac{
		2\sqrt{\beta}
	}{
		1+\beta
	}
	\left(
		1-\frac{K}{\sqrt{\beta}}
	\right),
\end{equation}
%
где \(K=\sqrt{R_{\textup{ш}}/P_{\textup{г}}}I_{\textup{п}}/2\) "---
безразмерный параметр нагруженности резонатора.

Мощность, рассеянную в ячейке, можно найти из выражения~\cref{eq:eq/cav/Vc} при \(I_{\textup{п}}=0\);
отражённая мощность находится из условия баланса мощностей \(P_{\textup{г}}=P_{\textup{п}}+P_{\textup{рез}}+P_{\textup{отр}}\).
Рассеянную, отражённую мощности и КПД резонатора можно определить из выражений
%
\begin{subequations}\label{eq:pow/opt/relation}
	\begin{align}
		P_{\textup{рез}} & = \frac{4 \beta}{(1+\beta)^2}
		\left(1-\frac{K}{\sqrt{\beta}}\right)^2P_{\textup{г}};                              \\
		P_{\textup{отр}} & = P_{\textup{г}} \left(\frac{\beta -1 -2K\sqrt{\beta}}{\beta+1}\right)^2; \\
		\eta             & = \frac{4 K \sqrt{\beta}}{1+\beta}\left(1-\frac{K}{\sqrt{\beta}}\right).
	\end{align}
\end{subequations}

Оптимальным будет режим работы резонатора, при котором максимальная мощность будет передаваться
пучку, а отражённая мощность будет равна нулю.
Такое условие реализуется при коэффициенте связи
%
\begin{equation}\label{eq:pow/opt/optbeta}
	\beta_{\textup{опт}} = \left[\frac{I_{\textup{п}}}{2}\sqrt{\frac{R_{\textup{ш}}}{P_{\textup{г}}}} +
		\sqrt{1 + \frac{I_{\textup{п}}^2}{4}\frac{R_{\textup{ш}}}{P_{\textup{г}}}}\right]^2.
\end{equation}

В случае большой добротности, имеющей место в СП резонаторах,
оптимальные коэффициент связи и входная мощность определяются выражениями~\cite{padamsee2008rf}
%
\begin{subequations}\label{eq:pow/opt/perfect}
	\begin{align}
		\beta_{\textup{опт}} & = \frac{I_{\textup{п}} R_{\textup{ш}}}{V_{\textup{рез}}} + 1; \\
		P_{\textup{г опт}}   & = \frac{V_{\textup{рез}}^2}{R_{\textup{ш}}}
		\left( \frac{I_{\textup{п}} R_{\textup{ш}}}{V_{\textup{рез}}} + 1 \right).
	\end{align}
\end{subequations}

Реальное устройство работает в несколько других условиях.
Синхронная фаза пучка выбирается из соображений продольной устойчивости и группировки
пучка~\cite{LebedevShalnov}.
В то же время резонансная частота СП резонатора подвержена изменениям, связанным с влиянием силы
Лоренца, флуктуациями давления в системе охлаждения, а также микрофонным
эффектом~\cite{padamsee2008rf}.
Для этого случая величина подводимой мощности, необходимой для получения требуемого
ускоряющего градиента, определяется выражением~\cite{PadamseeDesignTopics}
%
\begin{equation}\label{eq:eq/cav/detuned}
	P_{\textup{г}}=\frac{V_{\textup{рез}}^2}{4\sfrac{R}{Q}Q_{\textup{вн}}}\left[
		\left(
			1+\frac{I_{\textup{п}}\sfrac{R}{Q}Q_{\textup{вн}}}{V_{\textup{рез}}}
			\cos{\left(\varphi_{\textup{синх}}\right)}
		\right)^2 +
		\left(
			2 Q_{\textup{вн}}\frac{\Delta f}{f}
		\right)^2
	\right],
\end{equation}
%
где \(\Delta f\) "--- расстройка резонатора относительно номинальной частоты.
%
% Смещение резонансной частоты ячейки из-за индуктивности пучка
% \begin{equation}
% 	\label{eq:eq/cav/df}
% 	\frac{f_{\textup{г}}-f_0}{f_0}=\frac{I_0 R_{\textup{ш}}
% 		\sin{\left(\varphi_{\textup{синх}}\right)}}{2 Q_0 V_{\textup{рез}}},
% \end{equation}
% где \(\varphi_{\textup{синх}}\) "--- синхронная фаза пучка.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
