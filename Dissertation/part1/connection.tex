\section{Соединение секций при помощи волновода\label{sec:eq/waveguide}}

При разработке сложных схем питания УС возможно возникновение необходимости моделирования соединения
секций при помощи волновода.
На \cref{fig:eq/waveguide/sections} изображены возможные схемы каскадного соединения УС при
помощи волновода.
На \cref{fig:eq/waveguide/sections-2tr} изображено соединение двух секций на~БВ при помощи волновода.
Мощность от генератора поступает в первую секцию, и, после её прохождения секции, поступает во
вторую.
Все три части вместе образуют единую высокодисперсную линию передачи.
Данную схему можно использовать для сборки модульной секции на~БВ, состоящей из нескольких участков,
питающихся от одного генератора.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Каскадное соединение УС на~БВ\label{fig:eq/waveguide/sections-2tr}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{two_tr_struct_waveguide}}{}%
			\resizebox{!}{0.12\textheight}{%
				\input{images/scheme/eq/two_tr_struct_waveguide.tikz}
			}
		}
		\hfill
		\subcaptionbox{Каскадное соединение УС на~СВ и БВ\label{fig:eq/waveguide/sections-trst}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{tr_st_struct_waveguide}}{}%
			\resizebox{!}{0.12\textheight}{%
				\input{images/scheme/eq/tr_st_struct_waveguide.tikz}
			}
		}
		\hfill
	}
	\caption{Соединение УС при помощи
		волновода\label{fig:eq/waveguide/sections}}
\end{figure}

На \cref{fig:eq/waveguide/sections-trst} изображены секция на~БВ, подключённая к секции на~СВ
при помощи волновода.
В данной схеме секция на~СВ играет роль узкополосной нагрузки.
Поведение данной схемы будет различно в случае наличия и отсутствия нагрузки пучком.
При согласовании секции на рабочем токе в присутствии нагрузки пучком отражения от секции на~СВ на
рабочей частоте не будет.
В отсутствии нагрузки пучком СВЧ мощность будет отражаться от секции.

Для моделирования таких соединений можно воспользоваться одним из двух методов "--- методом
внесённого эквивалентного сопротивления или методом обобщённого многополюсника~\cite{Silaev}.

% Анализ подобных устройств обычно производится при помощи матричных методов расчётов.
% Однако в этом случае теряется информация о внутреннем состоянии устройства,
% а именно информация о распределении полей.
% Для поручения более точной картины происходящих в устройствах процессов можно
% использовать комбинацию МЭС, матричного метода и метода ориентированных графов~\cite{Silaev}.

\paragraph{Внесение эквивалентного сопротивления}

На \cref{fig:eq/waveguide/thevenin} представлена ЭС волновода в виде четырёхполюсника по
Тевенену-Гельмгольцу~\cite{miano2001transmission}.
Данная схема описывает пассивный четырёхполюсника, абстрагируясь от его внутренней
структуры.
Подобным образом можно описать поведение передающей линии в стационарном
состоянии~\cite{marcuvitz1951waveguide}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{thevenin}}{}%
		\input{images/scheme/eq/thevenin.tikz}
	}
	\caption{ЭС четырёхполюсника по
		Тевенену-Гельмгольцу\label{fig:eq/waveguide/thevenin}}
\end{figure}

Здесь \(R_1\) и \(R_2\) "--- входное и выходное сопротивления четырёхполюсника соответственно,
\(U_1\) и \(U_2\) "--- амплитуды отражённой волы со стороны выхода и падающей волны со стороны
входа соответственно.
Функция, определяющая величину амплитуд \(U_1 = \operatorname{f}_1(U_{\textup{г}})\) и \(U_2 =
\operatorname{f}_2(U_{\textup{г}})\) будет зависеть от свойств подключённых к четырёхполюснику
устройств.

В случае отсутствия во второй секции источников мощности, ЭС упрощается до простого внесения
входного сопротивления второй секции в схему первой секции.
Этот частный случай реализуется при каскадном соединении УС, питающихся от одного генератора.

На \cref{fig:eq/waveguide/stationary_wg} изображена ЭС участка соединения секций
\cref{fig:eq/waveguide/sections}, включающая в себя схемы последней ячейки первой секции,
первой ячейки второй секции и волноводного соединения, изображённого на
\cref{fig:eq/waveguide/thevenin}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{stationary_wg-dis}}{}%
		\input{images/scheme/eq/stationary_wg-dis.tikz}
	}
	\caption{Две ячейки УС, соединённые
		волноводом\label{fig:eq/waveguide/stationary_wg}}
\end{figure}

Здесь \(R^{\textup{В}}_1\) и \(R^{\textup{В}}_2\) "--- входные сопротивления первой и второй секций
соответственно.
Они представляют из себя комплексные сопротивления, определяющиеся
выражениями~\cref{eq:eq/zin/c} или~\cref{eq:eq/zin/l} в зависимости от представления.
Для анализа данной схемы при помощи МЭС требуется определить связь значений \(U_1\) и \(U_2\) с
\(U_{\textup{г}}\).
Для этого можно воспользоваться методом ориентированных графов~\cite{Silaev}.

На \cref{fig:eq/waveguide/graph-init} изображён направленный граф четырёхполюсного
устройства, характеризующегося матрицей рассеяния \([S]\).
Используя правила преобразования ориентированных графов, его можно привести к виду,
позволяющему определить выходной или выходной коэффициенты отражения устройства.
Результаты этих преобразований приведены на
\cref{fig:eq/waveguide/graph-in,fig:eq/waveguide/graph-out} соответственно.
%
\begin{figure}[htbp]
	\centerfloat{%
		\subcaptionbox{Изначальный граф\label{fig:eq/waveguide/graph-init}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{graph_init-dis}}{}%
			\input{images/scheme/eq/graph_init-dis.tikz}
		}
		\hfill
		\subcaptionbox{Граф отражения на входе\label{fig:eq/waveguide/graph-in}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{graph_in}}{}%
			\input{images/scheme/eq/graph_in.tikz}
		}
		\hfill
		\subcaptionbox{Граф отражения на выходе\label{fig:eq/waveguide/graph-out}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{graph_out}}{}%
			\input{images/scheme/eq/graph_out.tikz}
		}
	}
	\caption{Ориентированные графы, описывающие
		четырёхполюсник\label{fig:eq/waveguide/graph}}
\end{figure}

Из анализа ориентированных графов, изображённых на
\cref{fig:eq/waveguide/graph-in,fig:eq/waveguide/graph-out}, можно найти выражения для определения
коэффициентов отражения секции
%
\begin{subequations}
	\begin{align}
		\textup{Г}_{\textup{вх}}  & =
		S_{11} +
		\frac{S_{12}S_{21}\textup{Г}_{\textup{н}}}{1-S_{22}\textup{Г}_{\textup{н}}}; \\
		\textup{Г}_{\textup{вых}} & =
		S_{22} +
		\frac{S_{12}S_{21}\textup{Г}_{\textup{г}}}{1-S_{11}\textup{Г}_{\textup{г}}}.
	\end{align}
\end{subequations}

Метод ориентированных графов позволяет находить амплитуды падающих и отражённых волн и для более
сложных топологий соединений, однако при этом возрастает также и сложность выкладок.

Таким образом, для схем соединения, представленных на \cref{fig:eq/waveguide/sections},
выражения для \(U_1\) и \(U_2\) будут выглядеть следующим образом
%
\begin{subequations}
	\begin{align}
		U_1 & =
		U_2
		\left(
			S_{11}
			\exp{(-L \gamma)} +
			\frac{
			a_2^2 (1-{S_{11}^{II}}^{2})
			\textup{Г}_{\textup{г}}
			\exp{(-L \gamma)}
			}{
			1-S_{11}^{II}
			\textup{Г}_{\textup{г}}
			\exp{(-L \gamma)}
			}
		\right); \\
		U_2 & =
		U_{\textup{г}} a_1 \sqrt{1-{S_{11}^I}^2}
		\exp{(-L \gamma)}.
	\end{align}
\end{subequations}
%
Здесь \(S_{11}^I\) и \(S_{11}^{II}\) "--- рассчитанные по~\cref{eq:eq/zin/s11}
параметры отражения от первой и второй секций соответственно,
\(L\) "--- длина соединяющего волновода, \(\gamma\) "--- комплексный коэффициент распространения
волны в волноводе,
\(a_1\) и \(a_2\) "--- коэффициенты затухания в первой и второй секциях соответственно.
Значения \(a_1\) и \(a_2\) можно привести к потерям в секциях при помощи
выражения~\cref{eq:eq/zin/loss}.

% В случае, когда амплитуды электрического поля в подключаемой секции не представляют интереса,
% и к ней не подключены другие источники мощности, в схеме~\cref{fig:eq/waveguide/thevenin}
% её можно заменить комплексным сопротивлением, равным входному сопротивлению подключаемой секции.

\paragraph{Метод обобщённого многополюсника}

При наличии в схеме питания дополнительных элементов, таких как мостовые соединения, делители
мощности
и др., преобразование направленных графов может быть трудоёмким.
В этом случае удобнее воспользоваться методом обобщённого
многополюсника~\cite{Silaev, kaluzni11}.

Для применения данного метода требуется пронумеровать порты всех устройств схемы по следующему
правилу: \(N\) портов устройств, являющихся выходными портами обобщённого устройства, получают
номер от~1 до~\(N\); \(2 M\) портов устройств, соединённых между собой передающими линиями,
нумеруются от~\(N + 1\) до~\(N + 2 M\); \(K\) портов устройств, нагруженных на комплексное
сопротивление, получают номер от~\(N + 2 M + 1\) до~\(N + 2 M + K\).
На \cref{fig:eq/waveguide/gen_matrix-gen} изображён многополюсник с пронумерованными по
данному правилу портами, на
\cref{fig:eq/waveguide/gen_matrix-ex} изображён пример такой
нумерации для обобщённого многополюсника, состоящего из двух четырёхполюсников и одного
шестиполюсника.
Порты \(a_1\) и \(a_2\) являются внешними для обобщённого устройства;
порты \(a_3\), \(a_6\) и \(a_4\), \(a_5\) попарно соединены передающими линиями;
порт \(a_7\) нагружен на сопротивление.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Схема нумерации портов\label{fig:eq/waveguide/gen_matrix-gen}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{gener_matrix-dis}}{}%
			\input{images/scheme/eq/gener_matrix-dis.tikz}
		}
		\hfill
		\subcaptionbox{Пример многополюсника\label{fig:eq/waveguide/gen_matrix-ex}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{gener_matrix_example}}{}%
			\input{images/scheme/eq/gener_matrix_example.tikz}
		}
		\hfill
	}
	\legend{Угловым штрихом обозначены внешние порты устройства;
		вертикальным "--- порты, соединённые волноводами;
		горизонтальным "--- порты, нагруженные на комплексное~сопротивление}
	\caption{Обобщённый многополюсник\label{fig:eq/waveguide/gen_matrix}}
\end{figure}

Матрица обобщённого многополюсника с пронумерованными по указанному выше правилу портами выглядит
следующим образом
%
\begin{equation*}
	\begin{bmatrix}
		b_1     \\ \vdots \\ b_N \\ \hline
		b_{N+1} \\ \vdots \\ b_{N+2M+K} \\
	\end{bmatrix}
	=
	\left[
		\begin{array}{ccc|ccc}
			S_{1;1} & \cdots & S_{1;N} & S_{1;N+1} & \cdots & S_{1;N+2M+K} \\
			\cdots  & \ddots & \cdots  & \cdots    & \ddots & \cdots       \\
			S_{N;1} & \cdots & S_{N;N} & S_{N;N+1} & \cdots & S_{N;N+2M+K} \\
			\hline
			\cdots  & \ddots & \cdots  & \cdots    & \ddots & \cdots       \\
			\vdots  & \vdots & \vdots  & \vdots    & \vdots & \vdots       \\
			\cdots  & \cdots & \cdots  & \cdots    & \cdots & \cdots       \\
		\end{array}
		\right]
	\begin{bmatrix}
		a_1     \\ \vdots \\ a_N \\ \hline
		a_{N+1} \\ \vdots \\ a_{N+2M+K} \\
	\end{bmatrix},
\end{equation*}
%
где \(a_n\) и \(b_n\) "--- падающая и отражённая волны порта \(p_n\) в некоторой референтной
плоскости.
В клеточном виде матрицу можно записать в виде
%
\begin{equation}\label{eq:eq/waveguide/matrix}
	\begin{bmatrix}
		[B_1] \\
		[B_2]
	\end{bmatrix}
	=
	\begin{bmatrix}
		[S_1] & [S_2] \\
		[S_3] & [S_4] \\
	\end{bmatrix}
	\begin{bmatrix}
		[A_1] \\
		[A_2]
	\end{bmatrix}.
\end{equation}
%
Клетка матрицы \(S_1\) имеет размерность \((N \times N)\), клетки \(S_2\) и \(S_3\) "---
\((N \times 2M+K)\) и \((2M+K \times N)\) соответственно, клетка \(S_4\) "--- \((2M+K \times 2M+K)\).
Элементы матрицы составляются из элементов матриц рассеяния устройств, составляющих
обобщённый многополюсник.

Введём дополнительную матрицу связи \(S_{\textup{св}}\) с размерностью \((2M+K \times 2M+K)\),
характеризующую внутренние для обобщённого устройства связи портов:
соединения передающими линиями и подключённые нагрузки.
Элементы \(S_{ij}\) и \(S_{ji}\) этой матрицы будут характеризовать волноводные соединения и равны
\(\exp{(-\gamma_{ij} L_{ij})}\), где \(i\) и \(j\) "--- номера соединённых передающей линией портов.
Последние \(K\) элементов на главной диагонали будут равны коэффициентам отражения от подключённых
к портам нагрузок \(\textup{Г}_k\).
Данная матрица связывает отражённые и падающие волны последних \(2M+K\) портов в многополюснике
%
\begin{equation}\label{eq:eq/waveguide/Sconn}
	[A_2] = [S_{\textup{св}}][B_2].
\end{equation}
%
Записав выражение~\cref{eq:eq/waveguide/matrix} в виде системы уравнений
%
\begin{equation}\label{eq:eq/waveguide/cases}
	\begin{cases}
		[B_1] = [S_1][A_1] + [S_2][A_2]; \\
		[B_2] = [S_3][A_1] + [S_4][A_2],
	\end{cases}
\end{equation}
%
и, подставив в неё~\cref{eq:eq/waveguide/Sconn}, получаем
%
\begin{equation}\label{eq:eq/waveguide/cases2}
	\begin{cases}
		[B_2] = \left([I] - [S_4][S_{\textup{св}}]\right)^{-1}[S_3][A_1], \\
		[B_1] = \left\{[S_1] + [S_2][S_{\textup{св}}]\left([I] -
				[S_4][S_{\textup{св}}]\right)^{-1}[S_3]\right\}[A_1].
	\end{cases}
\end{equation}
%
Здесь \([B_2]\) характеризует отражённые волны <<внутренних>> портов многополюсника, \([B_1]\) "---
от <<внешних>>.
Соответственно, матрица рассеяния многополюсника определяется выражением
%
\begin{equation}\label{eq:eq/waveguide/smatrix}
	[S] = [S_1] + [S_2][S_{\textup{св}}]\left([I] - [S_4][S_{\textup{св}}]\right)^{-1}[S_3].
\end{equation}

Таким образом, при известных матрицах отражения УС, составляющих устройство, можно найти матрицу
рассеяния этого устройства, падающие и отражённые волны как для внешних портов, так и для
внутренних.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
