\section{Переходные процессы в секции\label{sec:eq/transitional}}

Для моделирования переходных процессов в ЦСР часто применяют преобразование
Лапласа~\cite{kaluzni08, StepnovPHD, ZavorotyloPHD}.
Преимущество данного метода заключается в переходе к алгебраической системе уравнений в комплексной
плоскости, решение которой значительно проще решения системы ОДУ\@.
Однако, для аналитического решения требуется, чтобы исходная матрица
была трёхдиагональной и симметричной~\cite{DAFONSECA20017}.
Численные же методы обратного преобразования Лапласа работают
нестабильно с осциллирующими функциями~\cite{mpmath, cohen2007numerical}.
Ниже описан метод расчёта переходных процессов в секциях ускорителей при помощи прямого решения
системы ОДУ~\cite{authorTransit}.

Уравнение Кирхгофа во временной области для ячейки с комбинированной связью с остальными ячейками
секции выглядит следующим образом
%
\begin{multline}\label{eq:eq/transitional/kirchhoff}
	\sum_{n=1}^{N}
	\left(
		\frac 1{C_{n}} \int {(i - i_n) dt} + M_{n} \frac{di_n}{dt}
	\right)
	+ L_0 \frac{di}{dt} +
	\frac 1{C} \int {i dt} + R i + R_L i = u.
\end{multline}

Использовав выражения~\cref{eq:eq/stationary/omegaQ,
	eq:eq/stationary/generat,
	eq:eq/stationary/coup,
	eq:eq/stationary/coupeh},
можно перейти от радиотехнических параметров схемы к электродинамическим.
В ёмкостном представлении выражение примет вид
%
\begin{multline}\label{eq:eq/transitional/base-с}
	\sum_{n=1}^{N} \left(
		\frac{K^H_n}{2} \frac{f_0}{f_n} \frac{d^2x_n}{dt^2} -
		2 \pi^2 K^E_n f_0^2 x_n
	\right) +
	\frac{d^2x}{dt^2} =
	-\frac{2 \pi  f_0}{Q_0} (1 + \chi_{\textup{г}} + \chi_{\textup{н}}) \frac{dx}{dt} - \\ -
	4 \pi^2 f_0^2 x +
	8 \pi f_0 f_{\textup{г}} \sqrt \frac{\pi f_0 P_{\textup{г}} \chi_{\textup{г}}}{Q_0}
	\cos{\left(2 \pi f_{\textup{г}} t + \varphi_0 \right)},
\end{multline}
%
в индуктивном представлении
%
\begin{multline}\label{eq:eq/transitional/base-l}
	\sum_{n=1}^{N} \left(
		f_0 \frac{K^H_n}{2} \frac{d^2x_n}{dt^2} -
		2 \pi^2 K^E_n f_0^2 f_n x_n
	\right) +
	\frac{d^2x}{dt^2} =
	- \frac{2 \pi f_0}{Q_0} (1 + \chi_{\textup{г}} + \chi_{\textup{н}}) \frac{dx}{dt} - \\ -
	4 \pi^2 f_0^2 x -
	4 f_0 f_{\textup{г}} \sqrt \frac{\pi P_{\textup{г}} \chi_{\textup{г}}}{Q_0}
	\cos{\left(2 \pi f_{\textup{г}} t + \varphi_0 \right)}.
\end{multline}

В данных выражениях вторые производные по времени сгруппированы в левой части уравнения.
Обозначим их \(k_{i,j}\).
В правой части собраны функции амплитуды, первые производные и свободные члены.
Обозначим правую часть уравнения \(\operatorname{g}(\dot x, x, t)\).

Для секции из \(N\) ячеек, каждая из которых описывается
выражением~\cref{eq:eq/transitional/base-с} или~\cref{eq:eq/transitional/base-l},
можно записать систему уравнений в матричном виде
%
\begin{equation}\label{eq:eq/transitional/Tmatrix}
	\begin{bmatrix}
		1       & k_{1;2}   & k_{1;3}
		        & \cdots
		        & k_{1;N-1} & k_{1;N} \\
		k_{2;1} & 1         & k_{2;3}
		        & \cdots
		        & k_{2;N-1} & k_{2;N} \\
		\cdots  & \cdots
		        & \cdots    & \ddots
		        & \cdots    & \cdots  \\
		k_{N;1} & k_{N;2}   & k_{N;3}
		        & \cdots
		        & k_{N;N-1} & 1       \\
	\end{bmatrix}
	\begin{bmatrix}
		\ddot x_1 \\ \ddot x_2 \\ \cdots \\ \ddot x_N \\
	\end{bmatrix}
	=
	\begin{bmatrix}
		\operatorname{g}_1(\dot x, x, t) \\ \operatorname{g}_2(\dot x, x, t) \\
		\cdots                           \\ \operatorname{g}_N(\dot x, x, t) \\
	\end{bmatrix}.
\end{equation}
%
Обозначим матрицу аргументов при вторых производных \([K]\).
Систему~\cref{eq:eq/transitional/Tmatrix} можно выразить относительно вектора вторых производных
амплитуд по времени
%
\begin{equation}
	\left[\ddot x\right] = \left[K\right]^{-1}
	\left[\operatorname{g}(\dot x, x, t)\right].
\end{equation}

Данную систему можно решить с использованием численных методов~\cite{deSolve-Manual}.
Для этого требуется свести матричное уравнение второго порядка к системе
матричных уравнений первого порядка.
Это можно сделать, введя замену \(\dot x_n = y_n\).
%
\begin{equation}\label{eq:eq/transitional/base}
	\begin{cases}
		\left[\dot x\right] = \left[y\right], \\
		\left[\dot y\right] = \left[T\right]^{-1}
		\left[\operatorname{g}(y, x, t)\right].
	\end{cases}
\end{equation}

Чтобы избежать расхождения результата численного решения на ЭВМ требуется
использовать нормированные частоты \(\hat f_i = f_i/f_{\textup{г}}\) и время \(\hat t = f_{\textup{г}} t\).
Временной шаг решения требуется выбирать меньше периода СВЧ колебаний.
Кроме того, можно использовать решатели, позволяющие автоматически переключаться между
явными и неявными методами решения ОДУ~\cite{desolve}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
