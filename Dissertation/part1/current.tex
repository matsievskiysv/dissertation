% \paragraph{Учёт нагрузки током\label{sec:eq/current}}

Слагаемое \(b_{\textup{п}}\) в формуле~\cref{eq:eq/stationary/base} характеризует влияние
пространственного заряда пучков заряженных частиц, проходящих через резонатор.
% (\cref{fig:eq/current/current_struct}).
%
% \begin{figure}[htbp]
% 	\centerfloat{
% 		\includegraphics[height=0.25\textheight]{scheme/current_struct}
% 		\caption{Схема ячейки с проходящими по оси сгустками
% 			частиц\label{fig:eq/current/current_struct}}
% 	}
% \end{figure}
%
Его можно представить в виде эквивалентного источника тока с использованием
следующих приближений~\cite{padamsee2008rf}:
%
\begin{itemize}[beginpenalty=10000]
	\item при похождении ячейки скорость частиц меняется незначительно;
	\item частота прохождения сгустков кратна частоте поля в резонаторе;
	\item частицы в сгустках имеют нормальное пространственное распределение;
	\item волны высших типов имеют незначительное влияние на частицы.
\end{itemize}
%
Тогда импульсный ток пучка заряженных частиц можно представить в виде
независимого источника переменного тока величиной
\(I_{\textup{п}} \approx i_{\textup{п}}/2\)~\cite{perry}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Последовательная схема}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{current_serial}}{}%
			\input{images/scheme/eq/current_serial.tikz}
		}
		\hfill
		\subcaptionbox{Параллельная схема}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{current_parallel}}{}%
			\input{images/scheme/eq/current_parallel.tikz}
		}
		\hfill
	}
	\caption{Эквивалентная схема ячейки с нагрузкой
		пучком\label{fig:eq/current/current}}
\end{figure}

Перейти от эквивалентного источника тока и источнику напряжения можно, рассмотрев ЭС возбуждаемого
проходящими пучками заряженных частиц резонатора в двух представлениях:
последовательном и параллельном (\cref{fig:eq/current/current})~\cite{StepnovPHD}.
Для рассматриваемого резонатора можно записать систему уравнений
%
\begin{equation}\label{eq:eq/current/base}
	\begin{cases}
		\left( R + j 2 \pi f L + \frac{1}{j 2 \pi f C} \right) \dot I = \dot U_{\textup{п}}; \\
		\left( \frac{1}{\hat R} + \frac{1}{j 2 \pi f \hat L} + j 2 \pi f \hat C \right)
		\dot {\hat U} = \dot I_{\textup{п}}.
	\end{cases}
\end{equation}

Мощность, выделяемая на сопротивлении \(\hat R\), определяется формулой~\cite{Atabekov}
%
\[P = \frac{\hat U^2}{2 \hat R}.\]
%
Сопоставив её с определением шунтового сопротивления резонатора~\cite{sobenin-tech}
%
\[R_{\textup{ш}} = \frac{\hat U^2}{P}\]
%
легко найти связь сопротивления параллельного контура и шунтового сопротивления
резонатора~\cite{perry}
%
\begin{equation}\label{eq:eq/current/rsh}
	R_{\textup{ш}} = 2 \hat R.
\end{equation}

После преобразований системы~\cref{eq:eq/current/base} с использованием
выражений~\cref{eq:eq/stationary/omegaQ,
	eq:eq/stationary/generat,
	eq:eq/stationary/coup,
	eq:eq/stationary/coupeh}
получим
%
\begin{equation}\label{eq:eq/current/cases}
	\begin{cases}
		\dot X - \frac{f^2}{f_0^2} \dot X + j \frac{f}{f_0} \frac{1}{Q} \dot X
		=
		j \dot U_{\textup{п}} f \sqrt{\frac{2 \pi}{f_0 \rho}}; \\
		\dot{\hat X} - \frac{f^2}{\hat{f_0}^2} \dot{\hat X} +
		j \frac{f}{\hat{f_0}}\frac{1}{\hat{Q}} \dot{\hat X}
		=
		j \dot I_{\textup{п}} f \hat{\rho}
		\sqrt{\frac{2 \pi \hat{\rho}}{\hat{f_0}}},
	\end{cases}
\end{equation}
%
где \(\rho\) и \(\hat \rho\) "--- характеристические сопротивления последовательного и параллельного
контуров соответственно;
\(\dot X = \dot I \sqrt{L}\) и \(\dot{\hat X} = \dot{\hat U} \sqrt{\hat C}\) "--- характеристики
запасённой энергии в контурах.
В силу того, что обе схемы на \cref{fig:eq/current/current} описывают один и тот же резонатор,
собственные частоты, добротности и запасённые энергии обеих схем должны быть равны:
%
\[f = \hat f;\;Q = \hat Q;\; \dot X = \dot {\hat X}.\]
%
Тогда равны левые части уравнений системы~\cref{eq:eq/current/cases}.
Следовательно, равны и правые части, и можно найти связь напряжения эквивалентного источника \(\dot
U_{\textup{п}}\) и импульсного тока пучка \(\dot I_{\textup{п}}\)
%
\begin{equation}\label{eq:eq/current/UtoI_C}
	\dot U_{\textup{п}} = \dot I_{\textup{п}} \sqrt{\rho \hat \rho}.
\end{equation}
%
После подстановки~\cref{eq:eq/current/UtoI_C} в~\cref{eq:eq/stationary/kirchhoff} можно записать
выражение учёта нагрузки пучком формулы~\cref{eq:eq/stationary/base} для ёмкостного
представления в виде
%
\begin{equation}\label{eq:eq/current/c}
	b^C_{\textup{п}} = \sqrt{\frac{\pi f_0 R_{\textup{ш}}}{Q}} |\dot I_{\textup{п}}|
	\exp{\left(j \left[\varphi_{\textup{п}} + \frac{\pi}{2}\right]\right)},
\end{equation}
%
и для индуктивного представления в виде
%
\begin{equation}\label{eq:eq/current/l}
	b^L_{\textup{п}} = \frac{1}{2 f} \sqrt{\frac{f_0 R_{\textup{ш}}}{\pi Q}}
	|\dot I_{\textup{п}}| \exp{\left(j \left[\varphi_{\textup{п}} - \frac{\pi}{2}\right]\right)}.
\end{equation}
%
В обоих выражениях \(\varphi_{\textup{п}}\) "--- фаза пролёта пучком резонатора в лабораторной
системе отчёта.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
