\section{Дисперсионная характеристика секции\label{sec:eq/dispersion}}

Для бесконечной секции, состоящей из одинаковых ячеек, можно определить дисперсионную
характеристику "--- зависимость фазовой скорости распространяющейся в секции волны от
частоты~\cite{sobenin_kdv}.
Дисперсия может быть как положительная, так и отрицательная.
При положительной дисперсии направления фазовой и групповой скоростей сонаправлены, при
отрицательной "--- противоположны.
% Часто дисперсионная характеристика УС изображается в виде графика в координатах
% \((\theta, f)\), где \(\theta\) "--- набег фазы на период секции.

Для бесконечной однородной секции параметры ячеек одинаковы \(f_1, f_2, \ldots = f_0;\)
%
\(K^E_1, K^E_2, \ldots = K^E;\) \(K^H_1, K^H_2, \ldots = K^H\).
%
Кроме того, пренебрежём потерями в ячейках \(Q_1, Q_2, \ldots = \infty\).
Тогда выражение~\cref{eq:eq/stationary/base} при ёмкостном представлении
преобразуется к виду
%
\begin{equation}\label{eq:eq/dispersion/init-c}
	\dot X_0 \left( 1 - \frac{f^2}{f_0^2} \right) -
	\sum_{n=1}^{N} \left( \frac{K^E_n}{2} +
		\frac{K^H_n}{2} \frac{f^2}{f_0^2} \right) \dot X_n = 0,
\end{equation}
%
при индуктивном представлении "--- к виду
%
\begin{equation}\label{eq:eq/dispersion/init-l}
	\dot X_0 \left( 1 - \frac{f_0^2}{f^2} \right) +
	\sum_{n=1}^{N} \left( \frac{K^H_n}{2} +
		\frac{K^E_n}{2} \frac{f_0^2}{f^2} \right) \dot X_n = 0.
\end{equation}
%
В силу однородности секции, набег фазы на ячейку будет постоянен и равен
%
\begin{equation}\label{eq:eq/dispersion/dphi}
	\dot X_n = \dot X_0 \exp{(j n \varphi)}.
\end{equation}

После подстановки выражения~\cref{eq:eq/dispersion/dphi}
в~\cref{eq:eq/dispersion/init-c, eq:eq/dispersion/init-l}
для обоих представлений получится одинаковое выражение
%
\begin{equation}\label{eq:eq/dispersion/general}
	\frac{f}{f_0} = \sqrt{\frac{1 - \sum_{n=1}^{N}K^E_n \cos{(n \varphi)}}
		{1 + \sum_{n=1}^{N}K^H_n \cos{(n \varphi)}}},
\end{equation}
%
где \(N\) "--- количество учтённых коэффициентов связи между ячейками.

Для часто используемого на практике случая учёта связи только соседних ячеек \(N = 1\)
выражение упрощается до
%
\begin{equation}\label{eq:eq/dispersion/lossless}
	\frac{f}{f_0} = \sqrt{\frac{1 - K^E \cos{\varphi}}
		{1 + K^H \cos{\varphi}}}.
\end{equation}

Данная зависимость полностью определяется тремя точками дисперсионной кривой.
Так, при наличии двух известных точек дисперсионной зависимости с координатами \((\varphi_1; f_1)\)
и \((\varphi_2; f_2)\), а также при известном значении собственной резонансной частоты ячейки,
соответствующей виду колебаний \(\pi/2\), можно определить значения коэффициентов
связи ячеек при помощи выражений
%
\begin{subequations}
	\begin{align}
		K^E & =
		\frac{
			1
			-
			\frac{f_1^2}{f_0^2}
			(1 - K^H \cos{\varphi _1})
		}{
			\cos{\varphi _1}
		}
		=
		\frac{
			1
			-
			\frac{f_2^2}{f_0^2}
			(1 - K^H \cos{\varphi _2})
		}{
			\cos{\varphi _2}
		};\label{eq:eq/dispersion/Ke} \\
		K^H & =
		\frac{
			\cos{\varphi _1}
			\left( 1 - \frac{f_2^2}{f_0^2} \right)
			-
			\cos{\varphi _2}
			\left( 1 - \frac{f_1^2}{f_0^2} \right)
		}{
			\cos{\varphi _1}
			\cos{\varphi _2}
			\left(
				\frac{f_1^2}{f_0^2}
				-
				\frac{f_2^2}{f_0^2}
			\right)
		}.\label{eq:eq/dispersion/Kh}
	\end{align}
\end{subequations}

Если учесть наличие потерь в ячейках, то набег фазы на ячейку будет выражаться в виде
%
\[
	X_n = X_0 \exp{(j n \varphi)} \exp{(\alpha n D)},
\]
%
а выражение для дисперсионной характеристики при учёте связи только соседних ячеек примет
вид~\cite{kaluzni06}
%
\begin{equation}\label{eq:eq/dispersion/lossy}
	\frac f {f_0}
	=
	\sqrt{
		\frac{
			1 - K^E \cos{\varphi}\cosh{(\alpha D)}
		}{
			1 + K^H \cos{\varphi}\cosh{(\alpha D)}
		}
	},
\end{equation}
%
где
%
\begin{multline}
	\label{eq:eq/dispersion/lossy-alphaD}
	\cosh{(\alpha D)} =
	\left[
		-(K^E - K^H) \cos{\varphi}
		\pm
		\left\{
			{(K^E - K^H)}^2 \cos^2{\varphi} +
			\right.
			\right.
			\\
			\left.
			\left.
			+
			4 (Q^2 {(K^E + K^H)}^2 \sin^2{\varphi} +
			K^E K^H \cos^2{\varphi})
			\times
			\right.
			\right.
			\\
			\left.
			\left.
			\times
			(1 + Q^2 {(K^E + K^H)}^2
			\sin^2{\varphi})
		\right\}^{\sfrac{1}{2}}
	\right]
	\times\\\times
	\left[
		2 Q^2 {(K^E + K^H)}^2 \sin^2{\varphi} +
		K^H K^E \cos^2{\varphi}
	\right]^{-1}.
\end{multline}
%
Следует отметить, что функция \(\operatorname{f}(\varphi)\), определяемая
выражениями~\cref{eq:eq/dispersion/lossy, eq:eq/dispersion/lossy-alphaD}
имеет асимптоты в точках \(\varphi = 0\) и \(\varphi = \pi\).
Применение выражений в областях, близких к указанным точкам, недопустимо.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
