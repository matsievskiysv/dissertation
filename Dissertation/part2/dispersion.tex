\section{Дисперсионные характеристики ускоряющих структур\label{sec:comp/dispersion}}

Дисперсионная характеристика является одним из основных параметров
УС~\cite{sobenin-acc-st}.
Она характеризует фазовую и групповую скорости волны, распространяющейся в УС на виде колебаний
\(\theta\).
На \cref{fig:com/dispersion/interface} представлен интерфейс расчёта дисперсионных
характеристик секций УС программы \textsc{LinacCalc}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\includegraphics[width=\columnwidth]{linaccalc/dispersion}
	}
	\caption{Интерфейс модуля расчёта дисперсионных характеристик ячеек
		программы \textsc{LinacCalc}\label{fig:com/dispersion/interface}}
\end{figure}

На \cref{fig:comp/dispersion/models} приведены три структуры, для которых проводилось
сравнение расчётов при помощи МЭС и~МКЭ\@.
На \cref{fig:comp/dispersion/models-dls,fig:comp/dispersion/models-dlsm}
представлены модели ячеек КДВ и \mbox{КДВ-М} соответственно.
Данные ячейки широко применяются для ускорения лёгких
частиц~\cite{dlw, Antipov:IPAC2018-WEPMF068, Tang:IPAC2018-MOPML055, kutsaev_obrval}.
Различие между ними заключается во взаимном направлении фазовой и групповой скоростей волны:
для ячеек КДВ направления скоростей совпадают, для \mbox{КДВ-М} "--- они противоположны.
На \cref{fig:comp/dispersion/models-defl} представлена ячейка
ВЧ дефлектора на~БВ~\cite{authorvakDefl}. % , TTV_Smirnov, deflect_cell, Smirnov:IPAC10
Данный тип ячеек предназначен для преобразования продольного эмиттанса пучка в
поперечный путём действия на него волны с поперечно направленным электрическим полем
\(E_{11}\). % ~\cite{Smirnov:FEL15}
Для представления ячейки дефлектора подходит ЭС с комбинированной связью между
ячейками~\cite{kaluzni}.

Таким образом, ячейка КДВ обладает преобладающей ёмкостной связью, \mbox{КДВ-М} "--- индуктивной
связью, ячейка дефлектора "--- комбинированной.
Для расчёта с использованием МКЭ использованы модели одного периода структур с
применением периодических граничных условий~\cite{utep}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{КДВ\label{fig:comp/dispersion/models-dls}}{%
			\includegraphics[width=0.33\columnwidth]{model/dls}}
		\hfill
		\subcaptionbox{КДВ-М\label{fig:comp/dispersion/models-dlsm}}{%
			\includegraphics[width=0.33\columnwidth]{model/dlsm}}
		\hfill
		\subcaptionbox{Дефлектор на~БВ\label{fig:comp/dispersion/models-defl}}{%
			\includegraphics[width=0.33\columnwidth]{model/defl}}
		\hfill
	}
	\caption{Модели ячеек с различными типами связи\label{fig:comp/dispersion/models}}
\end{figure}

Дисперсионные зависимости для секций ячеек КДВ, \mbox{КДВ-М} и ВЧ~дефлектора,
полученные при помощи МКЭ и~МЭС, приведены на \cref{fig:comp/dispersion/disps}.
На данных рисунках зависимость, полученная при помощи формулы~\cref{eq:eq/dispersion/lossless},
обозначена как зависимость <<без потерь>>, рассчитанная по формуле~\cref{eq:eq/dispersion/lossy}
"--- <<с потерями>>.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{КДВ\label{fig:comp/dispersion/disps-dls}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{disps-dls}}{}%
			\input{images/plot/compare/dispersion_dls.tikz}}
		\hfill
		\subcaptionbox{КДВ-М\label{fig:comp/dispersion/disps-dlsm}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{disps-dlsm}}{}%
			\input{images/plot/compare/dispersion_dlsm.tikz}}
		\hfill
	} \par
	\centerfloat{%
		\subcaptionbox{Ячейка ВЧ дефлектора\label{fig:comp/dispersion/disps-defl}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{disps-defl}}{}%
			\input{images/plot/compare/dispersion_defl.tikz}}
	}
	\caption{Сравнение дисперсионных характеристик, рассчитанных двумя
		методами\label{fig:comp/dispersion/disps}}
\end{figure}

Для \mbox{КДВ} разница расчётов частот видов колебаний составила
\(\delta_{lossless} = \SI{0,51}{\MHz}\) со среднеквадратичным отклонением
\(\sigma_{lossless} = \SI{0,22}{\MHz}\) для случая без потерь и
\(\delta_{lossy} = \SI{0,51}{\MHz}\), \(\sigma_{lossy} = \SI{0,22}{\MHz}\)
 для случая с потерями;
для \mbox{КДВ-М} "--- \(\delta_{lossless} = \SI{2}{\MHz}\), \(\sigma_{lossless} = \SI{0,76}{\MHz}\) и
\(\delta_{lossy} = \SI{2}{\MHz}\), \(\sigma_{lossy} = \SI{0,76}{\MHz}\);
для ВЧ дефлектора "--- \(\delta_{lossless} = \SI{12,4}{\MHz}\), \(\sigma_{lossless} = \SI{5}{\MHz}\) и
\(\delta_{lossy} = \SI{12,4}{\MHz}\), \(\sigma_{lossy} = \SI{5}{\MHz}\).

Характерным является наклон кривых "--- знаки тангенса угла наклона касательной и прямой, проходящей
через начало координат, совпадают для секции ячеек КДВ и противоположны для \mbox{КДВ-М} и
дефлектора.
Это указывает на сонаправленность фазовой и групповой скоростей в первом случае и их
разнонаправленность в двух других случаях.

При увеличении величины связи между соседними ячейками также возрастает связь удалённых друг от
друга ячеек.
В этом случае при моделировании УС требуется учёт дополнительных коэффициентов связи.
На \cref{fig:comp/dispersion/dispersion_defl} изображены дисперсионные характеристики для секции
ячеек ВЧ дефлектора с увеличенной связью между ячейками.
Зависимость для МЭС без учёта связи с удалёнными ячейками обозначена как зависимость <<без учёта>>.
С учётом связи "--- как зависимость <<с учётом>>.
Разница расчётов и среднеквадратичное отклонение частот от значений, рассчитанных при помощи МКЭ,
для этих двух случаев составили \(\delta = \SI{8,8}{\MHz}\), \(\sigma = \SI{6,3}{\MHz}\) и
\(\delta = \SI{2,1}{\MHz}\), \(\sigma = \SI{1,6}{\MHz}\) соответственно.
Из представленных результатов расчётов видно, что учёт связи удалённых ячеек позволяет точнее
моделировать УС ЛУЭ\@.
%
\begin{figure}[htbp]
	\centerfloat{
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{defl_big}}{}%
		\input{images/plot/compare/dispersion_defl_big.tikz}
	}
	\caption{Результаты моделирования дисперсионной характеристики секции ВЧ дефлектора на~БВ
		при помощи МКЭ и МЭС с учётом и без учёта связи удалённых
		ячеек\label{fig:comp/dispersion/dispersion_defl}}
\end{figure}

% На \cref{fig:comp/dispersion/dispersion_defl} представлена дисперсионная характеристика
% ячейки дефлектора в случае крайнего увеличения размера апертуры центрального отверстия.

% Как видно из представленной зависимости, в области вида колебаний \(\theta=0\)
% проявляется значительное различие результатов расчётов, проделанных при помощи двух методов.
% Это связанно с тем, что при значительном увеличении отверстия связи уже нельзя игнорировать
% наличие связи через ячейку~\cite{sobenin-tech}.
% Кроме того, при увеличении ёмкостной связи ячеек, величины ёмкостей \(C\), \(C_R\) и \(C_L\) схемы,
% изображённой на \cref{fig:eq/stationary/stationary-scheme}, будут сопоставимы.
% В таком случае использование упрощённого выражения коэффициента связи \(K^E_R\) и \(K^E_L\),
% указанного в формуле~\cref{eq:eq/stationary/coupeh}, недопустимо~\cite{Atabekov}.

% \begin{figure}[htbp]
% 	\centerfloat{
% 		\includegraphics[width=0.8\columnwidth]{plot/compare/dispersion_defl_big}
% 	}
% 	\caption{
% 		Дисперсионная характеристика ячейки дефлектора с большим коэффициентом
% 		связи, полученная при помощи МКЭ и МЭС с и без учёта потерь\label{fig:comp/dispersion/dispersion_defl}}
% \end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
