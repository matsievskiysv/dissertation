\subsection{Секция на бегущей волне\label{sec:hyb/sec/tr}}

За группирователем на~СВ располагается секция на~БВ\@.
Минимальная энергия, при которой возможна инжекция в секцию на~БВ, настроенную на фазовую скорость
\(\beta_{\textup{ф}}=1\), определяется выражением
%
\[\gamma_{\textup{мин}} = \frac{1+\left( G / \pi \right)^2}{2 G / \pi},\]
%
где
%
\(G = e E \lambda/\pi W_0\) "--- нормированная амплитуда волны,
\(e\) и \(W_0\) "--- заряд и энергия покоя электрона соответственно,
\(E\) "--- амплитуда ускоряющего поля, \(\lambda\) "--- длина волны генератора~\cite{valdner_lab}.
Для рассматриваемого ускорителя минимальная энергия инжекции в секцию на~БВ, рассчитанная по данной
формуле, составляет \(W^{\textup{БВ}}_{\textup{мин}}=\SI{0.4}{\MeV}\).
Как показано в \cref{tab:hyb/sec/st/par}, средняя энергия пучка на выходе секции на~СВ
составляет \(W^{\textup{СВ}}_{\textup{ср}}=\SI{0.96}{\MeV}\).
Таким образом, инжекция частиц в секцию на~БВ, настроенную на относительную фазовую скорость волны
\(\beta_{\textup{ф}}=1\), возможна.

Секция на~БВ состоит из ячеек \mbox{КДВ-М} с видом колебаний \(\theta=2\pi/3\).
На данном виде колебаний УС обладает высоким шунтовым сопротивлением и
низкими омическими потерями~\cite{sobenin_kdv, sokolov_kdvopt}.
Расчётный макет регулярных ячеек \mbox{КДВ-М} представлен на \cref{fig:hyb/sec/tr/cell-reg},
геометрические параметры макета обозначены на \cref{fig:hyb/sec/tr/draw}.
ЭДХ ячеек представлены в \cref{tab:hyb/sec/tr/reg_cell}, вариационные характеристики
изображены на \cref{fig:hyb/sec/tr/par_var}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Сечение макета\label{fig:hyb/sec/tr/cell-reg}}{
			\includegraphics[height=0.2\textheight]{model/dlsm_hybrid}}
		\hfill
		\subcaptionbox{Основные размеры\label{fig:hyb/sec/tr/draw}}{
			\includegraphics[height=0.2\textheight]{model/dlsm_draw}}
		\hfill
	}
	\caption{Модель одной ячейки и двух полуячеек \mbox{КДВ-М}\label{fig:hyb/sec/tr/cell}}
\end{figure}
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Длина носиков\label{fig:hyb/sec/tr/par_var-nl}}{
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{dlsm_nl}}{}%
			\input{images/plot/hybrid/dlsm_var_n_l.tikz}}
		\hfill
		\subcaptionbox{Угол раствора щелей связи\label{fig:hyb/sec/tr/par_var-cw}}{
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{dlsm_cw}}{}%
			\input{images/plot/hybrid/dlsm_var_k.tikz}}
		\hfill
	}
	\caption{Зависимость шунтового сопротивления и добротности ячеек \mbox{КДВ-М} от
		геометрических параметров\label{fig:hyb/sec/tr/par_var}}
\end{figure}
%
\begin{table}[htbp]
	\centerfloat{%
		\captionsetup{justification=centering}
		\caption{Параметры регулярных ячеек \mbox{КДВ-М}\label{tab:hyb/sec/tr/reg_cell}}
		\begin{tabular}{lS[table-parse-only]}
			\toprule
			Параметр                                    & Значение \\ \midrule
			\(\beta_{\textup{ф}}\)                      & 1        \\
			\(\beta_{\textup{гр}}, \si{\percent}\)      & 0.1      \\
			\(k_{\textup{св}}, \si{\percent}\)          & 1        \\
			D, \si{\mm}                                 & 34       \\
			\(f_{2\pi/3}, \si{\MHz}\)                   & 2856,1   \\
			\(\alpha, \si{\neper\per\m}\)               & 0.3      \\
			\(r_{\textup{ш}}, \si{\MOhm\per\m}\)        & 100      \\
			\(E\lambda/\sqrt{P}, \si{\Ohm\tothe{1/2}}\) & 720      \\
			\bottomrule
		\end{tabular}
	}
\end{table}

Для настройки ячеек ТТВ использованы аналитические выражения МЭС
оптимальных связи ячейки с подводящим волноводом~\cref{eq:eq/opt_coup/chi} и собственной
резонансной частоты ячейки~\cref{eq:eq/opt_coup/f}.
Распределение амплитуды и фазы электрического поля в оптимизированной секции, рассчитанные
в программе \textsc{LinacCalc}, представлены на
\cref{fig:hyb/sec/st/field_lc}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Модуль амплитуды\label{fig:hyb/sec/st/field_lc-mod}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{kdvm_e_mod}}{}%
			\input{images/plot/hybrid/kdvm_e_mod.tikz}}
		\hfill
		\subcaptionbox{Фаза\label{fig:hyb/sec/st/field_lc-phase}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{kdvm_e_phase}}{}%
			\input{images/plot/hybrid/kdvm_e_phase.tikz}}
		\hfill
	}
	\caption{Распределение электрического поля в секции на~БВ,
		рассчитанное при помощи программы
		\textup{LinacCalc}\label{fig:hyb/sec/st/field_lc}}
\end{figure}

Для моделирования ячейки ТТВ использован макет, представленный на
\cref{fig:hyb/sec/tr/tune-1}.
Полуячейка \mbox{КДВ-М} является регулярной, на плоскости сечения устанавливается магнитное
граничное условие.
Собственная частота ячейки ТТВ настраивалась на полученное в программе \texttt{LinacCalc} значение
частоты изменением радиуса ячейки, коэффициент связи с подводящим волноводом "--- изменением ширины
окна связи.

\begin{comment}
\begin{figure}[htbp]
	\centerfloat{%
		\includegraphics[height=0.35\textheight]{model/dlsm_hybrid_power}
	}
	\caption{Сечение модели ячейки ТТВ и полуячейки регулярной
		ячейки~\mbox{КДВ-М}\label{fig:hyb/sec/tr/cell-ttv}}
\end{figure}
\end{comment}

При дальнейшем моделировании ячейки ТТВ, его параметры были уточнены с использованием метода
настройки по трём коэффициентам отражения, описанного в работе~\cite{ALESINI20071176}.
Для настройки использовались три макета с различным количеством регулярных ячеек:
половиной ячейки~(\cref{fig:hyb/sec/tr/tune-1}),
целой и половиной ячейки~(\cref{fig:hyb/sec/tr/tune-2}),
двумя целыми и половиной ячейки~(\cref{fig:hyb/sec/tr/tune-3}).
Все макеты закорачивались идеальным проводником электрического поля.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Макет 1\label{fig:hyb/sec/tr/tune-1}}{%
			\includegraphics[height=0.3\textheight]{model/dlsm_1_hybrid}}
		\hfill
		\subcaptionbox{Макет 2\label{fig:hyb/sec/tr/tune-2}}{%
			\includegraphics[height=0.3\textheight]{model/dlsm_2_hybrid}}
		\hfill
		\subcaptionbox{Макет 3\label{fig:hyb/sec/tr/tune-3}}{%
			\includegraphics[height=0.3\textheight]{model/dlsm_3_hybrid}}
		\hfill
	}
	\caption{Сечения макетов настройки ячейки ТТВ по трём коэффициентам
		отражения\label{fig:hyb/sec/tr/tune}}
\end{figure}

Для каждого макета рассчитывались комплексные коэффициенты отражения на рабочей частоте,
которые затем пересчитывались в комплексный коэффициент отражения при помощи формул
%
\begin{subequations}\label{eq:hyb/sec/tr/tune}
	\begin{equation}\label{eq:hyb/sec/tr/phi1}
		\varphi_{21} = \frac{\angle{\left\{ \textup{Г}_2/\textup{Г}_1 \right\}}}{2} - \theta,
	\end{equation}
	\begin{equation}\label{eq:hyb/sec/tr/phi2}
		\varphi_{32} = \frac{\angle{\left\{ \textup{Г}_3/\textup{Г}_2 \right\}}}{2} - \theta,
	\end{equation}
	\begin{equation}\label{eq:hyb/sec/tr/sin}
		\sin{\delta_2} = \pm
		\frac{\sin{(2 \theta + \varphi_{32} + \varphi_{21})}}
		{\sqrt{1 + \frac{\sin^2{\varphi_{21}}}{\sin^2{\varphi_{32}}} -
		2 \frac{\sin{\varphi_{21}}}{\sin{\varphi_{32}}}
		\cos{(2 \theta + \varphi_{32} + \varphi_{21})}
		}},
	\end{equation}
	\begin{equation}\label{eq:hyb/sec/tr/cos}
		\cos{\delta_2} = \mp
		\frac{\cos{(2 \theta + \varphi_{32} + \varphi_{21})} -
		\frac{\sin{\varphi_{21}}}{\sin{\varphi_{32}}}}
		{\sqrt{1 + \frac{\sin^2{\varphi_{21}}}{\sin^2{\varphi_{32}}} -
		2 \frac{\sin{\varphi_{21}}}{\sin{\varphi_{32}}}
		\cos{(2 \theta + \varphi_{32} + \varphi_{21})}
		}},
	\end{equation}
	\begin{multline}\label{eq:hyb/sec/tr/s11}
		|S_{11}| = \left\{
			1 + 4 \frac{\sin\theta}{\sin{\varphi_{32}}}
			\left[
				\left( \cos{(\theta + \varphi_{32})} +
					\right.
					\right.
					\right.
					\\
					\left.
					\left.
					\left.
					+ \frac{\sin{\theta}}{\sin{\varphi_{32}}} \sin^2{\delta_2} +
					\sin{(\theta + \varphi_{32})} \sin{\delta_2} \cos{\delta_2}
				\right)
			\right]
		\right\}^{-1/2},
	\end{multline}
\end{subequations}
%
где \(\textup{Г}_1\), \(\textup{Г}_2\) и \(\textup{Г}_3\) "--- комплексные коэффициенты отражения
от макета~1~(\cref{fig:hyb/sec/tr/tune-1}), макета~2~(\cref{fig:hyb/sec/tr/tune-2}) и
макета~3~(\cref{fig:hyb/sec/tr/tune-3}) соответственно;
\(\theta\) "--- рабочий вид колебаний в секции.
Знаки выражений \(\sin{\delta_2}\) и \(\cos{\delta_2}\) выбирались таким образом, чтобы значение
модуля отражения \(|S_{11}|\) было вещественным.

При помощи выражений~\cref{eq:hyb/sec/tr/tune} исследовано пространство параметров
радиуса ячейки ТТВ \texttt{R} и ширины окна связи \texttt{W} в окрестности из предварительно
настроенных значений.
График зависимости модуля коэффициента отражения \(|S_{11}|\) от указанных параметров приведён
на \cref{fig:hyb/sec/tr/tune_map}.
Значение коэффициента связи в точке минимального отражения отличается от предварительно настроенного
значения на~\SI{2.7}{\percent}, значение собственной частоты "--- на~\SI{0.88}{\MHz}.
Таким образом, аналитический расчёт при помощи МЭС даёт хорошее начальное
приближение параметров ТТВ, что сокращает суммарное время моделирования и оптимизации секции на~БВ\@.
%
\begin{figure}[htbp]
	\centerfloat{%
		\includegraphics[width=0.8\linewidth]{plot/hybrid/kdvm_tune}
	}
	\caption{Зависимость модуля коэффициента отражения от ячейки ТТВ от
		радиуса \texttt{R} и ширины окна связи \texttt{W}\label{fig:hyb/sec/tr/tune_map}}
\end{figure}

Моделирование динамики частиц в секции на~БВ производилось при помощи программы расчёта
динамики на~БВ семейства программ \textsc{Beamdulac}~\cite{beamd-trw}.
Для уменьшения размеров пучка в начале секции использован постоянный магнит, создающий магнитное
поле величиной \SI{55}{\gauss}.
Параметры пучка на выходе из секции представлены в \cref{tab:hyb/sec/tr/par}.
Энергетические параметры, поперечное распределение частиц и фазовый портрет пучка на выходе из
секции на~БВ представлены на \cref{fig:hyb/sec/tr/gamma, fig:hyb/sec/tr/beam,
	fig:hyb/sec/tr/energy}~\cite{authorscopHyb,authorscopHyb2}.
%
\begin{table}[htbp]
	\centerfloat{%
		\begin{threeparttable}
			\caption{Параметры пучка на выходе из секции на
				БВ\label{tab:hyb/sec/tr/par}}
			\begin{tabular}{lS[table-parse-only]}
				\toprule
				\multicolumn{1}{c}{Параметр}               & Значение \\ \midrule
				Коэффициент токопрохождения, \si{\percent} & 97       \\
				Ток на выходе, \si{\A}                     & 0,1      \\
				Средняя энергия на выходе, \si{\MeV}       & 9,98     \\
				\(\alpha\)                                 & -0,57    \\
				\(\beta, \si{\cm\per\radian}\)             & 28,6     \\
				\(\epsilon, \si{\cm\milli\radian}\)        & 2,3      \\
				\bottomrule
			\end{tabular}
		\end{threeparttable}
	}
\end{table}
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Распределение по фазе\label{fig:hyb/sec/tr/gamma-phi}}{%
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/trw/gamma_over_phi}}
		\hfill
		\subcaptionbox{Распределение по продольной координате\label{fig:hyb/sec/tr/gamma-l}}{%
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/trw/gamma_over_l}}
		\hfill
	}
	\caption{Энергетическое распределение частиц в пучке на выходе из гибридного
		ускорителя\label{fig:hyb/sec/tr/gamma}}
\end{figure}
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Профиль пучка\label{fig:hyb/sec/tr/beam-pr}}{%
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/trw/profile}}
		\hfill
		\subcaptionbox{Распределение частиц в пучке\label{fig:hyb/sec/tr/beam-pr_hist}}{%
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/trw/profile_size}}
		\hfill
	} \par
	\centerfloat{%
		\hfill
		\subcaptionbox{Фазовый портрет пучка\label{fig:hyb/sec/tr/beam-emit}}{%
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/trw/emit}}
		\hfill
	}
	\legend{Эллипсами обозначены области, в которых находится \(3\sigma\) частиц}
	\caption{Поперечное распределение частиц в пучке на выходе из гибридного
		ускорителя\label{fig:hyb/sec/tr/beam}}
\end{figure}
%
\begin{figure}[htbp]
	\centerfloat{%
		\includegraphics[width=0.6\columnwidth]{plot/hybrid/beam/trw/energy}
	}
	\caption{Распределение частиц по энергии на выходе из гибридного
		ускорителя\label{fig:hyb/sec/tr/energy}}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
