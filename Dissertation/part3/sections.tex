\section{Определение параметров секций ускорителя\label{sec:hyb/reg}}

Ускоритель рассчитывался на среднюю энергию пучка~\SI{10}{\MeV} в диапазоне
импульсного тока пучка \SIrange{0.1}{0.5}{\A} на рабочей частоте~\SI{2856}{\MHz}.
Длина секции на~БВ ограничена величиной \SI{1.5}{\m}.
ЭДХ ускоряющих ячеек, использованные в расчётах, приведены в \cref{tab:hyb/reg/cells}.
% Чертежи ячеек приведены в \cref{app:draw}.
%
\begin{table}[htbp]
	\centerfloat{%
		\begin{threeparttable}
			\caption{ЭДХ ускоряющих ячеек\label{tab:hyb/reg/cells}}
			\begin{tabular}{lcc}
				\toprule
				\multicolumn{1}{c}{Параметр}                                                           & {КДВ-М}   & {БУС}      \\
				\midrule
				Шунтовое сопротивление \(r_{\textup{ш}}\), \si{\MOhm\per\m}
				                                                                                       & \num{100} & "---       \\
				Шунтовое эффективное сопротивление \(r_{\textup{ш}}^{\textup{эфф}}\), \si{\MOhm\per\m} & "---      & \num{75}   \\
				Период структуры \(D\), \si{\mm}                                                       & \num{33}  & \num{58}   \\
				Затухание \(\alpha\), \si{\neper\per\m}                                                & \num{0,3} & "---       \\
				Собственная добротность \(Q_0\)                                                        & "---      & \num{13e3} \\
				\bottomrule
			\end{tabular}
		\end{threeparttable}
	}
\end{table}

Очевидно, что величина набранной пучком в ускорителе энергии составляется из энергий,
набранных в секциях на~СВ и~БВ
%
\begin{equation}\label{eq:hyb/reg/Wbal}
	\Delta W_{\Sigma} = \Delta W_{\textup{СВ}} + \Delta W_{\textup{БВ}}.
\end{equation}

Фиксируя суммарный прирост энергии в ускорителе \(\Delta W_{\Sigma}\) определим возможные
соотношения прироста энергии в секциях \(\Delta W_{\textup{СВ}}/\Delta W_{\textup{БВ}}\).
Для этого, используя аналитические выражения, определим зависимость прироста энергии частиц в
секциях от количества ускоряющих ячеек.
Эти зависимости представлены на \cref{fig:hyb/reg/dW}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\subcaptionbox{Секция на~СВ\label{fig:hyb/reg/dW-stw}}{%
			\hfill
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_st}}{}
			\input{images/plot/hybrid/sections_tr_st.tikz}
		}
		\hfill
		\subcaptionbox{Секция на~БВ\label{fig:hyb/reg/dW-trw}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_tr}}{}
			\input{images/plot/hybrid/sections_tr_tr.tikz}
		}
		\hfill
	}
	\caption{Зависимость прироста энергии пучка от количества ячеек в секциях для различных
		величин тока ускоряемого пучка\label{fig:hyb/reg/dW}}
\end{figure}

Зависимость для секции на~СВ на \cref{fig:hyb/reg/dW-stw} определена при помощи
выражения~\cref{eq:eq/cav/Vc} параграфа~\cref{sec:eq/cav}.
Значение коэффициента связи секции определено при помощи выражения~\cref{eq:pow/opt/optbeta}.
Мощность, подводимая к секции на~СВ, определена при помощи выражения~\cref{eq:hyb/reg/Pst},
полученного путём подстановки~\cref{eq:pow/opt/optbeta} в~\cref{eq:eq/cav/Vg}
%
\begin{equation}\label{eq:hyb/reg/Pst}
	P^{\textup{СВ}}_{\textup{вх опт}} =
	\frac{E_{\textup{ус}}^2 n D}{2 r_{\textup{ш}}^{\textup{эфф}}}
	\left(1 +
		\sqrt{1+\left(\frac{I_{\textup{п}} r_{\textup{ш}}^{\textup{эфф}}}{E_{\textup{ус}}}\right)^2}
	\right),
\end{equation}
%
где \(E_{\textup{ус}}\) "--- напряжённость поля в ячейках секции в отсутствии пучка,
\(n\) "--- количество ячеек секции,
\(D\) "--- их период.

Зависимость набора энергии в секции на~БВ с постоянным импедансом от её длины
\cref{fig:hyb/reg/dW-trw} получена при помощи выражения
%
\begin{equation}\label{eq:hyb/reg/v}
	V =
	\sqrt{\frac{2 R_{\textup{ш}} P_0}{\tau}} \left(1-e^{-\tau}\right)-
	R_{\textup{ш}} I_0 \left(1-\frac{1-e^{-\tau}}{\tau}\right),
\end{equation}
%
где \(V\) "--- ускоряющее напряжение в секции на~БВ;
% \(\eta\) "--- КПД секции;
% \(V_{\textup{г}}\) "--- напряжение в секции, индуцированное генератором;
% \(V_{\textup{п}}\) "--- напряжение в секции, индуцированное пролетающим пучком частиц;
\(R_{\textup{ш}}\) "--- шунтовое сопротивление ячеек;
\(P_0\) "--- входящая СВЧ мощность;
\(\tau = \alpha L\) "--- затухание мощности в секции длиной \(L\);
\(I_0\) "--- импульсный ток пучка~\cite{perry, sobenin-acc-st}.

Для каждого значения тока пучка уровень подводимой к секции на~БВ СВЧ мощности выбирался
исходя из условий максимизации КПД секции и сохранения баланса
энергий~\cref{eq:hyb/reg/Wbal} путём вариации количества ячеек в секциях~\cite{sobenin-acc-st}.
Как видно из \cref{fig:hyb/reg/dW}, при оптимизации секций для получения максимального КПД
увеличение тока пучка снижает темп ускорения.
Кроме того, больший ток пучка требует большей подводимой СВЧ мощности.
Зависимость заданного уровня входной мощности от тока пучка представлена на
\cref{fig:hyb/reg/Pin}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_P_vs_I}}{}%
		\input{images/plot/hybrid/sections_tr_P_vs_I.tikz}
	}
	\caption{Зависимость уровня подводимой к гибридному ускорителю СВЧ мощности от тока
		ускоряемого пучка\label{fig:hyb/reg/Pin}}
\end{figure}

На \cref{fig:hyb/reg/n_sw_tr} изображена зависимость количества ячеек секции на~БВ от
количества ячеек секции на~СВ, при которой суммарный прирост энергии в ускорителе будет
равен~\SI{10,1(1)}{\MeV}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_st_vs_tr}}{}%
		\input{images/plot/hybrid/sections_tr_st_vs_tr.tikz}
	}
	\caption{Зависимость количества ячеек секции на~БВ от
		количества ячеек секции на~СВ гибридного ускорителя, при которых соблюдается баланс
		мощностей для различных величин тока ускоряемого пучка\label{fig:hyb/reg/n_sw_tr}}
\end{figure}

Рассмотрим баланс мощности в секциях
%
\begin{equation}\label{eq:hyb/reg/Pbal}
	\begin{cases}
		P_{\textup{вх}} = P^{\textup{БВ}}_{\textup{пот}} + P^{\textup{БВ}}_{\textup{пуч}} +
		P^{\textup{БВ}}_{\textup{вых}}; \\
		P^{\textup{БВ}}_{\textup{вых}} = P^{\textup{СВ}}_{\textup{пот}} + P^{\textup{СВ}}_{\textup{пуч}} + P_{\textup{наг}}.
	\end{cases}
\end{equation}
%
\(P_{\textup{вх}}\) "--- мощность СВЧ генератора, питающего ускоритель.
В секции на~БВ часть мощности \(P^{\textup{БВ}}_{\textup{пот}}\) теряется в стенках ячеек,
\(P^{\textup{БВ}}_{\textup{пуч}}\) расходуется на ускорение пучка частиц.
Неизрасходованная в секции на~БВ мощность \(P^{\textup{БВ}}_{\textup{вых}}\) делится между секцией на~СВ
и поглощающей нагрузкой.
В секции на~СВ \(P^{\textup{СВ}}_{\textup{пот}}\) теряется в стенках ячеек,
\(P^{\textup{СВ}}_{\textup{пуч}}\) расходуется на ускорение пучка.
\(P_{\textup{наг}}\) рассеивается в поглощающей нагрузке.

% \begin{equation}\label{eq:hyb/reg/eff}
% 	\eta = \frac{V I_0}{P} \sqrt{\frac{2 r_{\textup{ш}} L}{P_0}} \sqrt{\frac{2}{\alpha L}}
% 	\left(1-e^{-\alpha L}\right) I_0-
% 	\frac{r_{\textup{ш}} L}{P_0} \left(1-\frac{1-e^{\alpha l}}{\alpha L}\right) I_0^2
% \end{equation}

% \begin{figure}[htbp]
% 	\centerfloat{
% 		\hfill
% 		\subcaptionbox{}{%
% 						\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{cells_rsh}}}{}%
% \input{images/plot/hybrid/cells_rsh}.tikz}
% 		\hfill
% 		\subcaptionbox{}{%
% 						\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{cells_alpha}}}{}%
% \input{images/plot/hybrid/cells_alpha}.tikz}
% 		\hfill \\
% 		\subcaptionbox{}{%
%   \ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{cells_E-lambda-p}}}{}%
% \input{images/plot/hybrid/cells_E-lambda-p}.tikz}
% }
% 	}
%          \legend{Набор энергии в секции (а), КПД (б)}
% \caption{Параметры ячеек\label{fig:hyb/reg/cells}}
% \end{figure}

% На \cref{fig:hyb/reg/coup} изображены зависимости набора энергии пучком и КПД секции
% от значения коэффициента связи ячеек.

% \begin{figure}[htbp]
% 	{\centering
% 		\hfill
% 		\subcaptionbox{\label{fig:hyb/reg/coup-dW}}{%
%   \ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{dW_l_a}}}{}%
% \input{images/plot/hybrid/dW_l_a}.tikz}
% 		\hfill
% 		\subcaptionbox{\label{fig:hyb/reg/coup-eff}}{%
%   \ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{eff_l_a}}}{}%
%         \input{images/plot/hybrid/eff_l_a}.tikz}
% 		\hfill
% 	}
% 	\legend{Набор энергии в секции (а), КПД (б)}
% 	\caption{Аналитические зависимости параметров секции на~БВ от значения коэффициента связи ячеек\label{fig:hyb/reg/coup}}
% \end{figure}

% Данные выражения можно использовать для графического анализа параметров секции на~БВ и выбора
% рабочего режима ускорителя.
% Стоит заострить внимание на том факте, что вышеуказанные формулы предназначены для расчёта ускорителя
% на~БВ и не подразумевают наличие секции на~СВ\@.
% Таким образом, полученные данные аналитического анализа надо рассматривать лишь в качестве опорных
% данных для дальнейшего анализа.
% На \cref{fig:hyb/reg/curr} изображены графики вышеуказанных выражений в виде функций от длины
% секции на~БВ для различных значений тока пучка.

Для указанных на \cref{fig:hyb/reg/n_sw_tr} соотношений количества ячеек секций можно определить
мощность, потребляемую каждой из секций.
На \cref{fig:hyb/reg/P} изображены величины мощности, расходуемой в секциях на~СВ и~БВ
с учётом нагрузки током в зависимости от количества ячеек в секции на~СВ\@.
Разница между расходуемыми в секциях мощностями рассеивается в поглощающей нагрузке.
Скачки зависимости \cref{fig:hyb/reg/P-load} объясняются тем, что секции могут состоять
только из целого количества ячеек.
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Секция на~СВ\label{fig:hyb/reg/P-stw}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_pst}}{}
			\input{images/plot/hybrid/sections_tr_pst.tikz}
		}
		\hfill
		\subcaptionbox{Секция на~БВ\label{fig:hyb/reg/P-trw}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_ptr}}{}
			\input{images/plot/hybrid/sections_tr_ptr.tikz}
		}
		\hfill
	} \par
	\centerfloat{%
		\subcaptionbox{Поглощающая нагрузка\label{fig:hyb/reg/P-load}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_load}}{}
			\input{images/plot/hybrid/sections_tr_load.tikz}
		}
	}
	\caption{Зависимость расходуемой в секциях гибридного ускорителя СВЧ мощности от
		количества ячеек в секции на~СВ для различных величин тока
		ускоряемого пучка\label{fig:hyb/reg/P}}
\end{figure}

Соотношение рассеиваемых в секциях на~СВ и~БВ мощностей, представленных на
\cref{fig:hyb/reg/P}, необходимо обеспечить при помощи системы СВЧ питания по одной из
рассмотренных в параграфе~\cref{sec:hyb/scheme} схем.
На \cref{fig:hyb/reg/eff} изображена зависимость эффективности использования СВЧ мощности
в стационарном режиме от количества ячеек секций при различных величинах импульсного тока пучка.
При расчётах использовалось соотношение между количеством ячеек секций, представленное на
\cref{fig:hyb/reg/n_sw_tr}.
Пунктирными линиями обозначен КПД ускорителя на~БВ эквивалентной длины.
Как видно из представленной зависимости, гибридная схема ускорителя позволяет повысить
эффективность использования СВЧ мощности в стационарном режиме в сравнении со схемой на~БВ\@.
При этом КПД мало зависит от количества ячеек в секции на~СВ\@.
Это объясняется тем, что на суммарный КПД действуют два фактора: количество ячеек в секции на~СВ и
коэффициент затухания между секциями на~СВ и~БВ\@.
При изменении длины секции на~СВ соответствующее изменение коэффициента затухания между секциями
компенсирует изменение суммарного КПД ускорителя.
% Увеличение количества ячеек в секции на~СВ увеличивает суммарный КПД ускорителя и коэффициент
% затухания между секциями, который, в свою очередь, уменьшает суммарный КПД\@.
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_eta}}{}%
		\input{images/plot/hybrid/sections_tr_eta.tikz}
	}
	\legend{Пунктирными линиями обозначен КПД ускорителя на~БВ эквивалентной длины}
	\caption{Зависимость эффективности использования СВЧ мощности в
		гибридном ускорителе без циркулятора в стационарном режиме от количества ячеек в
		секции на~СВ для различных величин тока ускоряемого пучка\label{fig:hyb/reg/eff}}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
