\subsection{Секция на стоячей волне\label{sec:hyb/sec/st}}

Пучок электронов из термоэлектронной пушки попадает в группирователь, в котором формируются сгустки
для дальнейшего резонансного ускорения.
Группирователь на~СВ работает по принципу волноводной группировки: фазовая скорость и амплитуда
волны в устройстве подбирается таким образом чтобы фазовые колебания электронов затухали,
приближаясь к равновесному значению~\cite{valdner_linacs}.
Оптимальные параметры группировки можно получить, монотонно увеличивая фазовую скорость и амплитуду
волны~\cite{masunov-current-load}.

Первоначальные значения фазовой скорости ячеек группирователя и амплитуды волны получены путём
дискретизация фазовой скорости ячеек (\cref{fig:hyb/sec/st/group_vel}): в качестве фазовой
скорости волны в каждой ячейке группирователя использовано среднее арифметическое значение
средних фазовых скоростей пучка на входе и выходе ячейки.
Далее эти значения оптимизировались методом исследования пространства параметров.
Параметры настроенного группирователя представлены в \cref{tab:hyb/sec/st/gr_cell}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{group_vel}}{}%
		\input{images/scheme/hybrid/group_vel.tikz}
	}
	\legend{Сплошной линией обозначена средняя фазовая скорость пучка ускоряемых частиц,
		пунктирной "--- фазовые скорости волны в ячейках группирователя}
	\caption{Дискретизация фазовой скорости ячеек в группирователе\label{fig:hyb/sec/st/group_vel}}
\end{figure}

Моделирование динамики частиц в группирователе производилось при помощи
программы расчёта динамики в секциях на~СВ семейства программ
\textsc{Beamdulac}~\cite{beamd}.
Параметры пучка на выходе настроенного группирователя представлены в \cref{tab:hyb/sec/st/par}.
Энергетические параметры, поперечное распределение частиц и фазовый портрет пучка на выходе из
секции на~СВ представлены на
\cref{fig:hyb/sec/st/gamma, fig:hyb/sec/st/beam,
	fig:hyb/sec/st/energy}~\cite{authorscopHyb,authorscopHyb2}.
%
\begin{table}[htbp]
	\centerfloat{%
		\begin{threeparttable}
			\caption{Параметры пучка частиц на выходе из
				секции на~СВ\label{tab:hyb/sec/st/par}}
			\begin{tabular}{lS[table-parse-only]}
				\toprule
				\multicolumn{1}{c}{Параметр}         & Значение \\ \midrule
				Коэффициент захвата, \si{\percent}   & 39       \\
				Ток на выходе, \si{\A}               & 0,1      \\
				Средняя энергия на выходе, \si{\MeV} & 0,96     \\
				\(\alpha\)                           & -0,49    \\
				\(\beta, \si{\cm\per\radian}\)       & 3,46     \\
				\(\epsilon, \si{\cm\milli\radian}\)  & 6,2      \\
				\bottomrule
			\end{tabular}
		\end{threeparttable}
	}
\end{table}
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Распределение по фазе\label{fig:hyb/sec/st/gamma-phi}}{%
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/stw/gamma_over_phi}}
		\hfill
		\subcaptionbox{Распределение по продольной координате\label{fig:hyb/sec/st/gamma-l}}{%
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/stw/gamma_over_l}}
		\hfill
	}
	\caption{Энергетическое распределение частиц в пучке на выходе из секции
		на~СВ\label{fig:hyb/sec/st/gamma}}
\end{figure}
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Профиль пучка\label{fig:hyb/sec/st/beam-pr}}{%
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/stw/profile}}
		\hfill
		\subcaptionbox{Распределение частиц в пучке\label{fig:hyb/sec/st/beam-pr_hist}}{%
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/stw/profile_size}}
		\hfill
	} \par
	\centerfloat{
		\hfill
		\subcaptionbox{Фазовый портрет пучка\label{fig:hyb/sec/st/beam-emit}}{
			\includegraphics[width=0.48\columnwidth]{plot/hybrid/beam/stw/emit}}
		\hfill
	}
	\legend{Эллипсами обозначены области, в которых находится \(3\sigma\) частиц}
	\caption{Поперечное распределение частиц в пучке на выходе из секции
		на~СВ\label{fig:hyb/sec/st/beam}}
\end{figure}
%
\begin{figure}[htbp]
	\centerfloat{
		\includegraphics[width=0.6\columnwidth]{plot/hybrid/beam/stw/energy}
	}
	\caption{Распределение частиц по энергии на выходе из секции на~СВ\label{fig:hyb/sec/st/energy}}
\end{figure}

\FloatBarrier

\paragraph{Настройка макета группирователя}

С использованием полученных при моделировании динамики частиц значениях амплитуды поля и фазовой
скорости волны в ячейках группирователя настроена трёхмерная модель группирователя на~СВ\@.
В качестве УС использованы ячейки БУС\@.

Сначала ячейки группирователя были настроены индивидуально, с использованием макета, изображённого на
\cref{fig:hyb/sec/st/buncher-reg}.
Геометрические параметры макета обозначены на \cref{fig:hyb/sec/st/draw}.
Данный макет состоит из двух половин ускоряющей ячейки и целой ячейки связи.
Половины ускоряющих ячеек закорочены либо идеальным проводником электрического поля
(электрическое граничное условие), либо идеальным проводником магнитного поля (магнитное граничное условие).
Длины ячеек выбирались из выражения \(D = \theta c_0 \beta_{\textup{ф}} / 2 \pi f_0\),
где \(c_0\) "--- скорость света в вакууме,
\(\beta_{\textup{ф}}\) "--- относительная фазовая скорость волны на виде колебаний \(\theta\),
\(f_0\) "--- резонансная частота~\cite{sobenin-tech}.
%
\begin{figure}[htbp]
	\centerfloat{
		\hfill
		\subcaptionbox{Регулярная ячейка\label{fig:hyb/sec/st/buncher-reg}}{
			\includegraphics[height=0.2\textheight]{model/buncher_regular_cell}}
		\hfill
		\subcaptionbox{Две ячейки\label{fig:hyb/sec/st/buncher-two}}{
			\includegraphics[height=0.2\textheight]{model/buncher_two_cells}}
		\hfill
		\subcaptionbox{Крайняя ячейка\label{fig:hyb/sec/st/buncher-last}}{
			\includegraphics[height=0.2\textheight]{model/buncher_second_half_third_full}}
		\hfill
	} \par
	\centerfloat{
		\hfill
		\subcaptionbox{Группирователь\label{fig:hyb/sec/st/buncher-full}}{
			\includegraphics[width=0.4\columnwidth]{model/buncher_full_cells}}
		\hfill
		\subcaptionbox{Группирователь с вводом мощности\label{fig:hyb/sec/st/buncher-power}}{
			\includegraphics[width=0.4\columnwidth]{model/buncher_full_power}}
		\hfill
	}
	\caption{Сечения расчётных макетов ячеек БУС\label{fig:hyb/sec/st/buncher}}
\end{figure}
%
\begin{figure}[htbp]
	\centerfloat{
		\includegraphics[width=0.6\columnwidth]{model/cell_reg}
	}
	\caption{Основные размеры ячейки БУС\label{fig:hyb/sec/st/draw}}
\end{figure}
%
\begin{figure}[htbp]
	\centerfloat{%
		\hfill
		\subcaptionbox{Длина носиков\label{fig:hyb/sec/st/par_var-nl}}{
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{bus_nl}}{}%
			\input{images/plot/hybrid/bus_var_n_l.tikz}}
		\hfill
		\subcaptionbox{Угол раствора щелей связи\label{fig:hyb/sec/st/par_var-cw}}{
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{bus_cw}}{}%
			\input{images/plot/hybrid/bus_var_cw_theta.tikz}}
		\hfill
	}
	\caption{Зависимость шунтового эффективного сопротивления и добротности регулярных ячеек БУС
		от геометрических параметров\label{fig:hyb/sec/st/par_var}}
\end{figure}

Настройка ускоряющих ячеек производилась подстройкой их радиуса \texttt{Rr} с использованием
электрического граничного условия таким образом, чтобы резонансная частота рабочего вида колебаний
была равна рабочей частоте ускорителя.
Ячейки связи настраивались подстройкой их радиуса \texttt{Rc} с использованием магнитного граничного
условия таким образом, чтобы их резонансная частота была равна рабочей частоте ускорителя.
На \cref{fig:hyb/sec/st/par_var} представлены вариационные характеристики регулярной ячейки БУС\@.

После настройки регулярной ячейки настраивалась величина связи между ячейками группирователя.
Для этого использовался макет, представленный на \cref{fig:hyb/sec/st/buncher-two}.
Он отличается от макета, представленного на \cref{fig:hyb/sec/st/buncher-reg} тем, что в качестве
половин ускоряющих ячеек взяты ранее настроенные отдельно соседние ячейки группирователя.
Настройка связи осуществлялась изменением угла раствора щелей связи по магнитному полю
\texttt{cw\_theta}
таким образом, чтобы отношение амплитуд ускоряющего поля в ячейках соответствовало ранее
определённому значению.
При этом радиусы ускоряющих ячеек и ячеек связи подстраивались таким образом, чтобы частота ячеек
оставалась равной рабочей частоте ускорителя.

Крайние ячейки несимметричны относительно центральной плоскости, поэтому их частоты подстраивались с использованием
макета, изображённого на \cref{fig:hyb/sec/st/buncher-last}.

После настройки всех ячеек группирователя они были объединены в полный макет
(\cref{fig:hyb/sec/st/buncher-full}).
Частота основного вида колебаний настроенного макета равна рабочей частоте ускорителя,
фазовые скорости ячеек и относительные амплитуды колебаний в них соответствуют значениям, ранее полученным из расчёта
динамики частиц.
Распределение поля в группирователе представлено на \cref{fig:hyb/sec/st/buncher_field}.
%
\begin{figure}[htbp]
	\centering{
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{buncher_field}}{}%
		\input{images/plot/hybrid/buncher_field.tikz}
	}
	\caption{Распределение ускоряющего поля в группирователе\label{fig:hyb/sec/st/buncher_field}}
\end{figure}
%
\begin{table}[htbp]
	\centerfloat{
		\begin{threeparttable}
			\caption{ЭДХ ячеек секции на~СВ\label{tab:hyb/sec/st/gr_cell}}
			\begin{tabular}{lS[table-parse-only]S[table-parse-only]S[table-parse-only]S[table-parse-only]S[table-parse-only]}
				\toprule
				Параметр                                            & Ячейка~1 &
				Ячейка~2
				                                                    & Ячейка~3 & Регулярная~ячейка \\ \midrule
				\(\beta_{\textup{ф}}\)                              & 0.65     & 0.64
				                                                    & 0.99     & 1                 \\
				\(E_{\textup{ус}}, \si{\kV\per\cm}\)                & 80       & 140
				                                                    & 150      & 160               \\
				D, \si{\mm}                                         & 34       & 33
				                                                    & 52       & 52                \\
				f, \si{\MHz}                                        & 2855,97  & 2855.99
				                                                    & 2856.05  & 2856.0            \\
				\(Q_0\)                                             & 1.0e4    & 1.0e4
				                                                    & 1.6e4    & 1.6e4             \\
				\(r_{\textup{ш}}^{\textup{эфф}}, \si{\MOhm\per\m}\) & 28       & 25
				                                                    & 74       & 75                \\
				% \(r_{\textup{ш}}, \si{\MOhm\per\m}\)         & 43       & 40
				% & 77  & 77     \\
				T                                                   & 0.65     & 0.64
				                                                    & 0.85     & 0.8               \\
				\bottomrule
			\end{tabular}
		\end{threeparttable}
	}
\end{table}

Ввод мощности осуществляется в последнюю ячейку секции при помощи прямоугольного волновода.
Для настройки ввода мощности использовался макет, изображённый на \cref{fig:hyb/sec/st/buncher-power}.
Требуемый коэффициент связи секции с волноводом \(\chi = \num{2.6}\), определённый при помощи
выражения~\cref{eq:pow/opt/optbeta}, настраивался изменением ширины щели связи волновода с
ячейкой.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
