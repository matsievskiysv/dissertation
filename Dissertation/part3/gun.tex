\subsection{Электронная пушка\label{sec:hyb/gun}}

Пучок электронов низкой энергии для последующего ускорения формируется при помощи
электронных пушек.
Самое большое распространение в настоящее время получили фото- и термоэмиссионные
электронные пушки.
Фотоэмиссионные пушки конструктивно сложны и используются в основном в
исследовательских установках, для которых критическими параметрами являются
яркость и эмиттанс пучка; длительность сгустка электронной пушки~\cite{guns-heph}.
В промышленности наиболее часто используются электронные пушки, основанные на
механизме термоэлектронной эмиссии~\cite{raizer}.
Такие пушки относительно дёшевы и просты в эксплуатации; использование управляющих
электродов и сеток позволяет изменять выходной ток пучка широком
диапазоне~\cite{SHVEDUNOV201995}.

% Важными характеристиками электронных пушек являются ток и первеанс
% пучка~\cite{alamovskiy}.
% Первеанс определяется как \(P = I/U^{3/2}\), где \(I\) "--- ток
% пучка, \(U\) "--- напряжение между электродами.

Пучки заряженных частиц в фазовом пространстве обычно характеризуются
эллипсами~\cite{LebedevShalnov}.
Такой эллипс изображён на \cref{fig:hyb/gun/ellips}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{ellips}}{}%
		\input{images/scheme/hybrid/ellips.tikz}
	}
	\legend{Цифрами обозначены особые точки эллипса}
	\caption{Представление пучка в виде параметризованного эллипса в фазовом
		пространстве\label{fig:hyb/gun/ellips}}
\end{figure}

Из координат особых точек эллипса можно найти параметры Твисса~\cite{twiss},
описывающие данный эллипс
%
\begin{subequations}\label{eq:eq/stationary/twiss}
	\begin{align}
		\alpha & = -\frac{\dot x_2}{\dot x_4}; \\
		\beta  & = \frac{\dot x_3}{x_1};       \\
		\gamma & = \frac{\dot x_3}{x_1},
	\end{align}
\end{subequations}
%
и эмиттанс пучка
%
\begin{equation}\label{eq:eq/stationary/emit}
	\varepsilon = x_2 \dot x_4.
\end{equation}
%
Параметры эллипса, изображённые на \cref{fig:hyb/gun/ellips},
связаны с параметрами Твисса следующими выражениями:
%
\begin{subequations}\label{eq:eq/stationary/params}
	\begin{align}
		x_m      & = \sqrt{\varepsilon \beta};  \\
		\theta_m & = \sqrt{\varepsilon \gamma}; \\
		\chi     & = -\arctan{\alpha}.
	\end{align}
\end{subequations}

Выбор рабочего напряжения электронной пушки обуславливается типом используемого группирователя.
Группирователи на~СВ позволяют инжектировать более низкоэнергетичный пучок в сравнении с
группирователями на~БВ\@.
Высокая энергия инжекции упрощает настройку группирователя, но требует более дорогих, сложных в
эксплуатации и громоздких источников постоянного напряжения.

Проведена оптимизация геометрических параметров трёхэлектродной электронной пушки с напряжением
между анодом и катодом~\SI{30}{\kV}~\cite{authorscopGun}.
Это напряжение обеспечивает достаточную для группировки пучка энергию.
Целью оптимизации являлась настройка положения кроссовера пучка до его совпадения с
геометрическим центром первой ячейки группирователя, а также минимизация сечения
пучка электронов этой точке.
Параметрами оптимизации служили радиус скругления катода пушки, форма управляющего
электрода и его потенциал.
Модель оптимизированной трёхэлектродной электронной пушки представлена на \cref{fig:hyb/gun/gun}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\includegraphics[width=0.5\columnwidth]{model/gun}
	}
	\legend{Стрелкой обозначена область катода, с которой происходит
		эмиссия~частиц}
	\caption{Сечение модели трёхэлектродной электронной пушки\label{fig:hyb/gun/gun}}
\end{figure}

Траектории движения частиц в электронной пушке, рассчитанные при помощи программы
\textsc{egun}~\cite{egun}, представлены на \cref{fig:hyb/gun/trj}.
%  \textsc{operafea}~\cite{operafea}~\cref{fig:hyb/gun/trj-opera}
%
\begin{figure}[htbp]
	\centerfloat{%
		% \subcaptionbox{egun\label{fig:hyb/gun/trj-egun}}{%
		\includegraphics[height=0.15\textheight]{hybrid/gun_egun}
		% } \\
		% \subcaptionbox{parmela\label{fig:hyb/gun/trj-parmela}}{%
		% 	\includegraphics[height=0.15\textheight]{beam/parmela_profile_long}}
		% \subcaptionbox{opera-fea\label{fig:hyb/gun/trj-opera}}{%
		% 	\includegraphics[width=0.8\columnwidth]{beam/gun_opera}} \\
	}
	\caption{Траектории движения частиц в электронной пушке\label{fig:hyb/gun/trj}}
\end{figure}

На \cref{fig:hyb/gun/current} представлена зависимость тока пучка электронов
от напряжения на управляющем электроде.
Исходя из коэффициента захвата группирователя, расчёт которого представлен в
параграфе~\cref{sec:hyb/sec/st},
выбрана рабочая точка, соответствующая току электронного пучка
\SI{0.25}{\A}.
Рассчитанные параметры пучка электронной пушки в рабочем режиме представлены в
\cref{tab:hyb/gun_par}.
% фазовый портрет и профиль пучка \textsc{egun} "--- на
% \cref{fig:hyb/gun/egun}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{gun_current}}{}%
		\input{images/plot/gun/gun_current.tikz}
	}
	\caption{Зависимость тока пучка электронной пушки от напряжения управляющего
		электрода\label{fig:hyb/gun/current}}
\end{figure}
%
\begin{table}[htbp]
	\centerfloat{%
		\captionsetup{justification=centering}
		\caption{Параметры пучка электронной пушки\label{tab:hyb/gun_par}}
		\begin{tabular}{lS[table-parse-only]}
			\toprule
			\multicolumn{1}{c}{Параметр}        & Значение \\ \midrule
			\(\alpha\)                          & -0.97    \\
			\(\beta, \si{\cm\per\radian}\)      & 6.45     \\
			\(\epsilon, \si{\cm\milli\radian}\) & 16.2     \\
			\(I, \si{\A}\)                      & 0.25     \\
			\bottomrule
		\end{tabular}
	}
\end{table}

% \begin{figure}[htbp]
% 	\centerfloat{%
% 		\hfill
% 		\subcaptionbox{Фазовый портрет на выходе устройства}{%
% 			\includegraphics[height=0.3\textheight]{gun/egun_emit}}
% 		\hfill
% 		\subcaptionbox{Эволюция профиля пучка вдоль оси устройства}{%
% 			\includegraphics[height=0.3\textheight]{gun/egun_profile}}
% 		\hfill
% 	}
% 	\caption{Параметры пучка электронной пушки\label{fig:hyb/gun/egun}}
% \end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
