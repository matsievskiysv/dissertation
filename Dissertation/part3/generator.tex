\section{Устойчивость работы генератора СВЧ\label{sec:hyb/gen}}

Для питания ЛУЭ используют магнетроны и клистроны.
Выходная частота клистронных генераторов определяется внешним задающим генератором.
Магнетронные генераторы являются автогенераторами и требуют стабилизации
режима работы за счёт подбора параметров нагрузки~\cite{Polovkov}.
Несмотря на это, магнетронные генераторы широко используются из-за высокого КПД и меньшей
стоимости~\cite{Zhou:SRF2017-TUPB099}.
Оба типа генераторов требуют согласования нагрузки.
Для большинства серийно производимых мощных клистронов и магнетронов на частоте 2856\,МГц
максимальный допустимый уровень КСВН составляет \num{2,1}~\cite{CanonETD}.
% что эквивалентно максимально допустимому модулю отражения от устройства
% \SI{-20,9}{\dB}

Условием устойчивой работы магнетронного генератора является единственное решение на рабочей частоте
секции уравнения генерации "--- баланса реактивных проводимостей генератора~\(B_{\textup{г}}\) и
ускоряющей секции~\(B_{\textup{у}}\)~\cite{Lebedev}.
%
\begin{equation}\label{eq:hyb/gen/gen}
	B_{\textup{г}} + B_{\textup{у}} = 0.
\end{equation}
%
Комплексная проводимость генератора может быть выражена как
%
\begin{subequations}\label{eq:hyb/gen/Bg}
	\begin{align}
		B_{\textup{г}}  & = 2 \frac{f-f_0}{f_{0\textup{г}}} Q_{\textup{вн}}; \\
		Q_{\textup{вн}} & =  0,417 \frac{f_0}{F_0},
	\end{align}
\end{subequations}
%
где \(f_0\) и \(f_{0\textup{г}}\) "--- резонансные частоты секции и генератора соответственно,
\(F_0\) "--- коэффициент затягивания частоты генератора~\cite{KaminskiyDD}.
Для нахождения мнимой проводимости секций используется выражение~\cref{eq:eq/zin/c}
параграфа~\cref{sec:eq/zin}.

На \cref{fig:hyb/reg/stability} приведены типичные характеристики устойчивости магнетронного
генератора.
Красными точками обозначены устойчивые состояния системы, в которых реализуется
условие~\cref{eq:hyb/gen/gen}.
На \cref{fig:hyb/reg/stability-st} представлена характеристика стабильности одночастного режима
работы магнетронного генератора.
В этом режиме случайные изменения частоты генерируемых СВЧ колебаний \(\Delta f\) приведут к
увеличению реактивной проводимости \(b\), в свою очередь уменьшающей изначальное изменение частоты
\(\Delta f\)~\cite{Polovkov}.
Нагрузка обеспечивает отрицательную обратную связь, стабилизируя выходной сигнал автогенератора.
На \cref{fig:hyb/reg/stability-ust} представлена характеристика многочастотного режима работы
магнетронного генератора.
В случае, представленном на рисунке, система имеет два нестабильных и три стабильных равновесных
состояния.
При случайном отклонении частоты СВЧ колебаний может произойти переход от одного стабильного
состояния к другому.
Очевидно, что для узкополосных устройств, таких как секции на~СВ, такое изменение частоты
недопустимо.
Для устойчивой работы ускорителя с питанием от автогенератора требуется установление одночастотного
режима работы системы.
%
\begin{figure}[htbp]
	\centerfloat{%
		\subcaptionbox{Одночастотный режим\label{fig:hyb/reg/stability-st}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{stability-st}}{}
			\input{images/plot/hybrid/stability_stable.tikz}
		}
		\hfill
		\subcaptionbox{Многочастотный режим\label{fig:hyb/reg/stability-ust}}{%
			\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{stability-ust}}{}
			\input{images/plot/hybrid/stability_unstable.tikz}
		}
	}
	\legend{Кругами обозначены устойчивые равновесные состояния системы,
		треугольниками "--- неустойчивые}
	\caption{Характеристика стабильности работы магнетрона\label{fig:hyb/reg/stability}}
\end{figure}

На \cref{fig:hyb/reg/stability_ui} изображён интерфейс модуля расчёта режимов работы
магнетрона программы \textsc{LinacCalc}.
Для расчёта каскадного соединения секций использован модуль задания нагрузки секции, интерфейс
которого изображён на \cref{fig:hyb/reg/load_ui}.
Величина необходимого затухания мощности между секциями, определённая в параграфе~\cref{sec:hyb/reg},
представлена на \cref{fig:hyb/reg/Pfrac_vs_n}.
Для моделирования соединения секций использованы выражения, представленные в
параграфе~\cref{sec:eq/waveguide}.
%
\begin{figure}[htbp]
	\centerfloat{%
		\includegraphics[width=\columnwidth]{linaccalc/stability}
	}
	\caption{Интерфейс модуля расчёта режимов работы магнетронного
		генератора программы \textsc{LinacCalc}\label{fig:hyb/reg/stability_ui}}
\end{figure}
%
\begin{figure}[htbp]
	\centerfloat{%
		\includegraphics[width=\columnwidth]{linaccalc/input_load}
	}
	\caption{Интерфейс модуля задания нагрузки секции программы
		\textsc{LinacCalc}\label{fig:hyb/reg/load_ui}}
\end{figure}
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{sections_tr_frac_db}}{}%
		\input{images/plot/hybrid/sections_tr_frac_db.tikz}
	}
	\caption{Зависимость отношения входной мощности секции на~СВ к выходной
		мощности секции на~БВ от количества ячеек в секции на~СВ гибридного
		ускорителя для различных величин тока ускоряемого
		пучка\label{fig:hyb/reg/Pfrac_vs_n}}
\end{figure}

Для реализации одночастотного режима работы ускорителя требуется изоляция секций СВ и~БВ на уровне
не меньше, чем~\SI{14}{\dB}.
Характеристика устойчивости при данном уровне затухания между секциями представлена на
\cref{fig:hyb/reg/stability-st}.
При таком уровне развязки между секциями увеличение КПД по сравнению со схемой на~БВ незначительно,
и использование усложнённой схемы питания необоснованно.
Таким образом, для данной схемы питания ускорителя без циркулятора в качестве генератора требуется
использовать клистрон.

% Для секции на~СВ, используя выражения~\cref{eq:pow/opt/relation} параграфа~\cref{sec:eq/cav}, можно
% определить оптимальную связь с подводящей линией для заданного импульсного значения тока пучка.

% Полученные опорные значения параметров секций на~СВ и~БВ указаны в \cref{tab:hyb/reg/acc_par}.

% \begin{table}[hbtp]
% 	\caption{Входные и выходные параметры аналитического анализа
% 		секций\label{tab:hyb/reg/acc_par}}
% 	\centerfloat{
% 		\begin{tabular}{lc}
% 			\toprule
% 			\textbf{Параметр} & \textbf{Значение} \\ \midrule
% 			\(W_{\max}\), МэВ     & 10                \\
% 			\(P_{in}\), МВ     & 5,5                \\
% 			% \(\beta_\varphi^{\textup{рег}}\)   & 0,999             \\
% 			\(f_0\), МГц        & 2856              \\
% 			\(L^{\textup{БВ}}\), м            & 1,4               \\
% 			\(I_0\), A          & 0,35              \\
% 			\(\chi^{\textup{СВ}}\)          & 0,4               \\
% 			\bottomrule
% 		\end{tabular}
% 	}
% \end{table}
%
\begin{figure}[htbp]
	\centerfloat{%
		\ifdefmacro{\tikzsetnextfilename}{\tikzsetnextfilename{conn_s11}}{}%
		\input{images/plot/hybrid/conn_s11.tikz}
	}
	\legend{Горизонтальной линией обозначен уровень \(\textup{КСВН}=1,2\)}
	\caption{Зависимость модуля отражения гибридного ускорителя без циркулятора от
		количества ячеек в секции на~СВ для различных величин тока ускоряемого
		пучка\label{fig:hyb/reg/s11_vs_n}}
\end{figure}

Полученная зависимость отражения мощности от входного порта ускорителя от количества ячеек в секциях
представлена на \cref{fig:hyb/reg/s11_vs_n}.
Как видно из представленной зависимости, отражение от устройства меньше предельного значения
реализуется только для малого количества ячеек на~СВ\@.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertation"
%%% End:
