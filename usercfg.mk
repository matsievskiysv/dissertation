### Пользовательские настройки
# Откомментируйте настройки, чтобы использовать их по умолчанию

# MKRC ?= latexmkrc # config file
# SOURCE ?= dissertation # source .tex file
BACKEND ?= -pdflua
## -pdf=pdflatex
## -pdfdvi=pdflatex with dvi
## -pdfps=pdflatex with ps
## -pdfxe=xelatex with dvi (faster than -xelatex)
## -xelatex=xelatex without dvi
## -pdflua=lualatex with dvi  (faster than -lualatex)
## -lualatex=lualatex without dvi

# DRAFTON ?= # 1=on;0=off
SHOWMARKUP ?= 1 # 1=on;0=off
FONTFAMILY ?= 2 # 0=CMU;1=MS fonts;2=Liberation fonts
# ALTFONT ?= # 0=Computer Modern;1=pscyr;2=XCharter
# USEBIBER ?= # 0=bibtex8;1=biber
# USEFOOTCITE ?= # 0=no;1=yes
BIBGROUPED ?= 0 # 0=no;1=yes
IMGCOMPILE ?= 1 # 1=on;0=off
NOTESON ?= 2 # 0=off;1=on, separate slide;2=on, same slide
LATEXFLAGS ?= -halt-on-error -file-line-error
LATEXMKFLAGS ?= -silent
BIBERFLAGS ?= --fixinits --isbn-normalise
REGEXDIRS ?= . Dissertation Dissertation/part1 Dissertation/part2 Dissertation/part3 Dissertation/part4 Synopsis Presentation common images/cache # distclean dirs
TIMERON ?= 1 # show CPU usage

### Пользовательские правила

### Откоментируйте и измените эти строки, чтобы задать файлы для форматирования
INDENT_FILES += $(wildcard Dissertation/part1/*.tex)
INDENT_FILES += $(wildcard Dissertation/part2/*.tex)
INDENT_FILES += $(wildcard Dissertation/part3/*.tex)
INDENT_FILES += $(wildcard Dissertation/part4/*.tex)
INDENT_FILES += Synopsis/content.tex
INDENT_FILES += Presentation/preamble.tex
INDENT_FILES += $(wildcard Presentation/part*.tex)
INDENT_FILES += Presentation/conclusion.tex
INDENT_FILES += common/characteristic.tex
INDENT_FILES += common/concl.tex
