#!/bin/bash

target=$@
EXT=(".png" ".jpg" ".jpeg")

if [ "$target" == "" ]; then
    for i in ${EXT[*]}; do
	target+=$(find . -iname "*$i")
    done
fi

for i in $target; do
    echo $(basename $i)
    convert -define trim:percent-background=0% $i -trim $i
done
