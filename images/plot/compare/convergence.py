#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib

FILE = "convergence"
YLAB = r"$\chi$"
XLAB = r"Количество элементов сетки"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

mesh = pd.read_table(FILE + "_mesh.txt", sep='\s+',
                     skiprows=[0, 1], names=["mc", "mesh"])
chi = pd.read_table(FILE + "_chi.txt", sep='\s+',
                    skiprows=[0, 1], names=["mc", "chi"])
data = mesh.merge(chi, on="mc")


plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.mesh, data.chi, linewidth=LINEWIDTH)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
