#!/usr/bin/python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tikzplotlib

YLAB = r"$f, \si{\MHz}$"
XLAB = r"\theta, \si{\degree}"
LINEWIDTH = 4
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2
BASENAME = "dispersion_defl_big"

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)


cst = pd.read_table(BASENAME + "_cst.txt", sep='\s+',
                    comment="#", names=["phi", "f"])

cst = cst[1:]

phi = np.linspace(0.1, np.pi, 101)
Ke1 = 0.793
Ke2 = 0.0
Kh1 = -0.821
Kh2 = -0.0

d1 = -10
d2 = 2997

f1 = d1+d2*np.sqrt((1 - Ke1*np.cos(phi) - Ke2*np.cos(2*phi)) /
                   (1 + Kh1*np.cos(phi) + Kh2*np.cos(2*phi)))

f11 = d1+d2*np.sqrt((1 - Ke1*np.cos(cst.phi/180*np.pi) - Ke2*np.cos(2*cst.phi/180*np.pi)) /
                    (1 + Kh1*np.cos(cst.phi/180*np.pi) + Kh2*np.cos(2*cst.phi/180*np.pi)))

Ke1 = 0.786
Ke2 = 0.0
Kh1 = -0.821
Kh2 = -0.0
Ke2 = 0.007
Kh2 = -0.000

f2 = d1+d2*np.sqrt((1 - Ke1*np.cos(phi) - Ke2*np.cos(2*phi)) /
                   (1 + Kh1*np.cos(phi) + Kh2*np.cos(2*phi)))

f22 = d1+d2*np.sqrt((1 - Ke1*np.cos(cst.phi/180*np.pi) - Ke2*np.cos(2*cst.phi/180*np.pi)) /
                    (1 + Kh1*np.cos(cst.phi/180*np.pi) + Kh2*np.cos(2*cst.phi/180*np.pi)))

plt.figure(figsize=FIGSIZE, dpi=DPI)

plt.plot(cst.phi, cst.f, linewidth=LINEWIDTH, linestyle="--")
plt.plot(phi/np.pi*180, f1, linewidth=LINEWIDTH, linestyle=":")
plt.plot(phi/np.pi*180, f2, linewidth=LINEWIDTH, linestyle="-.")
# plt.xlim(5, 180)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
# plt.legend(["МКЭ", "МЭС, без учёта", "МЭС, с учётом"],
#            loc='upper center', bbox_to_anchor=(0.5, -0.45),
#            fancybox=True, shadow=True)
plt.legend(["МКЭ", "МЭС, без учёта", "МЭС, с учётом"])
tikzplotlib.save(BASENAME + ".tikz", axis_width="10cm", axis_height="7cm")
tikzplotlib.save(BASENAME + "_syn.tikz", axis_width="6.5cm", axis_height="4.5cm")
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)

plt.plot(cst.phi, cst.f, linewidth=LINEWIDTH)
plt.plot(phi/np.pi*180, f1, linewidth=LINEWIDTH)
plt.plot(phi/np.pi*180, f2, linewidth=LINEWIDTH)
# plt.xlim(5, 180)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
plt.legend(["МКЭ", "МЭС, без учёта связи через ячейку", "МЭС, с учётом связи через ячейку"],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True)
tikzplotlib.save(BASENAME + "_pres.tikz", axis_width="6.5cm", axis_height="4.5cm")
plt.close()

for f, name in [(f11, "without"), (f22, "with")]:
    delta = np.abs(f - cst.f)
    mean = np.mean(delta)
    print(name, mean, np.sqrt(np.sum((delta - mean) ** 2) / len(delta)))
