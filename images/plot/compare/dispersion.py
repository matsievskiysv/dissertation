#!/usr/bin/python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tikzplotlib
import re

YLAB = r"$f, \si{\MHz}$"
XLAB = r"\theta, \si{\degree}"
LINEWIDTH = 4
LINEWIDTHPRES = 2
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

begintikz = re.compile(r'begin\{tikzpicture\}')

for position, basename in [("right", "dispersion_defl"),
                           ("top", "dispersion_dlsm"),
                           ("top", "dispersion_dls")]:
    cst = pd.read_table(basename + "_cst.txt", sep='\s+',
                        comment="#", names=["phi", "f"])
    lc_lossless = pd.read_csv(basename + "_lossless.csv")
    lc_lossy = pd.read_csv(basename + "_lossy.csv")
    rg = np.where(np.diff(np.sign(np.diff(np.diff(lc_lossy["2"])))))[0]
    if rg.size == 3:
        lc_lossy = lc_lossy[rg[0]:rg[2]]
        lc_lossless = lc_lossless[rg[0]:rg[2]]
    elif rg.size == 2:
        lc_lossy = lc_lossy[rg[0]:rg[1]]
        lc_lossless = lc_lossless[rg[0]:rg[1]]

    lc_lossy = lc_lossy.reset_index(drop=True)
    lc_lossless = lc_lossless.reset_index(drop=True)
    # print((cst.phi >= lc_lossy.phi[0]) & (cst.phi <= lc_lossy.phi[len(lc_lossy.phi)-1]))
    cst = cst[((cst.phi <= 180*lc_lossy.phi[len(lc_lossy.phi)-1]) &
               (cst.phi >= 180*lc_lossy.phi[0]))]

    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.plot(cst.phi, cst.f, linewidth=LINEWIDTH, linestyle="--")
    plt.plot(lc_lossy.phi*180, lc_lossy["2"], linewidth=LINEWIDTH, linestyle="-.")
    plt.plot(lc_lossless.phi*180, lc_lossless["2"], linewidth=LINEWIDTH, linestyle=":")
    plt.xlabel(XLAB)
    plt.ylabel(YLAB)
    plt.grid()
    # plt.xscale('log')
    # plt.tight_layout()
    # plt.show()
    # plt.savefig(FILE + ".png")
    if position == "top":
        plt.legend(["МКЭ", "МЭС, с потерями", "МЭС, без потерь"],
                   loc='lower right', bbox_to_anchor=(1, 1.05),
                   fancybox=True, shadow=True)
    else:
        plt.legend(["МКЭ", "МЭС, с потерями", "МЭС, без потерь"],
                   loc='upper left', bbox_to_anchor=(1.02, 1),
                   fancybox=True, shadow=True)
    tikzplotlib.save(basename + ".tikz", axis_width="7cm", axis_height="5cm")
    plt.close()

    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.plot(cst.phi, cst.f, linewidth=LINEWIDTHPRES)
    plt.plot(lc_lossy.phi*180, lc_lossy["2"], linewidth=LINEWIDTHPRES)
    plt.plot(lc_lossless.phi*180, lc_lossless["2"], linewidth=LINEWIDTHPRES)
    plt.xlabel(XLAB)
    plt.ylabel(YLAB)
    plt.grid()
    plt.legend(["МКЭ", "МЭС, с потерями", "МЭС, без потерь"],
               loc='lower right', bbox_to_anchor=(1, 1.05),
               fancybox=True, shadow=True)
    code = tikzplotlib.get_tikz_code(axis_width="3.5cm", axis_height="3cm")
    with open(basename + "_pres.tikz", "w") as f:
        for l in code.split('\n'):
            f.write(l + '\n')
            if begintikz.search(l) is not None:
                f.write(r"\tikzstyle{every node}=[font=\scriptsize]" + '\n')

    for lc, name in [(lc_lossless, "lossless"), (lc_lossy, "lossy")]:
        ii = [np.where(lc["phi"]*180 >= ph)[0][0] for ph in cst.phi]
        delta = (np.abs((lc["2"][ii].reset_index(drop=True) - cst.f).dropna()))
        mean = np.mean(delta)
        mean_sq = np.sqrt(np.sum((delta - mean) ** 2) / len(delta))
        print(basename, name, mean, mean_sq)
