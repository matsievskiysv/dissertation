#!/usr/bin/python3
from os import path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

FILE = path.splitext(path.basename(__file__))[0]
YLAB = r"$E, \si{\kV\per\m}$"
XLAB = "Резонанс"
LINEWIDTH = 8
FIGSIZE = (7,5)
DPI = 300
FONTSIZE = 18
MARKERS = ["o", "v", "s", "D", "^", "<", ">", "P", "X", "p", "h", "+", "x"]

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

data = pd.read_csv(FILE + ".txt", sep="\t", names=["mkr1",
                                                   "mkr2",
                                                   "mkr3",
                                                   "mkr4",
                                                   "mkr5",
                                                   "mkr6",
                                                   "mes1",
                                                   "mes2",
                                                   "mes3",
                                                   "mes4",
                                                   "mes5",
                                                   "mes6"])

beginaxis = re.compile(r'begin\{axis\}')

for j in np.arange(1,7):
    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.plot(np.arange(len(data),dtype=np.uint)+1,data["mkr"+str(j)]/1e3,
                marker=MARKERS[0], linewidth=LINEWIDTH, label="МКЭ")
    plt.plot(np.arange(len(data),dtype=np.uint)+1,data["mes"+str(j)]/1e3,
                marker=MARKERS[1], linewidth=LINEWIDTH, label="МЭС", linestyle="--")
    plt.locator_params(axis="x", integer=True)
    plt.legend(loc='upper right', bbox_to_anchor=(0, 0),
               fancybox=True, shadow=True, ncol=1)
    plt.xlim([0.5,len(data)+0.5])
    plt.xlabel(XLAB)
    plt.ylabel(YLAB)
    plt.grid()
    #plt.show()
    # plt.tight_layout()
    # plt.savefig(FILE + f"-{j}.png")
    code = tikzplotlib.get_tikz_code(axis_width="7cm", axis_height="5cm")
    with open(FILE + f"-{j}.tikz", "w") as f:
        for l in code.split('\n'):
            f.write(l + '\n')
            if beginaxis.search(l) is not None:
                f.write("xtick=data,\n")
    plt.close()

mkr = pd.concat([data["mkr1"],
                 data["mkr2"],
                 data["mkr3"],
                 data["mkr4"],
                 data["mkr5"],
                 data["mkr6"]])
mes = pd.concat([data["mes1"],
                 data["mes2"],
                 data["mes3"],
                 data["mes4"],
                 data["mes5"],
                 data["mes6"]])

norm = np.max(np.array([np.max(mes), np.max(mkr)]))

de = np.abs(mkr/norm - mes/norm)
de_mean = de.mean()
print("mean de:", de_mean)
print("sigma de:", np.sqrt(np.sum((de-de_mean)**2)/len(de)))
