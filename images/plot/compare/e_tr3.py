#!/usr/bin/python3
from os import path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

FILE = path.splitext(path.basename(__file__))[0]
YLAB = r"$E, \si{\kV\per\m}$"
XLAB = "Резонанс"
LINEWIDTH = 8
FIGSIZE = (7,5)
DPI = 300
FONTSIZE = 18
MARKERS = ["o", "v", "s", "D", "^", "<", ">", "P", "X", "p", "h", "+", "x"]

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

data = pd.read_table(FILE + ".txt", sep="\t", names=["mkr1",
                                                     "mkr2",
                                                     "mkr3",
                                                     "mes1",
                                                     "mes2",
                                                     "mes3"])

beginaxis = re.compile(r'begin\{axis\}')

for j in np.arange(1,4):
    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.plot(np.arange(len(data), dtype=np.uint)+1, data["mkr"+str(j)]/1e3,
                marker=MARKERS[2*j], linewidth=LINEWIDTH, label="МКР")
    plt.plot(np.arange(len(data), dtype=np.uint)+1, data["mes"+str(j)]/1e3,
                marker=MARKERS[2*j+1], linewidth=LINEWIDTH, linestyle="--", label="МЭС")
    plt.locator_params(axis="x", integer=True)
    plt.legend(loc='lower right', bbox_to_anchor=(1, 1.05),
               fancybox=True, shadow=True, ncol=2)
    plt.xlim([0.5,len(data)+0.5])
    plt.xlabel(XLAB)
    plt.ylabel(YLAB)
    plt.grid()
    #plt.show()
    # plt.tight_layout()
    # plt.savefig(FILE + f"-{j}.png")
    code = tikzplotlib.get_tikz_code(axis_width="7cm", axis_height="5cm")
    with open(FILE + f"-{j}.tikz", "w") as f:
        for l in code.split('\n'):
            f.write(l + '\n')
            if beginaxis.search(l) is not None:
                f.write("xtick=data,\n")
    plt.close()

mkr = pd.concat([data["mkr1"], data["mkr2"], data["mkr3"]])
mes = pd.concat([data["mes1"], data["mes2"], data["mes3"]])

de = 2 * (mkr - mes) / (mkr + mes)
de_mean = de.mean()
print("mean de:", de_mean)
print("sigma de:", np.sqrt(np.sum((de-de_mean)**2)/len(de)))
