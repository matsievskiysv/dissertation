#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import tikzplotlib

LINEWIDTH = 3
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2
LINESTYLE=[(0, (5,1)),
           (0, (3,1,1,1)),
           (0, (1,1)),
           (0, (3,1,1,1,1,1)),
           (0, (5,1,1,1)),
           (0, (5,1,1,1,1,1))]
T = 1/2856

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

for name in ["t1", "t2", "t3", "t4"]:
    data = pd.read_csv(name + ".csv")
    plt.figure(figsize=FIGSIZE, dpi=DPI)
    for (i, cn) in enumerate(data.filter(regex="X_").columns):
        plt.plot(T*data.time, data[cn]/1e3, linewidth=LINEWIDTH)
    plt.grid()
    # plt.xscale('log')
    # plt.tight_layout()
    # plt.show()
    # plt.savefig(FILE + ".png")
    # plt.legend([r"\textup{Ячейка 1}",
    #             r"\textup{Ячейка 2}",
    #             r"\textup{Ячейка 3}",
    #             r"\textup{Ячейка 4}",
    #             r"\textup{Ячейка 5}",
    #             r"\textup{Ячейка 6}"],
    #            loc='upper center', bbox_to_anchor=(0.5, -0.3),
    #            fancybox=True, shadow=True, ncol=2)
    plt.xlabel(r"$t, \si{\micro\s}$")
    plt.ylabel(r"$E, \si{\kV\per\m}$")
    tikzplotlib.save(name + ".tikz", axis_width="7cm", axis_height="7cm")
    plt.close()
