#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import tikzplotlib
import re

LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

beginaxis = re.compile(r'begin\{axis\}')
begintikz = re.compile(r'begin\{tikzpicture\}')

data = pd.read_csv("sens.csv")

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.fill_between(data.f, data["min"], data["max"], color="red")
plt.grid()
# plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
plt.xlabel(r"$f, \si{\MHz}$")
plt.ylabel(r"$|S_{11}|, \si{\dB}$")
code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
with open("sens.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(l + '\n')
        if beginaxis.search(l) is not None:
            f.write("xtick distance=2,\n")
code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.5cm")
with open("sens_pres.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(l + '\n')
        if beginaxis.search(l) is not None:
            f.write("xtick distance=3,\n")
        if begintikz.search(l) is not None:
            f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()
