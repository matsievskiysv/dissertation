#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import tikzplotlib
import re

LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

beginaxis = re.compile(r'begin\{axis\}')

s11_cst = pd.read_table("s11_cst.txt", sep='\s+',
                        skiprows=range(0,2), names=["f", "s11"])
s11_lc = pd.read_csv("s11_lc.csv", skiprows=1, names=["f", "s11"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(s11_lc.f, s11_lc.s11, linewidth=LINEWIDTH)
plt.plot(s11_cst.f, s11_cst.s11, linewidth=LINEWIDTH, linestyle="--")
plt.legend(["МЭС", "МКЭ"])
plt.xlim((min(s11_lc.f), max(s11_lc.f)))
plt.grid()
# plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
plt.xlabel(r"$f, \si{\MHz}$")
plt.ylabel(r"$|S_{11}|, \si{\dB}$")
code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
with open("s11.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(l + '\n')
        if beginaxis.search(l) is not None:
            f.write("xtick distance=2,\n")
plt.close()


for name, off in [("r1", 1),
                  ("r2", 1),
                  ("r3", 1),
                  ("r4", 0.95),]:
    e_cst = pd.read_table(name + ".txt", sep='\s+',
                          skiprows=range(0,2), names=["l", "E"])
    e_cst = e_cst[e_cst.E != 0]
    e_cst.l = e_cst.l - min(e_cst.l)
    e_lc = pd.read_csv(name + ".csv", skiprows=1, names=["n", "E"])
    e_lc.E = e_lc.E * off
    e_lc["s"] = (max(e_cst.l) - min(e_cst.l)) * (e_lc.n-1) / e_lc.n.size
    e_lc["e"] = (max(e_cst.l) - min(e_cst.l)) * e_lc.n / e_lc.n.size


    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.bar((e_lc.s + e_lc.e) / 2, e_lc.E, width=(e_lc.e - e_lc.s) * 0.5, color="C0")
    plt.plot(e_cst.l, e_cst.E, color="C0", linewidth=LINEWIDTH)
    plt.plot(e_cst.l, e_cst.E, color="C1", linewidth=LINEWIDTH)
    # plt.show()
    plt.legend(["МЭС", "МКЭ"])
    plt.grid()
    # plt.xscale('log')
    # plt.tight_layout()
    # plt.show()
    # plt.savefig(FILE + ".png")
    plt.xlabel(r"$l, \si{\mm}$")
    plt.ylabel(r"$E, \si{\V\per\m}$")
    tikzplotlib.save(name + ".tikz", axis_width="10cm", axis_height="7cm")
    plt.close()
