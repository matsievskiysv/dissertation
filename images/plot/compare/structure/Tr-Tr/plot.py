#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import tikzplotlib
import re

LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

beginaxis = re.compile(r'begin\{axis\}')
begintikz = re.compile(r'begin\{tikzpicture\}')

for ph, ph_lambda in zip(["0", "45", "90", "135"], ["1", "1/4", "1/2", "3/4"]):
    s11_cst = pd.read_table(f"s11_cst-{ph}.txt", sep='\s+',
                            comment="#", names=["f", "s11"])
    s11_cst.f = s11_cst.f*1e3
    s11_lc = pd.read_csv(f"s11_lc-{ph}.csv", skiprows=1, names=["f", "s11"])

    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.plot(s11_lc.f, s11_lc.s11, linewidth=LINEWIDTH)
    plt.plot(s11_cst.f-0.2, s11_cst.s11, linewidth=LINEWIDTH, linestyle="--")
    plt.legend(["МЭС", "МКЭ"],
               loc='upper right', bbox_to_anchor=(0, -0.1),
               fancybox=True, shadow=True, ncol=1)
    plt.xlim((min(s11_cst.f), max(s11_cst.f)))
    plt.grid()
    # plt.xscale('log')
    # plt.tight_layout()
    # plt.show()
    # plt.savefig(FILE + ".png")
    plt.xlabel(r"$f, \si{\MHz}$")
    plt.ylabel(r"$|S_{11}|, \si{\dB}$")
    tikzplotlib.save(f"s11-{ph}.tikz", axis_width="7cm", axis_height="5cm")
    plt.close()

    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.plot(s11_lc.f, s11_lc.s11, linewidth=LINEWIDTH)
    plt.plot(s11_cst.f-0.2, s11_cst.s11, linewidth=LINEWIDTH, linestyle="--")
    plt.legend(["МЭС", "МКЭ"],
               loc='lower right', bbox_to_anchor=(1, 1.02),
               fancybox=True, shadow=True, ncol=2)
    plt.xlim((min(s11_cst.f), max(s11_cst.f)))
    plt.grid()
    plt.xlabel(r"$f, \si{\MHz}$")
    plt.ylabel(r"$|S_{11}|, \si{\dB}$")
    tikzplotlib.save(f"s11-{ph}_syn.tikz", axis_width="5.7cm", axis_height="4.3cm")
    plt.close()

    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.plot(s11_lc.f, s11_lc.s11, linewidth=LINEWIDTH)
    plt.plot(s11_cst.f-0.2, s11_cst.s11, linewidth=LINEWIDTH)
    plt.legend(["МЭС", "МКЭ"],
               loc='upper right', bbox_to_anchor=(0, -0.1),
               fancybox=True, shadow=True, ncol=1)
    plt.xlim((min(s11_cst.f), max(s11_cst.f)))
    plt.grid()
    # plt.xscale('log')
    # plt.tight_layout()
    # plt.show()
    # plt.savefig(FILE + ".png")
    plt.xlabel(r"$f, \si{\MHz}$")
    plt.ylabel(r"$|S_{11}|, \si{\dB}$")
    plt.title(f"$\\lambda = {ph_lambda}$")
    code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.5cm")
    with open(f"s11-{ph}_pres.tikz", "w") as f:
        for l in code.split('\n'):
            f.write(l + '\n')
            if beginaxis.search(l) is not None:
                f.write("xtick distance=3,\n")
            if begintikz.search(l) is not None:
                f.write(r"\tikzstyle{every node}=[font=\small]" + '\n')
    plt.close()
