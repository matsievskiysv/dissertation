#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import tikzplotlib

LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

data = pd.read_csv("sens.csv")

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.fill_between(data.f, data["min"], data["max"], color="red")
plt.grid()
# plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
plt.xlabel(r"$f, \si{\MHz}$")
plt.ylabel(r"$|S_{11}|, \si{\dB}$")
tikzplotlib.save("sens.tikz", axis_width="10cm", axis_height="7cm")
plt.close()
