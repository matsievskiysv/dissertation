#!/usr/bin/env Rscript

library(tikzDevice)

d <- read.csv("conn_tr-tr.csv")
mkr <- d[d$mkr == 1,]
mes <- d[d$mkr == 0,]

CEX <- 4
LWD <- 4
MAI <- c(3.3,3.3,0.5,0.5)

tikz("conn_n2.tikz", width=11/2.54, height=7/2.54)
par(cex=CEX, lwd=LWD, mai=MAI)
plot(mes$n,
     mes$s2,
     type="b",
     lty=1,
     lwd=LWD,
     xlab=expression(lambda),
     ylab=expression("S"["11"]*"(dB)"),
     ylim=c(min(c(mes$s2,mkr$s2)),max(c(mes$s2,mkr$s2))))
lines(mkr$n, mkr$s2, type="b", lty=2, lwd=LWD)
legend("bottomleft", legend=c("1", "2"), lty=c(1,2), lwd=c(LWD,LWD))
grid(col=1)
dev.off()
s2mkr <- 10^(mkr$s2/20)
s2mes <- 10^(mes$s2/20)
dd <- 2*abs(s2mkr-s2mes)/(s2mkr+s2mes)
print(paste("Среднеквадратичное отклонение", sqrt(sum(dd^2)/length(dd))))

tikz("conn_n4.tikz", width=11/2.54, height=7/2.54)
par(cex=CEX, lwd=LWD, mai=MAI)
plot(mes$n,
     mes$s4,
     type="b",
     lty=1,
     lwd=LWD,
     xlab=expression(lambda),
     ylab=expression("S"["11"]*"(dB)"),
     ylim=c(min(c(mes$s4,mkr$s4)),max(c(mes$s4,mkr$s4))))
lines(mkr$n, mkr$s4, type="b", lty=2, lwd=LWD)
legend("bottomleft", legend=c("1", "2"), lty=c(1,2), lwd=c(LWD,LWD))
grid(col=1)
dev.off()
s4mkr <- 10^(mkr$s4/20)
s4mes <- 10^(mes$s4/20)
dd <- 2*abs(s4mkr-s4mes)/(s4mkr+s4mes)
print(paste("Среднеквадратичное отклонение", sqrt(sum(dd^2)/length(dd))))
