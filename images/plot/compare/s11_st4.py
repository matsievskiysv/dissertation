#!/usr/bin/python3
from os import path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

FILE = path.splitext(path.basename(__file__))[0]
YLAB1 = r"$f, \si{\MHz}$"
YLAB2 = r"$S_{11}, \si{\dB}$"
XLAB = "Резонанс"
LINEWIDTH = 5
FIGSIZE = (7,5)
DPI = 300
FONTSIZE = 18
MARKERS = ["o", "v", "s", "D", "^", "<", ">", "P", "X", "p", "h", "+", "x"]

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

data = pd.read_table(FILE + ".txt", sep="\t",
                     names=["beta", "f_mkr", "s11_mkr", "f_mes", "s11_mes"])

beginaxis = re.compile(r'begin\{axis\}')

plt.figure(figsize=FIGSIZE, dpi=DPI)
for (i,), beta in np.ndenumerate(np.unique(data.beta)):
    ldata = data[data.beta == beta]
    plt.plot(np.arange(len(ldata),dtype=np.uint)+1,ldata.f_mkr,
             color=f"C{i}", marker=MARKERS[2*i], linewidth=LINEWIDTH)
    plt.plot(np.arange(len(ldata),dtype=np.uint)+1,ldata.f_mes,
             color=f"C{i}", marker=MARKERS[2*i+1], linewidth=LINEWIDTH, linestyle="--")
    plt.locator_params(axis="x", integer=True)
plt.legend([r"$\beta" + f"_{x}={b}".replace(".", ",") + "$"
             for b in np.unique(data.beta) for x in [r"{\textup{МКЭ}}", r"{\textup{МЭС}}"]],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
plt.xlabel(XLAB)
plt.ylabel(YLAB1)
plt.grid()
#plt.show()
# plt.tight_layout()
# plt.savefig(FILE + "-f.png")
code = tikzplotlib.get_tikz_code(axis_width="7cm", axis_height="5cm")
with open(FILE + "-f.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(l + '\n')
        if beginaxis.search(l) is not None:
            f.write("xtick=data,\n")
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
for (i,), beta in np.ndenumerate(np.unique(data.beta)):
    ldata = data[data.beta == beta]
    plt.plot(np.arange(len(ldata),dtype=np.uint)+1,ldata.s11_mkr,
             color=f"C{i}", marker=MARKERS[2*i], linewidth=LINEWIDTH)
    plt.plot(np.arange(len(ldata),dtype=np.uint)+1,ldata.s11_mes,
             color=f"C{i}", marker=MARKERS[2*i+1], linewidth=LINEWIDTH, linestyle="--")
plt.locator_params(axis="x", integer=True)
plt.legend([r"$\beta" + f"_{x}={b}".replace(".", ",") + "$"
             for b in np.unique(data.beta) for x in [r"{\textup{МКЭ}}", r"{\textup{МЭС}}"]],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
plt.xlabel(XLAB)
plt.ylabel(YLAB2)
plt.grid()
#plt.show()
# plt.tight_layout()
# plt.savefig(FILE + "-s11.png")
code = tikzplotlib.get_tikz_code(axis_width="7cm", axis_height="5cm")
with open(FILE + "-s11.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(l + '\n')
        if beginaxis.search(l) is not None:
            f.write("xtick=data,\n")
plt.close()

df = np.abs(data["f_mkr"] - data["f_mes"])
df_mean = df.mean()
print("mean df:", df_mean)
print("sigma df:", np.sqrt(np.sum((df-df_mean)**2)/len(df)))

ldata = pd.DataFrame({"S11_mkr-lin":10**(data["s11_mkr"]/20),
                      "S11_mes-lin":10**(data["s11_mes"]/20)})

ds = np.abs(ldata["S11_mkr-lin"] - ldata["S11_mes-lin"])
ds_mean = ds.mean()
print("mean ds:", ds_mean)
print("sigma ds:", np.sqrt(np.sum((ds-ds_mean)**2)/len(ds)))
