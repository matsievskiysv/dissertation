#!/usr/bin/python3
from os import path
import pandas as p
import matplotlib.pyplot as plt
import tikzplotlib

FILE = path.splitext(path.basename(__file__))[0]
YLAB = r"$I_{\textup{п}}, \si{\A}$"
XLAB = r"$V_{\textup{упр}}, \si{\kV}$"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

data = p.read_csv(FILE + ".csv", sep=",",
                  skiprows=1, names=["v", "i"],
                  comment="#")

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data["v"], data["i"], linewidth=LINEWIDTH)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
