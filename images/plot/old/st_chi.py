#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt

# Параметры группирователя

# Ячейка 1
f0_1= 2856.00021 * 1e6
E0_1= 0.510999 * 1e6
beta_1= 0.4763295
Ek_1= 0.070 * 1e6
T_1= 0.8680398
Pdiss_1= 366.9294
Q_1= 7605.94
Rsh_1= 34.067 * 1e6
r2Q_1= 42.186
zTT_1= 25.669 * 1e6

l1 = 30e-3 # длина
rsh1 = Rsh_1 * T_1 # шунтовое эффективное погонное сопротивление
E1 = 1.1e6 # напряжённость поля

Rsh1 = rsh1 * l1 # шунтовое эффективное сопротивление
V1 = E1 * l1 # напряжение
Pl1 = V1**2/Rsh1 # потери

# Ячейка 2
f0_2= 2856.00022 * 1e6
E0_2= 0.510999 * 1e6
beta_2= 0.4001168
Ek_2= 0.047 * 1e6
T_2= 0.8523185
Pdiss_2= 506.4884
Q_2= 5928.34
Rsh_2= 20.731 * 1e6
r2Q_2= 26.674
zTT_2= 15.060 * 1e6

l2 = 21e-3
rsh2 = Rsh_2 * T_2
E2 = 7e6

Rsh2 = rsh2 * l2
V2 = E2 * l2
Pl2 = V2**2/Rsh2

# Ячейка 3
f0_3= 2856.00000 * 1e6
E0_3= 0.510999 * 1e6
beta_3= 0.9526591
Ek_3= 1.170 * 1e6
T_3= 0.8096084
Pdiss_3= 324.4492
Q_3= 16915.6
Rsh_3= 117.556 * 1e6
r2Q_3= 113.880
zTT_3= 77.054 * 1e6

l3 = 50e-3
rsh3 = Rsh_3 * T_3
E3 = 16e6

Rsh3 = rsh3 * l3
V3 = E3 * l3
Pl3 = V3**2/Rsh3

# Ячейка 4 (регулярная)

f0_4= 2856.00068 * 1e6
E0_4= 0.510999 * 1e6
beta_4= 0.9983867
Ek_4= 8.489 * 1e6
T_4= 0.8034199
Pdiss_4= 211.7888
Q_4= 17586.4
Rsh_4= 123.708 * 1e6
r2Q_4= 118.962
zTT_4= 79.852 * 1e6

# Выходные данные расчёта
# максимальная энергия электронов
# средняя энергия электронов
# мощность пучка
# выходной ток пучка

Wmax= 6.49126 * 1e6
Wav= 5.54052 * 1e6
Pb= 1.07594 * 1e6
Pbtot= 1.07594 * 1e6
Pload= 1.06098 * 1e6
Iout= 0.18978
phase= 20.

n = 8 # количество ячеек
l4 = 52.4e-3 * n
rsh4 = Rsh_4 * T_4
E4 = 16.9e6

Rsh4 = rsh4 * l4
V4 = E4 * l4
Pl4 = V4**2/Rsh4

Iin = 0.559 # входной ток
V = V1 + V2 + V3 + V4 # суммарное напряжение
Rsh = Rsh1 + Rsh2 + Rsh3 + Rsh4 # суммарное шунтовое эффективное сопротивление
print(f"Rsh=\t{round(Rsh/1e6,3)} MOhm")
Ploss = Pl1 + Pl2 + Pl3 + Pl4 # суммарные потери
print(f"Ploss=\t{round(Ploss/1e6,3)} MW")

Pin = Ploss + Pbtot
print(f"Pin=\t{round(Pin/1e6,3)} MW")

kz = Iout/Iin
print(f"kz=\t{round(kz*100,3)} %")

beta = (Iout/2*np.sqrt(Rsh/Pin)+np.sqrt(1+Iout**2/4*Rsh/Pin))**2
print(f"beta_opt=\t{round(beta,3)}")

beta_c = np.sqrt(1-1/(Wmax/0.511e6+1)**2)
print(f"beta_c=\t{round(beta_c,3)}")

# beta_range = np.arange(1,5,0.1)
# kpd = 2*np.sqrt(beta_range)/(1+beta_range)*np.sqrt(Rsh/Pin)*Iout-Rsh/(Pin*(1+beta_range))*Iout**2
# plt.plot(beta_range, kpd)
# plt.show()

print(",".join([str(round(x,2)) for x in [Rsh, Ploss, Pin, kz, beta, beta_c]]))
