#!/usr/bin/python3
from os import path
import numpy as np
import pandas as p
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

FILE = path.splitext(path.basename(__file__))[0]
YLAB = "КПД, %"
XLAB = "L, м"
LINEWIDTH = 2
FIGSIZE = (9,9)
DPI = 300

l = np.arange(0.01,2.5,0.01)
rsh=np.array([83,71,63])*1e6
a=np.array([0.06,0.08,0.1])
alpha=np.array([0.18,0.19,0.2])
tau=np.outer(alpha,l)
I0=0.4
P0=5.5e6
def n(i):
    return (np.sqrt(rsh[i]*l/P0)*np.sqrt(1-np.exp(-2*tau[i]))*I0-
            rsh[i]*l/P0*(0.5-tau[i]*np.exp(-2*tau[i])/(1-np.exp(-2*tau[i])))*I0**2)*100

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(a):
    plt.plot(l[n(i)>0],n(i)[n(i)>0], linewidth=LINEWIDTH)
plt.legend([f"$a/\\alpha$={round(x, 2)}" for x in a])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
#plt.show()
plt.savefig(FILE + ".png")
