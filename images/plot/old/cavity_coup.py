#!/usr/bin/python3
from os import path
import numpy as np
import pandas as p
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

FILE = path.splitext(path.basename(__file__))[0]
YLAB = "V, МэВ"
XLAB = "L, м"
LINEWIDTH = 5
FIGSIZE = (7,5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

# Аналитическая связь резонатора с подводящей линией

Iout = np.arange(0.01,0.5,0.01)
Rsh_gr = 12.853e6
Rsh_reg = 75e6 * 0.055
Pin_gr = 0.455e6

(Iout/2*np.sqrt(Rsh/Pin)+np.sqrt(1+Iout**2/4*Rsh/Pin))**2

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(Iout,beta, linewidth=LINEWIDTH)
plt.legend([f"$a/\\alpha$={round(x, 2)}" for x in a])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
plt.show()
plt.tight_layout()
plt.savefig(FILE + ".png")
