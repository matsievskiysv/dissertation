#!/usr/bin/python3
from os import path
import numpy as np
import pandas as p
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

FILE = path.splitext(path.basename(__file__))[0]
YLAB = r"$\beta$"
XLAB = "L, мм"
LINEWIDTH = 5
FIGSIZE = (7,5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

data = p.read_csv(FILE + ".csv")

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.plot(data["L"], data["beta"], linewidth=LINEWIDTH)
plt.yscale("log")
plt.grid()
plt.tight_layout()
# plt.show()
plt.savefig(FILE + ".png")
