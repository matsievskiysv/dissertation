#!/usr/bin/python3
from os import path
import numpy as np
import matplotlib.pyplot as plt

FILE = path.splitext(path.basename(__file__))[0]
XLAB = "L, мм"
YLAB = "E"
COLOR = "black"
LINEWIDTH = 5
FIGSIZE = (7,5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

data = np.genfromtxt(FILE + ".txt")
plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data[:,0] - data[0,0],
         np.flip(data[:,1])/np.max(data[:,1]),
         color=COLOR, linewidth=LINEWIDTH)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
plt.tight_layout()
plt.savefig(FILE + ".png")
