#!/usr/bin/python3
from os import path
import numpy as np
import pandas as p
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

FILE = path.splitext(path.basename(__file__))[0]
YLAB = "КПД, %"
XLAB = "L, м"
LINEWIDTH = 5
FIGSIZE = (6,6)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)


l = np.arange(0.01,1.8,0.01)
rsh=70e6
alpha=0.2
tau=alpha*l
I0=np.arange(0.1,0.7,0.1)
P0=5.5e6
def n(I0):
    return (np.sqrt(rsh*l/P0)*np.sqrt(1-np.exp(-2*tau))*I0-
            rsh*l/P0*(0.5-tau*np.exp(-2*tau)/(1-np.exp(-2*tau)))*I0**2)*100

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i in I0:
    plt.plot(l[n(i)>0],n(i)[n(i)>0], linewidth=LINEWIDTH)
plt.legend([f"I={round(x, 1)}" for x in I0])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
#plt.show()
plt.tight_layout()
plt.savefig(FILE + ".png")
