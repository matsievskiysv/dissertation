#!/usr/bin/python3
from os import path
import numpy as np
import pandas as p
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

FILE = path.splitext(path.basename(__file__))[0]
XLAB = r"$\frac{a}{\alpha}$"
LINEWIDTH = 2
FIGSIZE = (9,5)
DPI = 300

data_k1 = p.read_csv(FILE + "_k1.csv", sep=",")
data_k15 = p.read_csv(FILE + "_k1.5.csv", sep=",")

YLAB = r"$r_{sh}$"
plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data_k1["a_lambda"],data_k1["rsh"], linewidth=LINEWIDTH)
plt.plot(data_k15["a_lambda"],data_k15["rsh"], linewidth=LINEWIDTH)
plt.legend(["$K_{св}=1%$", "$K_{св}=1,5%$"])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.show()
plt.savefig(FILE + "_rsh.png")

YLAB = r"$\alpha$"
plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data_k1["a_lambda"],data_k1["alpha"], linewidth=LINEWIDTH)
plt.plot(data_k15["a_lambda"],data_k15["alpha"], linewidth=LINEWIDTH)
plt.legend(["$K_{св}=1%$", "$K_{св}=1,5%$"])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.show()
plt.savefig(FILE + "_alpha.png")

YLAB = r"$\frac{E \lambda}{\sqrt{P}}$"
plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data_k1["a_lambda"],data_k1["E-lambda-p"], linewidth=LINEWIDTH)
plt.plot(data_k15["a_lambda"],data_k15["E-lambda-p"], linewidth=LINEWIDTH)
plt.legend(["$K_{св}=1%$", "$K_{св}=1,5%$"])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.show()
plt.savefig(FILE + "_E-lambda-p.png")
