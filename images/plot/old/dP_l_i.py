#!/usr/bin/python3
from os import path
import numpy as np
import pandas as p
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

FILE = path.splitext(path.basename(__file__))[0]
YLAB = "$P_{out}$, МВт"
XLAB = "N"
LINEWIDTH = 5
FIGSIZE = (7,5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)


n = np.arange(0,31,1)
l = n * 0.035
l[0] = 1e-15
rsh=70e6
alpha=0.19
tau=alpha*l
I0=[0.2]
P0=5.5e6

def dP(I0):
    return (np.sqrt(P0)*np.exp(-alpha*l)+I0*np.sqrt(rsh/2/alpha)*\
            (np.exp(-alpha*l)-1))**2

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i in I0:
    plt.scatter(n,dP(i)/1e6)
plt.legend([f"I = {round(x, 1)} A" for x in I0])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
# plt.ylim([0,12])
plt.grid()
# plt.show()
plt.tight_layout()
plt.savefig(FILE + ".png")
