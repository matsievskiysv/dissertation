#!/usr/bin/python3
from os import path
import numpy as np
import pandas as p
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

FILE = path.splitext(path.basename(__file__))[0]
YLAB = "P, МВт"
XLAB = "Количество ячеек"
LINEWIDTH = 2
FIGSIZE = (9,5)
DPI = 300

data = p.read_csv(FILE + ".txt", sep=",", names=["n",
                                                  "rsh",
                                                  "l",
                                                  "W",
                                                  "Pb",
                                                  "Pl",
                                                  "Pin"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data["n"],data["Pl"], linewidth=LINEWIDTH)
plt.plot(data["n"],data["Pb"], linewidth=LINEWIDTH)
plt.plot(data["n"],data["Pin"], linewidth=LINEWIDTH)
plt.legend(["$P_{Loss}$", "$P_{b}$", "$P_{out}$"])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
#plt.show()
plt.savefig(FILE + ".png")
