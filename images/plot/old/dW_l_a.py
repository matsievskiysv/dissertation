#!/usr/bin/python3
from os import path
import numpy as np
import pandas as p
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

FILE = path.splitext(path.basename(__file__))[0]
YLAB = "V, МэВ"
XLAB = "L, м"
LINEWIDTH = 5
FIGSIZE = (7,5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

l = np.arange(0.01,2.5,0.01)
rsh=np.array([83,71,63])*1e6
a=np.array([0.06,0.08,0.1])
alpha=np.array([0.18,0.19,0.2])
tau=np.outer(alpha,l)
I0=0.4
P0=5.5e6
def W(i):
    return (np.sqrt(2*rsh[i]*P0/alpha[i])*(1-np.exp(-tau[i]))-
            I0*rsh[i]*l*(1-(1-np.exp(-tau[i]))/(tau[i])))/1e6

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(a):
    plt.plot(l[W(i)>0],W(i)[W(i)>0], linewidth=LINEWIDTH)
plt.legend([f"$a/\\alpha$={round(x, 2)}" for x in a])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.show()
plt.tight_layout()
plt.savefig(FILE + ".png")
