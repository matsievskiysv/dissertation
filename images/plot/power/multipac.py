#!/usr/bin/python3
from os import path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

FILE = path.splitext(path.basename(__file__))[0]
YLAB = "P, МВт"
XLAB = "Количество ячеек"
LINEWIDTH = 2
FIGSIZE = (9,5)
DPI = 300

def E(d):
    """Напряжённость в зазоре для порядка n = [1;6].
    d (мм) - ширина зазора"""
    d = d / 1000
    return [4*np.pi*f**2*d/(2*n-1)/QtoM for n in range(1,7)]

def log_interp(zz, xx, yy):
    logz = np.log10(zz)
    logx = np.log10(xx)
    logy = np.log10(yy)
    return np.power(10.0, np.interp(logz, logx, logy))

QtoM = 1.758820024e11 # заряд к массе электрона
f = 325e6

p1 = pd.DataFrame(data={"P":[1,10,100,1000], "E": [326,1031,3262,11021]})
p2 = pd.DataFrame(data={"P":[1,10,100,1000], "E": [713,2257,7136,22567]})


plt.plot(p1.P,p1.E,color="r")
plt.plot(p2.P,p2.E,color="b")
plt.plot(np.interp(E(1),p1.E,p1.P),E(1), marker='o', color='r', ls='')
plt.plot(np.interp(E(0.5),p2.E,p2.P),E(0.5), marker='o', color='b', ls='')
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.ylabel('E, В/м')
plt.xlabel('P, Вт')
plt.legend(["Зазор 1 мм", "Зазор 0,5 мм"])
# plt.show()
plt.savefig(FILE + ".png")
