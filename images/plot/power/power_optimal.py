#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import tikzplotlib
import re


LINEWIDTH = 5
LINEWIDTHPRES = 2
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
LINESTYLE=[(0, (5,1)),
           (0, (3,1,1,1)),
           (0, (1,1)),
           (0, (3,1,1,1,1,1)),
           (0, (5,1,1,1)),
           (0, (5,1,1,1,1,1))]

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

beginaxis = re.compile(r'begin\{axis\}')
begintikz = re.compile(r'begin\{tikzpicture\}')

Q0 = 1.3e9  # добротность
Q0 = Q0 / 2  # реальная будет примерно в 2 раза ниже
Qls = [np.logspace(5, 8),
       np.logspace(5, 7),
       np.logspace(5.2, 5.8)]  # нагруженная добротность
ylim = [5e3, 7e3, 1.3e4]
xlog = [True, True, False]
beta_b = 0.21  # относительная фазовая скорость
df = np.array([25, 50, 100, 200])  # смещение частоты, вызванное силой Лоренса, давлением гелия и т.д., Гц
f0 = 325e6  # резонансная частота, МГц
Eacc = 6.4e6  # ускоряющий градиент, В/м
l = 0.188  # длина ячейки, м
# Rsh/Q0 = 302 Ом
Rsh = 302 * Q0  # шунтовое эффективное сопротивление, Ом/м
Ib = np.array([1e-4, 1e-3, 1e-2])  # ток пучка, А
# betalambda = beta_b * 299792458 / f0
phase_s = np.pi/9  # синхронная фаза
Vacc_des = Eacc*0.188  # ускоряющее напряжение в ячейке, В

imin = {}  # положения минимумов
for n, i in enumerate(Ib):
    Ql = Qls[n]
    beta = Q0/Ql-1  # коэффициент связи
    Qex = Q0 / beta
    imin[i] = {}
    plt.figure(num=None, figsize=FIGSIZE, dpi=DPI, facecolor='w', edgecolor='k')
    for j, delta in enumerate(df):
        Pg = Vacc_des**2/(4*Rsh*Qex/Q0)*((1+i*Rsh*Qex*np.cos(phase_s)/Q0/Vacc_des)**2+(2*Qex*(delta)/f0)**2)
        ql = Ql[Pg < ylim[n]]
        Pg = Pg[Pg < ylim[n]]
        plt.plot(ql, Pg/1e3, linewidth=LINEWIDTH, linestyle=LINESTYLE[j])
        imin[i][delta] = {"Pg": Pg[np.argmin(Pg)],
                          "Ql": ql[np.argmin(Pg)]}
        print(f"Ток {i} А, смещение частоты {delta} Гц. Минимальное значение мощности на графике {imin[i][delta]['Pg']:.2e} при внешней добротности {imin[i][delta]['Ql']:.2e}")
    if xlog[n]:
        plt.xscale('log')
    # plt.yscale('log')
    plt.grid()
    plt.legend([f"beforeplaceholder{f}afterplaceholder" for f in df],
               loc='lower right', bbox_to_anchor=(1, 1.05),
               fancybox=True, shadow=True, ncol=2)
    # plt.title(str(f"$I_п$={i} А").replace(".", ","))
    plt.xlabel(r"$Q_{\textup{н}}$")
    plt.ylabel(r"$P_{\textup{г}}, \si{\kW}$")
    for j, delta in enumerate(df):
        plt.axvline(imin[i][delta]["Ql"], color=f"C{j}")
    # plt.tight_layout()
    # plt.savefig(f"power_Ql_I{i}".replace(".","-") + ".png")
    #plt.show()
    code = tikzplotlib.get_tikz_code(axis_width="7.8cm", axis_height="5.5cm")
    with open(f"power_Ql_I{i}".replace(".", "-") + ".tikz", "w") as f:
        for line in code.split('\n'):
            f.write(line.replace("beforeplaceholder", r"$\Delta f = \SI{")
                    .replace("afterplaceholder", r"}{\Hz}$") + '\n')
            # if begintikz.search(line) is not None:
            #     f.write(r"\tikzstyle{every node}=[font=\scriptsize]" + '\n')
    plt.close()

    plt.figure(num=None, figsize=FIGSIZE, dpi=DPI, facecolor='w', edgecolor='k')
    for j, delta in enumerate(df):
        Pg = Vacc_des**2/(4*Rsh*Qex/Q0)*((1+i*Rsh*Qex*np.cos(phase_s)/Q0/Vacc_des)**2+(2*Qex*(delta)/f0)**2)
        ql = Ql[Pg < ylim[n]]
        Pg = Pg[Pg < ylim[n]]
        plt.plot(ql, Pg/1e3, linewidth=LINEWIDTH, linestyle=LINESTYLE[j])
        imin[i][delta] = {"Pg": Pg[np.argmin(Pg)],
                          "Ql": ql[np.argmin(Pg)]}
        print(f"Ток {i} А, смещение частоты {delta} Гц. Минимальное значение мощности на графике {imin[i][delta]['Pg']:.2e} при внешней добротности {imin[i][delta]['Ql']:.2e}")
    if xlog[n]:
        plt.xscale('log')
    # plt.yscale('log')
    plt.grid()
    plt.legend([f"beforeplaceholder{f}afterplaceholder" for f in df],
               loc='upper left', bbox_to_anchor=(1.02, 1),
               fancybox=True, shadow=True, ncol=1)
    # plt.title(str(f"$I_п$={i} А").replace(".", ","))
    plt.xlabel(r"$Q_{\textup{н}}$")
    plt.ylabel(r"$P_{\textup{г}}, \si{\kW}$")
    for j, delta in enumerate(df):
        plt.axvline(imin[i][delta]["Ql"], color=f"C{j}")
    # plt.tight_layout()
    # plt.savefig(f"power_Ql_I{i}".replace(".","-") + ".png")
    #plt.show()
    code = tikzplotlib.get_tikz_code(axis_width="6.5cm", axis_height="4.5cm")
    with open(f"power_Ql_I{i}".replace(".", "-") + "_syn.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(line.replace("beforeplaceholder", r"$\Delta f = \SI{")
                    .replace("afterplaceholder", r"}{\Hz}$") + '\n')
    plt.close()

    plt.figure(num=None, figsize=FIGSIZE, dpi=DPI, facecolor='w', edgecolor='k')
    for j, delta in enumerate(df):
        Pg = Vacc_des**2/(4*Rsh*Qex/Q0)*((1+i*Rsh*Qex*np.cos(phase_s)/Q0/Vacc_des)**2+(2*Qex*(delta)/f0)**2)
        ql = Ql[Pg < ylim[n]]
        Pg = Pg[Pg < ylim[n]]
        plt.plot(ql, Pg/1e3, linewidth=LINEWIDTHPRES)
        imin[i][delta] = {"Pg": Pg[np.argmin(Pg)],
                          "Ql": ql[np.argmin(Pg)]}
        print(f"Ток {i} А, смещение частоты {delta} Гц. Минимальное значение мощности на графике {imin[i][delta]['Pg']:.2e} при внешней добротности {imin[i][delta]['Ql']:.2e}")
    if xlog[n]:
        plt.xscale('log')
    # plt.yscale('log')
    plt.grid()
    plt.legend([f"beforeplaceholder{f}afterplaceholder" for f in df],
               loc='lower right', bbox_to_anchor=(1, 1.05),
               fancybox=True, shadow=True, ncol=1)
    # plt.title(str(f"$I_п$={i} А").replace(".", ","))
    plt.xlabel(r"$Q_{\textup{н}}$")
    plt.ylabel(r"$P_{\textup{г}}$, \si{\kW}")
    for j, delta in enumerate(df):
        plt.axvline(imin[i][delta]["Ql"], color=f"C{j}", linestyle="--")
    code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
    with open(f"power_Ql_I{i}".replace(".", "-") + "_pres.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(line.replace("beforeplaceholder", r"$\Delta f = \SI{")
                    .replace("afterplaceholder", r"}{\Hz}$") + '\n')
            if begintikz.search(line) is not None:
                f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')

# Ql = Qls[3]
# beta = Q0/Ql-1  # коэффициент связи
# Qex = Q0 / beta
# qpos = imin[0.01][30]["Ql"]  # оптимальная связь для тока 0,01А с расстройкой 30Гц
# qposi = np.where(Qex>=qpos)[0][0]
# for delta in df:
#     plt.figure(num=None, figsize=FIGSIZE, dpi=DPI, facecolor='w', edgecolor='k')
#     for i in Ib:
#         Pg = (Vacc_des**2/(4*Rsh*Qex/Q0)*((1+i*Rsh*Qex*np.cos(phase_s)/Q0/Vacc_des)**2+(2*Qex*(delta)/f0)**2))
#         plt.plot(Ql,Pg,linewidth=LINEWIDTH)
#         print(f"Ток {i} А, смещение частоты {delta} Гц. Требуемая подводимая мощность {Pg[qposi]:.2e}")
#     plt.axvline(qpos)
#     plt.xscale('log')
#     plt.grid()
#     plt.title(f"$\Delta$f = {delta}")
#     plt.legend([f"$I_п$={i} А" for i in Ib])
#     plt.xlabel("$Q_{вн}$")
#     plt.ylabel("$P_г$, Вт")
    # plt.tight_layout()
    # plt.savefig(f"power_Ql_f{delta}".replace(".","-") + ".png")
#     #plt.show()

# I = np.logspace(-8,-2)
# beta_opt = Rsh * I / Vacc_des + 1
# Pg = Vacc_des ** 2 * (I * Rsh / Vacc_des + 1) / Rsh
# # plt.xscale('log')
# plt.figure(num=None, figsize=FIGSIZE, dpi=DPI, facecolor='w', edgecolor='k')
# ax = plt.gca()
# ax2 = ax.twinx()
# ax.set_yscale('log')
# ax.set_xscale('log')
# ax2.set_yscale('log')
# ax2.set_xscale('log')
# ax.plot(I, beta_opt, linewidth=LINEWIDTH)
# ax2.plot(I, Pg, linewidth=LINEWIDTH)
# ax.set_xlabel(r"$I_п$, А")
# ax.set_ylabel(r"$\beta_{опт}$")
# ax2.set_ylabel(r"$P_{опт}$, Вт")
# ax.grid()
# plt.tight_layout()
# plt.savefig(f"power_beta".replace(".","-") + ".png")
# # plt.show()
