#!/usr/bin/python3
from os import path
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

FILE = path.splitext(path.basename(__file__))[0]
YLAB = r"$E_{\textup{макс}}, \si{\kV\per\cm}$"
XLAB_C = r"$Z_C, \si{\Ohm}$"
XLAB_L = r"$Z_L, \si{\Ohm}$"
LINEWIDTH = 5
LINEWIDTHPRES = 2
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

beginaxis = re.compile(r'begin\{axis\}')
begintikz = re.compile(r'begin\{tikzpicture\}')

# Common
data = pd.read_csv(FILE + ".csv", comment="%", names=["z", "e"])

data = data[data.z < 1e8]
data.e  = data.e/1e3*1e-2

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.z[data.z > 0], data.e[data.z > 0], linewidth=LINEWIDTH)
plt.xlabel(XLAB_L)
plt.ylabel(YLAB)
plt.grid()
plt.xscale('log')
# plt.show()
tikzplotlib.save(FILE + "_l.tikz", axis_width="7cm", axis_height="5cm")
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.z[data.z > 0], data.e[data.z > 0], linewidth=LINEWIDTHPRES)
plt.xlabel(XLAB_L)
plt.ylabel(YLAB)
plt.grid()
plt.xscale('log')
code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
with open(FILE + "_l_pres.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(l + '\n')
        # if beginaxis.search(l) is not None:
        #     f.write("xtick distance=3,\n")
        if begintikz.search(l) is not None:
            f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(-data.z[data.z < 0], data.e[data.z < 0], linewidth=LINEWIDTH)
plt.xlabel(XLAB_C)
plt.ylabel(YLAB)
plt.grid()
plt.xscale('log')
# plt.show()
tikzplotlib.save(FILE + "_c.tikz", axis_width="7cm", axis_height="5cm")
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(-data.z[data.z < 0], data.e[data.z < 0], linewidth=LINEWIDTHPRES)
plt.xlabel(XLAB_C)
plt.ylabel(YLAB)
plt.grid()
plt.xscale('log')
code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
with open(FILE + "_c_pres.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(l + '\n')
        # if beginaxis.search(l) is not None:
        #     f.write("xtick distance=3,\n")
        if begintikz.search(l) is not None:
            f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()
