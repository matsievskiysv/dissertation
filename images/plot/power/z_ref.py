#!/usr/bin/env python3

from matplotlib import pyplot as plt
from smithplot.smithaxes import SmithAxes
import numpy as np

z = np.concatenate((np.logspace(0, 0.5, 3, dtype="complex") * 1j,
                    np.logspace(0.5, 1, 3, dtype="complex") * 1j,
                    np.logspace(1, 3, 20, dtype="complex") * 1j,
                    np.logspace(3, 4, 3, dtype="complex") * 1j))
z = np.concatenate(([0+0j], z, [0+1e10j], -z[::-1]))

plt.figure(figsize=(6, 6))
ax = plt.subplot(1, 1, 1, projection='smith')
ax.plot(z, datatype=SmithAxes.Z_PARAMETER)
# plt.show()

with open("z.txt", "w") as f:
    f.write('zp ')
    f.write(",".join([str(np.imag(x)) for x in z]))
    f.write(' []')
