#!/usr/bin/python3
from os import path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tikzplotlib
import re
from scipy.interpolate import interp1d

FILE = "beta_over_l"
YLAB = r"$Q_{\textup{наг}}$"
XLAB = r"$L$, \si{\mm}"
LINEWIDTH = 5
LINEWIDTHPRES = 2
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

beginaxis = re.compile(r'begin\{axis\}')
begintikz = re.compile(r'begin\{tikzpicture\}')

# Common

Q0 = pd.read_table("Q0.txt", skiprows=[0, 1], names=["l", "Q0"], sep="\s+")
Qex = pd.read_table("Qex.txt", skiprows=[0, 1], names=["l", "Qex"], sep="\s+")
Q = Q0.merge(Qex, on="l")
Q["Ql"] = 1/(1/Q.Q0 + 1/Q.Qex)
Q.Ql

fQ = interp1d(Q.Ql, Q.l)

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(Q.l, Q.Ql, linewidth=LINEWIDTH)
# plt.plot(Q.Ql, Q.l)
plt.axvline(fQ(4e5), color="red", linewidth=LINEWIDTH, linestyle="--")
plt.axvline(fQ(5.9e5), color="red", linewidth=LINEWIDTH, linestyle="--")
plt.axvline(fQ(4.6e5), color="green", linewidth=LINEWIDTH)
plt.axvline(fQ(2e6), color="green", linewidth=LINEWIDTH)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
plt.yscale('log')
plt.xlim(-25, 0)
plt.ylim(5e4, 1e7)
# plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(Q.l, Q.Ql, linewidth=LINEWIDTHPRES)
# plt.plot(Q.Ql, Q.l)
plt.axvline(fQ(4e5), color="red", linewidth=LINEWIDTHPRES)
plt.axvline(fQ(5.9e5), color="red", linewidth=LINEWIDTHPRES)
plt.axvline(fQ(4.6e5), color="green", linewidth=LINEWIDTHPRES)
plt.axvline(fQ(2e6), color="green", linewidth=LINEWIDTHPRES)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
plt.yscale('log')
plt.xlim(-25, 0)
plt.ylim(5e4, 1e7)
# plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
with open(FILE + "_pres.tikz", "w") as f:
    for line in code.split('\n'):
        f.write(line + '\n')
        if begintikz.search(line) is not None:
            f.write(r"\tikzstyle{every node}=[font=\scriptsize]" + '\n')
plt.close()
