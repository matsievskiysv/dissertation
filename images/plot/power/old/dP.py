#!/usr/bin/python3
from os import path
import numpy as np
import matplotlib.pyplot as plt

FILE = "dP"
try:
    FILE = path.splitext(path.basename(__file__))[0]
except:
    pass
YLAB = r"${P_н}/{P_к}$"
XLAB = r"$I_п$"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)


# Common

Pn = np.array([3.1e3, 3.7e3, 1.27e4])
Pc = np.array([8.76e2, 1.57e3, 1.21e4])
dP = Pn/Pc
I = np.array([0.0001, 0.001, 0.01])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(I, dP)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.yscale('log')
plt.xscale('log')
plt.tight_layout()
# plt.show()
plt.savefig(FILE + ".png")
