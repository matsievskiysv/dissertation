#!/usr/bin/python3
from os import path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib

FILE = path.splitext(path.basename(__file__))[0]
YLAB = r"$|S_{11}|, \si{\dB}$"
XLAB = r"$\varepsilon$"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

# Common
data = pd.read_table(FILE + ".txt", comment="#", usecols=[0, 1, 2], names=["eps", "s11_re", "s11_im"])
data["s11_db"] = 20 * np.log10(np.abs(data.s11_re + data.s11_im*1j))

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.eps, data.s11_db, linewidth=LINEWIDTH)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
