#!/usr/bin/python3
from os import path
import numpy as np
import matplotlib.pyplot as plt
import tikzplotlib

FILE = path.splitext(path.basename(__file__))[0]
YLAB = r"$\Delta f, \si{\Hz}$"
XLAB = r"$Q_{\textup{наг}}$"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)


# Common

Ql = np.logspace(5.5, 8.5, 101)
f0 = 325e6

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(Ql, f0/2/Ql, linewidth=LINEWIDTH)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
plt.yscale('log')
plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
