#!/usr/bin/python3
from os import path
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib

# {{{ plot setup
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)
# }}}

### Copper

# {{{ Copper conductivity
FILE = "Copper_RRR_100_elect_conductivity_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "c"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.c, linewidth=LINEWIDTH)
plt.grid()
plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"S, \si{\siemens}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}

# {{{ Copper thermal conductivity
FILE = "Copper_RRR_100_temp_conductivity_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "c"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.c, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"k, \si{\W\per\m\per\K}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}

# {{{ Copper specific heat
FILE = "Copper_specific_heat_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "c"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.c, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"C, \si{\J\per\kg\per\K}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}

# {{{ Copper thermal expansion
FILE = "Copper_thermal_expansion_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "c"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.c*1e6, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"\beta, \si{\micro\m\per\m}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}

# {{{ Copper elasticity
FILE = "Copper_elasticity_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "y", "p"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.y/1e9, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"E, \si{\giga\pascal}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + "_young.tikz", axis_width="10cm", axis_height="7cm")
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.p, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"$\nu$")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + "_poisson.tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}

### Steel

# {{{ Steel conductivity
FILE = "Steel_elect_conductivity_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "c"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.c, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"S, \si{\siemens}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}

# {{{ Steel thermal conductivity
FILE = "Steel_temp_conductivity_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "c"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.c, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"k, \si{\W\per\m\per\K}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}

# {{{ Steel specific heat
FILE = "Steel_specific_heat_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "c"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.c, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"C, \si{\J\per\kg\per\K}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}

# {{{ Steel thermal expansion
FILE = "Steel_thermal_expansion_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "c"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.c*1e6, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"\beta, \si{\micro\m\per\m}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}

# {{{ Steel elasticity
FILE = "Steel_elasticity_vs_temperature"
data = pd.read_csv(FILE + ".csv", skiprows=[0], names=["t", "y", "p"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.y/1e9, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"E, \si{\giga\pascal}")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + "_young.tikz", axis_width="10cm", axis_height="7cm")
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.t, data.p, linewidth=LINEWIDTH)
plt.grid()
# plt.yscale('log')
# plt.xscale('log')
plt.xlabel(r"t, \si{\K}")
plt.ylabel(r"$\nu$")
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + "_poisson.tikz", axis_width="10cm", axis_height="7cm")
plt.close()
# }}}
