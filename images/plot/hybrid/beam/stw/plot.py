#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib import colors
import tikzplotlib

LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2
RANGE = range(2, 25)

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

c_light = 299792458
f = 2856e6
lambda_v = c_light/f
lmb = lambda_v * 100
W0 = 0.511e6

# {{{ Functions
def get_ellipsis(x, y, nstd=1, col="red", lwd=1):
    def eigsorted(cov):
        vals, vecs = np.linalg.eigh(cov)
        order = vals.argsort()[::-1]
        return vals[order], vecs[:,order]
    cov = np.cov(x, y)
    vals, vecs = eigsorted(cov)
    theta = np.degrees(np.arctan2(*vecs[:,0][::-1]))
    w, h = 2 * nstd * np.sqrt(vals)
    ell = Ellipse(xy=(np.mean(x), np.mean(y)),
                  width=w, height=h,
                  angle=theta, color='black', lw=lwd)
    ell.set_facecolor('None')
    ell.set_edgecolor(col)
    return ell, {"alpha": theta * np.pi / 180, "a": w/2, "b": h/2}
def get_ellipsis_points(a, b, alpha):
    return {
        1: (a*b*np.sqrt(1/(a**2*np.sin(alpha)**2+b**2*np.cos(alpha)**2)), 0),
        2: (a*b*np.sqrt(
                np.cos(alpha)**4/
                (a**2*b**2*(a**2*np.cos(alpha)**2-b**2*np.cos(alpha)**2+b**2)))*(a**2+b**2*np.tan(alpha)**2),
            a*b*np.sqrt(
                np.cos(alpha)**2/
                (a**2*b**2*(a**2-b**2+b**2/np.cos(alpha)**2)))*
            (a-b)*(a+b)*np.tan(alpha)),
        3: (a*b*np.sqrt(
            np.cos(alpha)**2/
            (a**2*b**2*(-a**2+a**2/np.cos(alpha)**2+b**2)))*
            (a+b)*(a-b)*np.tan(alpha),
            a*b*np.sqrt(
                np.cos(alpha)**4/
                (a**2*b**2*(-a**2*np.cos(alpha)**2+a**2+b**2*np.cos(alpha)**2)))*(a**2*np.tan(alpha)**2+b**2)),
        4: (0, a*b*np.sqrt(1/(a**2*np.cos(alpha)**2+b**2*np.sin(alpha)**2))),
    }

def get_twiss(p1, p2, p3, p4):
    return {
        "alpha": -p3[0]/p1[0],
        "beta": p2[0]/p4[1],
        "gamma": p3[1]/p1[0],
        "epsilon": p2[0]*p4[1],
    }
# }}}

# {{{ Input
out_start = pd.read_csv("./OUT_DATA_START", delim_whitespace=True).dropna()

for c in ["Z", "X", "Y"]:
    out_start[c] = out_start[c] * lmb / 2 / np.pi  # renormalize

out_start.rename(columns={"VZ": "GAMMA_Z",
                          "VX": "BETA_X",
                          "VY": "BETA_Y",
                          "PHAS": "PHI",}, inplace=True)

out_finish = pd.read_csv("./OUT_DATA_FINISH", delim_whitespace=True).dropna()

for c in ["Z", "X", "Y"]:
    out_finish[c] = out_finish[c] * lmb / 2 / np.pi  # renormalize

out_finish.rename(columns={"VZ": "GAMMA_Z",
                           "VX": "BETA_X",
                           "VY": "BETA_Y",
                           "PHAS": "PHI",}, inplace=True)
# }}}

out_finish_cropped = out_finish[(out_finish.PHI / np.pi > -2) & (out_finish.PHI / np.pi < 2)]

plt.figure(figsize=(FIGSIZE), dpi=DPI)
plt.subplot(2, 1, 1)
plt.hist2d(out_finish_cropped.PHI / np.pi, out_finish_cropped.GAMMA_Z, bins=100, norm=colors.LogNorm())
plt.grid()
plt.ylabel(r"$\gamma_{Z}$")
plt.ticklabel_format(axis="both", style="sci", useOffset=True, scilimits=(-2,2))
plt.xlim(0.75, 1.5)

plt.subplot(2, 1, 2)
# plt.hist(out_start.PHI / np.pi, bins=100, weights=np.repeat(100/out_start.PHI.size, out_start.PHI.size))
plt.hist(out_finish_cropped.PHI / np.pi, bins=100, weights=np.repeat(100/out_finish_cropped.PHI.size, out_finish_cropped.PHI.size))
plt.grid()
plt.ylabel("N")
plt.xlabel(r"$\varphi/\pi$")
plt.xlim(0.75, 1.5)
plt.tight_layout()

plt.savefig("gamma_over_phi.png")

plt.figure(figsize=(FIGSIZE), dpi=DPI)
plt.subplot(2, 1, 1)
plt.hist2d(out_finish_cropped.Z, out_finish_cropped.GAMMA_Z, bins=100, norm=colors.LogNorm())
plt.grid()
plt.ylabel(r"$\gamma_{Z}$")
plt.xlim(10.8, 12)
plt.ticklabel_format(axis="both", style="sci", useOffset=True, scilimits=(-2,2))

plt.subplot(2, 1, 2)
plt.hist(out_finish_cropped.Z, bins=100, weights=np.repeat(100/out_finish_cropped.Z.size, out_finish_cropped.Z.size))
plt.grid()
plt.xlim(10.8, 12)
plt.ylabel("N")
plt.xlabel("z, cm")
plt.tight_layout()

plt.savefig("gamma_over_l.png")

plt.figure(figsize=(FIGSIZE), dpi=DPI)
ax = plt.subplot(1, 1, 1)
plt.scatter(out_start.X, out_start.Y, color="cornflowerblue", alpha=0.7)
plt.scatter(out_finish_cropped.X, out_finish_cropped.Y, color="salmon", marker="^", alpha=0.7)
e, _ = get_ellipsis(out_start.X, out_start.Y, 3, col="b", lwd=2)
ax.add_artist(e)
e, _ = get_ellipsis(out_finish_cropped.X, out_finish_cropped.Y, 3, col="r", lwd=2)
ax.add_artist(e)
plt.grid()
plt.ylabel("y, cm")
plt.xlabel("x, cm")
plt.legend(["на входе", "на выходе"])
plt.tight_layout()

plt.savefig("profile.png")

plt.figure(figsize=(FIGSIZE), dpi=DPI)
ax = plt.subplot(1, 1, 1)
plt.scatter(out_start.X, out_start.BETA_X, color="cornflowerblue")
plt.scatter(out_finish_cropped.X, out_finish_cropped.BETA_X, color="salmon", marker="^")
e, _ = get_ellipsis(out_start.X, out_start.BETA_X, 3, col="b", lwd=2)
ax.add_artist(e)
e, _ = get_ellipsis(out_finish_cropped.X, out_finish_cropped.BETA_X, 3, col="r", lwd=2)
ax.add_artist(e)
plt.grid(e)
plt.ylabel(r"$\bar{v}_{x}$")
plt.xlabel("x, cm")
plt.legend(["на входе", "на выходе"])
plt.tight_layout()

plt.savefig("emit.png")


plt.figure(figsize=(FIGSIZE), dpi=DPI)
ax = plt.subplot(211)
# plt.hist(out_start.X, bins=100, weights=np.repeat(100/out_start.X.size, out_start.X.size))
# plt.hist(out_finish_cropped.X, bins=100, weights=np.repeat(100/out_finish_cropped.X.size, out_finish_cropped.X.size))
plt.hist(out_start.X, histtype='bar', bins=np.arange(-0.3,0.3,step=0.01), hatch='/', alpha=0.7)
plt.hist(out_finish_cropped.X, histtype='bar', bins=np.arange(-0.3,0.3,step=0.01), hatch='\\', alpha=0.7)
plt.legend(["на входе", "на выходе"])
plt.grid()
plt.ylabel("N")
plt.xlabel("x, cm")
ax = plt.subplot(212)
plt.hist(out_start.Y, bins=np.arange(-0.3,0.3,step=0.01), hatch='/', alpha=0.7)
plt.hist(out_finish_cropped.Y, bins=np.arange(-0.3,0.3,step=0.01), hatch='\\', alpha=0.7)
plt.legend(["на входе", "на выходе"])
plt.grid()
plt.ylabel("N")
plt.xlabel("y, cm")
plt.tight_layout()

plt.savefig("profile_size.png")

plt.figure(figsize=(FIGSIZE), dpi=DPI)
plt.subplot(1, 1, 1)
plt.hist((out_finish_cropped.GAMMA_Z-1)*W0/1e6, bins=100, weights=np.repeat(100/out_finish_cropped.GAMMA_Z.size, out_finish_cropped.GAMMA_Z.size))
plt.ylabel("N")
plt.xlabel("W, МэВ")
plt.grid()
plt.tight_layout()

plt.savefig("energy.png")

N = 100
data_hist = np.zeros((2,N))
data = np.sort(out_finish_cropped.GAMMA_Z-1)*W0/1e6
data_step = (data[-1] - data[0]) / N

data_current = data[0]
for i in np.arange(0, N):
    ind = np.where((data >= data_current) & (data < data_current + data_step))[0]
    d = data[ind]
    data_hist[0,i] = np.mean(d)
    data_hist[1,i] = d.size / data.size
    data_current += data_step

rms = np.sqrt(1/data_hist[0,:].size*np.sum(data_hist[1,:]**2))
print(f"RMS width {np.sum(data_hist[1,data_hist[1,:] >= rms])*100:.02f}")
