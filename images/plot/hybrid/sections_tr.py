#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import tikzplotlib
import re


FILE = "sections_tr"
XLAB_TR = r"$N_{\textup{БВ}}$"
XLAB_ST = r"$N_{\textup{СВ}}$"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2
RANGE = range(2, 25)
MARKERS = ["o", "v", "s", "D", "^", "<", ">", "P", "X"]
MARKERSIZE = 3


plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

beginaxis = re.compile(r'begin\{axis\}')
begintikz = re.compile(r'begin\{tikzpicture\}')


# Common

rsh_st = 75e6
rsh_tr = 100e6
alpha = 0.3
I0 = np.array([0.1, 0.2, 0.3, 0.4, 0.5])
n = np.arange(1, 100, 1)
D_st = 58e-3
D_tr = 33e-3
length_st = n * D_st
length_tr = n * D_tr
length_tr2 = np.arange(0, 5, 0.01)[1:]
Rsh_st = rsh_st * length_st
Rsh_tr = rsh_tr * length_tr
tau = alpha * length_tr
Eacc = np.repeat(1.0, n.size)
Eacc[0:3] = np.array([8, 14, 15])/15
Eacc = Eacc * 15e6
P0_in = np.array([3.2, 4.7, 6.2, 8.5, 10]) * 1e6
Qst = 12e3
f0 = 2856e6
v_gr_tr = 0.01

use_png = False

# Traveling wave


## {{{ P_vs_I
plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(I0, P0_in/1e6, marker='o')
# plt.legend([str(f"$I_п$ = {round(x, ROUND)} А").replace(".", ",") for x in I0])
# plt.ylim([0,12])
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_P_vs_I.png")
else:
    plt.ylabel(r"$P_{\textup{ВХ}}, \si{\MW}$")
    plt.xlabel(r"$I_{\textup{п}}, \si{\A}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_P_vs_I.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
            if beginaxis.search(line) is not None:
                f.write("xtick distance=0.1,\n")
plt.close()
## }}}

def Etatrl(i):
    """
    \[\eta^{tr} = \sqrt{\frac{r_{sh} L}{P_{in}}}
    \sqrt{\frac{2}{\alpha L}}
    \left(1-e^{-\alpha L}\right) I_0 -
    I_0^2 \frac{r_{sh} L}{P_{in}}
    \left(1 - \frac{1-e^{-\alpha L}}{\alpha L}\right)\]
    """
    return (np.sqrt(rsh_tr*length_tr2/P0_in[i])*np.sqrt(2/(alpha*length_tr2)) *
            (1-np.exp(-(alpha*length_tr2))) * I0[i] -
            I0[i]**2 * rsh_tr*length_tr2/P0_in[i] *
            (1 - (1-np.exp(-(alpha*length_tr2)))/(alpha*length_tr2)))

## {{{ eta_trl
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(length_tr2[Etatrl(i) > 0.05], Etatrl(i)[Etatrl(i) > 0.05]*100)
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_eta_trl.png")
else:
    plt.xlabel(r"L, \si{\m}")
    plt.ylabel(r"$\eta, \si{\percent}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_eta_trl.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}


def Etatr(i):
    """
    \[\eta^{tr} = \sqrt{\frac{R_{sh}}{P_{in}}}
    \sqrt{\frac{2}{\tau}}
    \left(1-e^{-\tau}\right) I_0 -
    I_0^2 \frac{R_{sh}}{P_{in}}
    \left(1 - \frac{1-e^{-\tau}}{\tau}\right)\]
    """
    return (np.sqrt(Rsh_tr/P0_in[i])*np.sqrt(2/tau) *
            (1-np.exp(-tau)) * I0[i] -
            I0[i]**2 * Rsh_tr/P0_in[i] *
            (1 - (1-np.exp(-tau))/tau))

## {{{ eta_tr
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    # plt.plot(n[Etatr(i) > 0.05], Etatr(i)[Etatr(i) > 0.05]*100, marker=MARKERS[i])
    plt.plot(n[RANGE], Etatr(i)[RANGE]*100, marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_eta_tr.png")
else:
    plt.xlabel(XLAB_TR)
    plt.ylabel(r"$\eta, \si{\percent}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_eta_tr.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}


def Wtr(i):
    """
    \[W^{tr} = \sqrt{\frac{2 R_{sh} P_{in}}{\tau}}\left(1-e^{-\tau}\right) -
    I_0 R_{sh} \left(1-\frac{1-e^{-\tau}}{\tau}\right)\]
    """
    return (np.sqrt(2*Rsh_tr*P0_in[i]/tau)*(1-np.exp(-tau)) -
            I0[i]*Rsh_tr*(1-(1-np.exp(-tau))/(tau)))


def Ptr_woc(i):
    """
    \[P^{tr} = P_0 \left(1-e^{-2\tau}\right)\]
    """
    return (P0_in[i]*(1-np.exp(-2*tau)))


def Etr_l(n, D, P0, alpha, rsh):
    """
    \[E^{tr} = \sqrt{r_{sh} P_0}
    \left(1-e^{-\alpha N D}\right)\]
    """
    return (np.sqrt(rsh*P0)*np.exp(-alpha*n*D))

## {{{ E_tr
plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(n, Etr_l(n, D_tr, P0_in[0], alpha, rsh_tr)/1e6, marker=MARKERS[i])
plt.locator_params(axis="x", integer=True)
# plt.ylim([0,12])
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_E_tr.png")
else:
    plt.xlabel("N")
    plt.ylabel(r"E, \si{\MV}")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_E_tr.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}

# for nn, ee in enumerate(Etr_l(np.arange(25, 0, -1), D_tr, P0_in[0], alpha, rsh_tr) / 1e5):
#     print(f"       {nn:2.0f}     1.0   0.03498   0.02898   {ee:5.1f}       0       0   0   0.0105   0.005      0  1e+008")

wtr = np.array([Wtr(i) for i, _ in enumerate(I0)])
ptr_woc = np.array([Ptr_woc(i) for i, _ in enumerate(I0)])

for i, _ in enumerate(I0):
    wtr[i][np.argwhere(np.sign(np.diff(wtr[i])) < 0)] = np.NaN
    wtr[i][-1] = np.NaN
    wtr[i][np.argwhere(wtr[i][~np.isnan(wtr[i])] > 10e6)] = np.NaN

## {{{ tr
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], wtr[i][RANGE]/1e6, marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_tr.png")
else:
    plt.xlabel(XLAB_TR)
    plt.ylabel(r"$\Delta W, \si{\MeV}$")
    code = tikzplotlib.get_tikz_code(axis_width="7cm", axis_height="5cm")
    with open(FILE + "_tr.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}

ptr = np.array([I*wtr[i] + ptr_woc[i] for i, I in enumerate(I0)])

for i, _ in enumerate(I0):
    ptr[i][ptr[i] > P0_in[i]] = np.NaN

## {{{ ptr
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], ptr[i][RANGE]/1e6, marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_ptr.png")
else:
    plt.xlabel(XLAB_TR)
    plt.ylabel(r"$P_{\textup{БВ}}, \si{\MW}$")
    code = tikzplotlib.get_tikz_code(axis_width="7cm", axis_height="5cm")
    with open(FILE + "_ptr.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}

## {{{ ptr_out
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[:45], (P0_in[i] - ptr[i])[:45]/1e6, marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_ptr_out.png")
else:
    plt.xlabel(XLAB_TR)
    plt.ylabel(r"$P_{\textup{БВ}}, \si{\MW}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_ptr_out.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}

## {{{ ptr_out_del
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[:45], ((P0_in[i] - ptr_woc[i]) - (P0_in[i] - ptr[i]))[:45]/1e6, marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_ptr_out_del.png")
else:
    plt.xlabel(XLAB_TR)
    plt.ylabel(r"$P_{\textup{БВ}}, \si{\MW}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_ptr_out_del.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}

## {{{ ptr_out_frac
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[:45], ((P0_in[i] - ptr_woc[i]) / (P0_in[i] - ptr[i]))[:45], marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_ptr_out_frac.png")
else:
    plt.xlabel(XLAB_TR)
    plt.ylabel(r"$P_{\textup{БВ}}, \si{\MW}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_ptr_out_frac.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}


# Standing wave


def Pg_opt_st(i):
    """
    \[P^{st}_g = \frac{E_{g} L \left(E_{g} +\sqrt{E_{g}^{2} + I_{0}^{2} r_{sh}^{2}}\right)}{2 r_{sh}}\]
    """
    return(Eacc**2*length_st*(1+np.sqrt(1+(I0[i]*rsh_st/Eacc)**2))/2/rsh_st)


Pst = np.array([Pg_opt_st(i) for i, _ in enumerate(I0)])

for i, _ in enumerate(I0):
    Pst[i][Pst[i] > P0_in[i]] = np.NaN

## {{{ pst
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], Pst[i][RANGE]/1e6, marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_pst.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$P_{\textup{СВ}}, \si{\MW}$")
    code = tikzplotlib.get_tikz_code(axis_width="7cm", axis_height="5cm")
    with open(FILE + "_pst.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}


def chiopt(i):
    """
    \[\chi_{opt} = \left[\frac{I_0}{2} \sqrt{\frac{R_{sh}}{P_{st}}} +
    \sqrt{1+\frac{I_0^2 R_{sh}}{4 P_{st}}}\right]^2\]
    """
    return((I0[i]*np.sqrt(Rsh_st/Pst[i])/2 +
            np.sqrt(1+I0[i]**2*Rsh_st/4/Pst[i]))**2)


chi = np.array([chiopt(i) for i, _ in enumerate(I0)])

## {{{ chi
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[5:], chi[i][5:], marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_chi.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$\chi$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_chi.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}


def Wst(i):
    """
    \[W^{st} = \sqrt{R_{sh} P_{st}}\frac{2 \sqrt{\chi}}{1+\chi}
    \left(1-\sqrt{\frac{R_{sh}}{P_{st}}}
    \frac{I_0}{2 \sqrt{\chi}}\right)\]
    """
    return (np.sqrt(Rsh_st*Pst[i])*2*np.sqrt(chi[i])/(1+chi[i]) *
            (1-np.sqrt(Rsh_st/Pst[i])*I0[i]/2/np.sqrt(chi[i])))


wst = np.array([Wst(i) for i, _ in enumerate(I0)])

wst[wst > 10e6] = np.NaN

## {{{ st
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE],
             (wst[i][RANGE]/1e6), marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_st.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$\Delta W, \si{\MeV}$")
    code = tikzplotlib.get_tikz_code(axis_width="7cm", axis_height="5cm")
    with open(FILE + "_st.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}


cells = np.empty((I0.size, n.size), dtype="int")
for i, I in enumerate(I0):
    for j, _ in enumerate(n):
        ind = np.where(10e6-wtr[i]-wst[i][j] < 0.1e6)[0]
        cells[i, j] = ind[0] if ind.size > 0 else 0


cellsp = cells.astype("float")
cellsp[cellsp == 0] = np.NaN

## {{{ st_vs_tr
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], cellsp[i][RANGE], marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0])
# plt.ylim([0,12])
plt.locator_params(axis="both", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_st_vs_tr.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(XLAB_TR)
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_st_vs_tr.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], cellsp[i][RANGE], marker=MARKERS[i], markersize=MARKERSIZE)
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
plt.locator_params(axis="both", integer=True)
plt.grid()
if use_png:
    plt.savefig(FILE + "_st_vs_tr_syn.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(XLAB_TR)
    code = tikzplotlib.get_tikz_code(axis_width="5.7cm", axis_height="4.3cm")
    with open(FILE + "_st_vs_tr_syn.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], cellsp[i][RANGE], marker=MARKERS[i], markersize=MARKERSIZE)
# plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
#            loc='lower right', bbox_to_anchor=(1, 1.05),
#            fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="both", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_st_vs_tr_pres.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(XLAB_TR)
    code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
    with open(FILE + "_st_vs_tr_pres.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
            if beginaxis.search(line) is not None:
                f.write("xtick distance=3,\n")
            if begintikz.search(line) is not None:
                f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()
## }}}

Pl = np.array([(P0_in[i] - ptr[i][cells[i]] - Pst[i])
               for i, I in enumerate(I0)])

Pl[cells == 0] = np.NaN
Pl[Pl <= 0] = np.NaN

## {{{ load
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], Pl[i][RANGE]/1e6, marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='upper left', bbox_to_anchor=(1.02, 1),
           fancybox=True, shadow=True, ncol=1)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_load.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$P_{\textup{наг}}, \si{\MW}$")
    code = tikzplotlib.get_tikz_code(axis_width="7cm", axis_height="5cm")
    with open(FILE + "_load.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}


W = np.array([np.array(wst[i] + wtr[i][cells[i]]) for i, _ in enumerate(I0)])
W[cells == 0] = np.NaN

## {{{ W
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n, W[i]/1e6, marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_W.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"W, \si{\MeV}")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_W.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}

eta = np.array([I*(wtr[i][cells[i]] +
                wst[i]) / P0_in[i]
               for i, I in enumerate(I0)])

eta[np.isnan(Pl)] = np.NaN

## {{{ eta
plt.figure(figsize=FIGSIZE, dpi=DPI)
fig, ax = plt.subplots()
colors = (plt.rcParams['axes.prop_cycle']).by_key()['color']
for i, _ in enumerate(I0):
    ax.plot(n[RANGE], eta[i][RANGE]*100, marker=MARKERS[i], color=colors[i])
for i, _ in enumerate(I0):
    ax.axhline(Etatr(i)[cells[i][3]]*100, 0.05, 0.95, linestyle="--", color=colors[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='upper left', bbox_to_anchor=(1.02, 1),
           fancybox=True, shadow=True, ncol=1)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_eta.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$\eta, \si{\percent}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_eta.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
fig, ax = plt.subplots()
colors = (plt.rcParams['axes.prop_cycle']).by_key()['color']
for i, _ in enumerate(I0):
    ax.plot(n[RANGE], eta[i][RANGE]*100, marker=MARKERS[i], color=colors[i], markersize=MARKERSIZE)
for i, _ in enumerate(I0):
    ax.axhline(Etatr(i)[cells[i][3]]*100, 0.05, 0.95, linestyle="--", color=colors[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='upper left', bbox_to_anchor=(1.02, 1),
           fancybox=True, shadow=True, ncol=1)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_eta.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$\eta, \si{\percent}$")
    code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
    with open(FILE + "_eta_pres.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
            if beginaxis.search(line) is not None:
                f.write("xtick distance=3,\n")
            if begintikz.search(line) is not None:
                f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()
## }}}

frac = np.array([Pst[i] / (P0_in[i] - ptr[i][cells[i]])
                 for i, I in enumerate(I0)])

frac[np.isnan(Pl)] = np.NaN

## {{{ frac
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], frac[i][RANGE], marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_frac.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"${P_{\textup{ВХ}}^{\textup{СВ}}}/{P_{\textup{ВЫХ}}^{\textup{БВ}}}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_frac.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}

frac_db = 10*np.log10(1/frac)

## {{{ frac_db
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], frac_db[i][RANGE], marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0])
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_frac_db.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"${P_{\textup{ВХ}}^{\textup{СВ}}}/{P_{\textup{ВЫХ}}^{\textup{БВ}}}, \si{\dB}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_frac_db.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], frac_db[i][RANGE], marker=MARKERS[i], markersize=MARKERSIZE)
# plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
#            loc='upper left', bbox_to_anchor=(1.02, 1),
#            fancybox=True, shadow=True, ncol=1)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_frac_db.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"${P_{\textup{ВХ}}^{\textup{СВ}}}/{P_{\textup{ВЫХ}}^{\textup{БВ}}}, \si{\dB}$")
    code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
    with open(FILE + "_frac_db_pres.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
            if begintikz.search(line) is not None:
                f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()
## }}}


def Etast(i):
    """
    \[\eta^{st} = \frac{2 \sqrt{\chi}}{1+\chi}\left(
    I_0 \sqrt{\frac{R_{sh}}{P_{st}}} -
    \frac{I_0^2}{2}\frac{R_{sh}}{P_{st} \sqrt{\chi}}\right)\]
    """
    return(2*np.sqrt(chi[i])/(1+chi[i]) *
           (I0[i] * np.sqrt(Rsh_st/Pst[i]) -
            I0[i]**2/2 * Rsh_st/Pst[i]/np.sqrt(chi[i])))


eta_st = np.array([I*wst[i] / Pst[i] for i, I in enumerate(I0)])
eta_st2 = np.array([Etast(i) for i, I in enumerate(I0)])

## {{{ eta_st
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], eta_st2[i][RANGE]*100, marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
# plt.ylim([0,12])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_eta_st.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$\eta, \si{\percent}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_eta_st.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}

taust = Qst/(np.pi*f0*(1+chi))

t_st = np.array([-taust[i] * np.log(1-np.sqrt(((P0_in[i] - ptr[i][cells[i]])/2) /
                                          (P0_in[i] - ptr[i][cells[i]]))) for i, I in enumerate(I0)])
t_tr = np.array([((i * D_st) + (cells[i] * D_tr)) / (299792458 * v_gr_tr) for i, I in enumerate(I0)])
t_hst = np.array([-taust[i] * np.log(1-np.sqrt(((P0_in[i] - ptr[i][cells[i]])/2) /
                                           (P0_in[i] - ptr_woc[i][cells[i]]))) for i, I in enumerate(I0)])
t_htr = np.array([(cells[i] * D_tr) / (299792458 * v_gr_tr) for i, I in enumerate(I0)])

tfrac = np.array([t_hst[i] / t_st[i] for i, I in enumerate(I0)])
tfrac[cells == 0] = np.NaN

## {{{ tfrac
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], tfrac[i][RANGE], marker=MARKERS[i])
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0])
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_tfrac.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$k_{\textup{з}}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_tfrac.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], tfrac[i][RANGE], marker=MARKERS[i], markersize=MARKERSIZE)
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower right', bbox_to_anchor=(1, 1.02),
           fancybox=True, shadow=True, ncol=2)
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_tfrac_pres.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$k_{\textup{з}}$")
    code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
    with open(FILE + "_tfrac_pres.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
            if begintikz.search(line) is not None:
                f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()

## }}}

eta_tr = np.array([Etatr(i)[cells[i]] for i, _ in enumerate(I0)])

k = np.linspace(1.9, 1, num=len(t_st[0]))
time_tr = ((eta_tr * (t_hst + t_htr) - eta * t_tr) / (eta - eta_tr))
time_st = ((eta_st * (t_hst + t_htr) - eta * k * t_st) / (eta - eta_st))

time_tr[time_tr <= 2e-8] = np.NaN
time_st[time_st <= 2e-8] = np.NaN

time_tr[time_st <= time_tr] = np.NaN

time_st[np.isnan(time_tr)] = np.NaN

## {{{ time
plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], time_st[i][RANGE], marker=MARKERS[i], color=f"C{i}")
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower left', bbox_to_anchor=(1.02, 0),
           fancybox=True, shadow=True, ncol=1)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], time_tr[i][RANGE], marker=MARKERS[i], color=f"C{i}", linestyle="--")
plt.yscale("log")
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_time.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$T_{\textup{имп}}, \si{\s}$")
    code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
    with open(FILE + "_time.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], time_st[i][RANGE], marker=MARKERS[i], color=f"C{i}")
plt.legend([str(f"$legendplaceholder$ = {round(x, ROUND)} А").replace(".", ",") for x in I0],
           loc='lower left', bbox_to_anchor=(1.02, 0),
           fancybox=True, shadow=True, ncol=1)
for i, _ in enumerate(I0):
    plt.plot(n[RANGE], time_tr[i][RANGE], marker=MARKERS[i], color=f"C{i}", linestyle="--")
plt.yscale("log")
plt.locator_params(axis="x", integer=True)
plt.grid()
# plt.show()
if use_png:
    plt.savefig(FILE + "_time_pres.png")
else:
    plt.xlabel(XLAB_ST)
    plt.ylabel(r"$T_{\textup{имп}}, \si{\s}$")
    code = tikzplotlib.get_tikz_code(axis_width="6.5cm", axis_height="4.5cm")
    with open(FILE + "_time_pres.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
plt.close()
## }}}

# {{{ Eta heatmap
t = np.linspace(1e-8, 1e-5)
for i, _ in enumerate(I0):
    data = np.empty((n.size, t.size), dtype='float')
    for nm, _ in enumerate(n):
        data[nm] = t/(t+t_hst[i][nm])*eta[i][nm]*100
    cols = np.where(np.apply_along_axis(np.any, 1, ~np.isnan(data)))[0]
    fig = plt.figure()
    plot = plt.pcolor(cols, t, np.transpose(data[cols, :]), shading='auto')
    fig.colorbar(plot)
    plt.xlabel('$N$')
    plt.ylabel('$T_{\textup{и}}$')
    plt.title(r'$\eta$, \si{\percent} ($legendplaceholder =\SI{' + str(I0[i]) + '}{\A})$')
    if use_png:
        plt.savefig(FILE + f"_eta_heatmap_{I0[i]}_pres.png")
    else:
        plt.xlabel(XLAB_ST)
        plt.ylabel(r"$T_{\textup{имп}}, \si{\s}$")
        code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
        with open(FILE + f"_eta_heatmap_{I0[i]}_pres.tikz", "w") as f:
            for line in code.split('\n'):
                f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
    plt.close()
# }}}

# print(f"cells = {cells}")
# print(f"chi = {chi[:,2]}")
# print(f"frac (dB): {frac_db}")
