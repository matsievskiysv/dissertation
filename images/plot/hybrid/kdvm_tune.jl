#!/usr/bin/env julia

using Plots
using DataFrames
using CSV

pyplot(size=(800,400))

FILE = "kdvm_tune"

if match(r"(\w+)\.jl", PROGRAM_FILE) != nothing
    FILE = match(r"(\w+)\.jl", PROGRAM_FILE)[1]
end

function getS11(Г0, Г1, Г2, ϕ)
    ν10 = angle(Г1/Г0)/2+ϕ
    ν21 = angle(Г2/Г1)/2+ϕ

    α = 2ϕ-ν10-ν21

    # δ2 = atan( 1/( 1/tan(α) - sin(ν10)/(sin(ν21)*sin(α))) )

    sinδ2_1 = sin(α)/sqrt(1-2*sin(ν10)/sin(ν21)*cos(α) + (sin(ν10)/sin(ν21))^2)
    sinδ2_2 = -sinδ2_1
    cosδ2_1 = (cos(α) - sin(ν10)/sin(ν21)) / sqrt(1-2*sin(ν10)/sin(ν21)*cos(α) + (sin(ν10)/sin(ν21))^2)
    cosδ2_2 = -cosδ2_1

    # println("-"^20)
    # println(sinδ2_1, "\t", sinδ2_2)
    # println(cosδ2_1, "\t", cosδ2_2)

    # S11 = 1/sqrt(Complex(1-4*sin(ϕ)/sin(ν21)*((cos(ϕ-ν21)-sin(ϕ)/sin(ν21))*sin(δ2)-sin(ϕ-ν21)*sin(δ2)*cos(δ2))))

    S11_1 = 1/sqrt(Complex(1-4*sin(ϕ)/sin(ν21)*((cos(ϕ-ν21)-sin(ϕ)/sin(ν21))*sinδ2_1-sin(ϕ-ν21)*sinδ2_1*cosδ2_1)))
    S11_2 = 1/sqrt(Complex(1-4*sin(ϕ)/sin(ν21)*((cos(ϕ-ν21)-sin(ϕ)/sin(ν21))*sinδ2_2-sin(ϕ-ν21)*sinδ2_2*cosδ2_2)))
    # println(S11, "\t", S11_1, "\t", S11_2)
    # println("-"^20)
    return imag(S11_1) == 0 ? real(S11_1) : real(S11_2)
end


for (i,j) ∈ Dict([(:Г1, "kdvm_tune_1.csv"), (:Г2, "kdvm_tune_2.csv"), (:Г3, "kdvm_tune_3.csv")])
    data = CSV.read(j; header=["row", "R", "W", "Im", "Re"], skipto=2) |> dropmissing
    data[i] = data.Re + data.Im*im
    data = data[!, [:R, :W, i]]
    eval(:($i = $data))
end

Г = join(Г1, Г2, on = [:R, :W])
Г = join(Г, Г3, on = [:R, :W])

Г.S11 = getS11.(Г.Г1, Г.Г2, Г.Г3, -2π/3)
Г.S11db = 20*log10.(Г.S11)

plot(Г.R, Г.W, Г.S11db, st=:surface, cam=(30,10),
     xaxis = ("R, mm"),
     yaxis = ("W, mm"),
     zaxis = ("S11, dB"));
savefig(FILE * ".png")


println(Г[Г.S11db .< -25, [:R, :W, :S11db]])
