#!/usr/bin/python3
from os import path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

FILE = "stability"
try:
    FILE = path.splitext(path.basename(__file__))[0]
except:
    pass
YLAB = r"$\Delta$W, МэВ"
XLAB_TR = "$N_{БВ}$"
XLAB_ST = "$N_{СВ}$"
LINEWIDTH = 5
LINEWIDTHPRES = 2
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
MARKERSIZE = 3


plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

begintikz = re.compile(r'begin\{tikzpicture\}')
beginaxis = re.compile(r'begin\{axis\}')

for s in ["stable", "unstable"]:

    d = pd.read_csv(FILE + "_" + s + ".csv")

    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.plot(d.f, d.section, linewidth=LINEWIDTH)
    plt.plot(d.f, d.generator, linewidth=LINEWIDTH, linestyle="--")
    idx1 = (np.argwhere(np.diff(np.sign(d.section - d.generator)) > 0)).flatten()
    idx2 = (np.argwhere(np.diff(np.sign(d.section - d.generator)) < 0)).flatten()
    plt.plot(d.f[idx1], d.generator[idx1], 'go')
    plt.plot(d.f[idx2], d.generator[idx2], 'r^')
    plt.legend(["Секция", "Генератор"])

    plt.xlabel(r"$f, \si{\MHz}$")
    plt.ylabel(r"$\Im{(Y)}$")
    plt.ylim((d.section.min()-np.abs(d.section.min()*0.1),
              d.section.max()+np.abs(d.section.max()*0.1)))
    plt.grid()
    # plt.tight_layout()
    # plt.show()
    # plt.savefig(FILE + s)
    code = tikzplotlib.get_tikz_code(axis_width="7.5cm", axis_height="7cm")
    with open(FILE + "_" + s + ".tikz", "w") as f:
        for l in code.split('\n'):
            f.write(l + '\n')
            if beginaxis.search(l) is not None:
                f.write("xtick distance=3,\n")
    plt.close()


    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.plot(d.f, d.section, linewidth=LINEWIDTHPRES)
    plt.plot(d.f, d.generator, linewidth=LINEWIDTHPRES, linestyle="--")
    idx1 = (np.argwhere(np.diff(np.sign(d.section - d.generator)) > 0)).flatten()
    idx2 = (np.argwhere(np.diff(np.sign(d.section - d.generator)) < 0)).flatten()
    plt.plot(d.f[idx1], d.generator[idx1], 'go', markersize=MARKERSIZE)
    plt.plot(d.f[idx2], d.generator[idx2], 'r^', markersize=MARKERSIZE)
    plt.legend(["Секция", "Генератор"],
               loc='lower right', bbox_to_anchor=(1, 1.02),
               fancybox=True, shadow=True, ncol=1)

    plt.xlabel(r"$f, \si{\MHz}$")
    plt.ylabel(r"$\Im{(Y)}$")
    plt.ylim((d.section.min()-np.abs(d.section.min()*0.1),
              d.section.max()+np.abs(d.section.max()*0.1)))
    plt.grid()
    # plt.tight_layout()
    # plt.show()
    # plt.savefig(FILE + s)
    code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
    with open(FILE + "_" + s + "_pres.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("I_п", r"I_{\\textup{п}}", line) + '\n')
            if begintikz.search(line) is not None:
                f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
    plt.close()
