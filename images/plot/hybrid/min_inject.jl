λ=10 # cm
E=50 # kV/cm
W0=0.511e3 # keV
G=E*λ/W0

β_min = (1-(G/π)^2)/(1+(G/π)^2)
γ_min = (1+(G/π)^2)/(2G/π)
W_min = (γ_min*W0-W0)/1e3 # MeV

