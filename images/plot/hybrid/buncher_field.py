#!/usr/bin/python3
from os import path
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib

FILE = path.splitext(path.basename(__file__))[0]
YLAB = r"E, \si{\kV\per\cm}"
XLAB = r"L, \si{\mm}"
LINEWIDTH = 5
FIGSIZE = (9, 5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

data = pd.read_csv(FILE + ".txt", comment="#", sep="\t", names=["l", "e"])

plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data.l-data.l[0], data.e[::-1], linewidth=LINEWIDTH)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
