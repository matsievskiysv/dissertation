#!/usr/bin/python3
from os import path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

FILE = path.splitext(path.basename(__file__))[0]
YLAB = r"$|S_{11}|$, дБ"
XLAB = r"$N_{\textup{СВ}}$"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
MARKERS = ["o", "v", "s", "D", "^", "<", ">", "P", "X"]
MARKERSIZE = 3


plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

begintikz = re.compile(r'begin\{tikzpicture\}')

data = pd.read_csv(FILE + ".csv")

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, c in enumerate(data.columns[1:]):
    plt.plot(data.N, data[c], marker=MARKERS[i])

plt.axhline(20*np.log10((1.2-1)/(1.2+1)), 0.01, 0.99, color="r")
plt.legend(data.columns[1:])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.show()
# plt.tight_layout()
# plt.savefig(FILE + ".png")
code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
with open(FILE + ".tikz", "w") as f:
    for line in code.split('\n'):
        f.write(re.sub("I_п", r"I_{\\textup{п}}", line) + '\n')
code = tikzplotlib.get_tikz_code(axis_width="5.7cm", axis_height="4.3cm")
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, c in enumerate(data.columns[1:]):
    plt.plot(data.N, data[c], marker=MARKERS[i], markersize=MARKERSIZE)

plt.axhline(20*np.log10((1.2-1)/(1.2+1)), 0.01, 0.99, color="r")
plt.legend(data.columns[1:],
           loc='lower right', bbox_to_anchor=(1, 1.05),
           fancybox=True, shadow=True, ncol=2)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
code = tikzplotlib.get_tikz_code(axis_width="5.7cm", axis_height="4.3cm")
with open(FILE + "_syn.tikz", "w") as f:
    for line in code.split('\n'):
        f.write(re.sub("I_п", r"I_{\\textup{п}}", line) + '\n')
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i, c in enumerate(data.columns[1:]):
    plt.plot(data.N, data[c], marker=MARKERS[i], markersize=MARKERSIZE)

plt.axhline(20*np.log10((1.2 - 1)/(1.2 + 1)), 0.01, 0.99, color="r")
plt.legend(data.columns[1:],
           loc='lower right', bbox_to_anchor=(1, 1.02),
           fancybox=True, shadow=True, ncol=2)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
with open(FILE + "_pres.tikz", "w") as f:
    for line in code.split('\n'):
        f.write(re.sub("I_п", r"I_{\\textup{п}}", line) + '\n')
        if begintikz.search(line) is not None:
            f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()
