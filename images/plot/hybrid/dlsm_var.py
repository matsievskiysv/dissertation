#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

LINEWIDTH = 5
FIGSIZE = (9, 5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

# {{{ n_l

n_l = pd.read_csv("dlsm_var_n_l.csv")

n_l["rsh"] = n_l["rsh"] / 1e6

fig = plt.figure(figsize=FIGSIZE, dpi=DPI)
left = fig.add_subplot(111)
right = left.twinx()

left.plot(n_l.n_l, n_l["rsh"], marker="o", color="C1", linestyle="--")
left.plot(n_l.n_l, n_l["rsh"], marker="o", color="C0")
right.plot(n_l.n_l, n_l["e_lambda_sqrt_P"], marker="o", color="C1", linestyle="--")

left.legend([r"legend1", r"legend2"],
            loc='lower right', bbox_to_anchor=(1, 0),
            fancybox=True, shadow=True, ncol=1)

left.set_xlabel(r"n_l, \si{\mm}")
left.set_ylabel(r"$r_{\textup{ш}}, \si{\MOhm\per\m}$")
right.set_ylabel(r"$E \lambda / \sqrt{P}, \si{\Ohm\tothe{1/2}}$")

left.grid()

code = tikzplotlib.get_tikz_code(axis_width="5cm", axis_height="7cm")
with open("dlsm_var_n_l.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(re.sub("legend2", r"$E \\lambda / \\sqrt{P}$",
                       re.sub("legend1",  r"$r_{\\textup{ш}}$", l)) + '\n')
plt.close()

# }}}

# {{{ cw_theta

k = pd.read_csv("dlsm_var_a_lambda.csv")

k["rsh"] = k["rsh"] / 1e6
k["k"] = k["k"] * 1e2

fig = plt.figure(figsize=FIGSIZE, dpi=DPI)
left = fig.add_subplot(111)
right = left.twinx()

left.plot(k.k, k["rsh"], marker="o", color="C1", linestyle="--")
left.plot(k.k, k["rsh"], marker="o", color="C0")
right.plot(k.k, k["e_lambda_sqrt_P"], marker="o", color="C1", linestyle="--")

left.legend([r"legend1", r"legend2"],
            loc='lower left', bbox_to_anchor=(0, 0),
            fancybox=True, shadow=True, ncol=1)

left.set_xlabel(r"a/\lambda\cdot\num{d2}")
left.set_ylabel(r"$r_{\textup{ш}}, \si{\MOhm\per\m}$")
right.set_ylabel(r"$E \lambda / \sqrt{P}, \si{\Ohm\tothe{1/2}}$")

left.grid()

code = tikzplotlib.get_tikz_code(axis_width="5cm", axis_height="7cm")
with open("dlsm_var_k.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(re.sub("legend2", r"$E \\lambda / \\sqrt{P}$",
                       re.sub("legend1",  r"$r_{\\textup{ш}}$", l)) + '\n')
plt.close()

# }}}
