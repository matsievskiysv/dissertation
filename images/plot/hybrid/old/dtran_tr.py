#!/usr/bin/python3
from os import path
import numpy as np
import matplotlib.pyplot as plt
import tikzplotlib

FILE = "dtran_tr"
try:
    FILE = path.splitext(path.basename(__file__))[0]
except:
    pass
YLAB = r"$t_n/t_0$"
XLAB = r"$N_{\textup{СВ}}$"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

data = np.genfromtxt(FILE + ".csv", delimiter=',', skip_header=1)

d = data[1:, 1:] / data[0, 1:]

plt.figure(figsize=FIGSIZE, dpi=DPI)
for i in range(0, d.shape[1]):
    plt.plot(data[1:, 0], d[:, i], marker="o")
plt.legend([f"$I_п$ = {i} А".replace(".", ",") for i in [0.1,0.2,0.3,0.4,0.5]])
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
tikzplotlib.save(FILE + ".tikz", axis_width="10cm", axis_height="7cm")
