#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import tikzplotlib
import re

LINEWIDTH = 5
LINEWIDTHPRES = 2
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18
ROUND = 2

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

beginaxis = re.compile(r'begin\{axis\}')
begintikz = re.compile(r'begin\{tikzpicture\}')

plt.figure(figsize=FIGSIZE, dpi=DPI)
for name, style in [("trans_orig", "--"), ("trans_forced", "-")]:
    data = pd.read_csv(name + ".csv")
    for i, cn in enumerate(data.filter(regex="X_").columns):
        plt.plot(1/2856*data.time, data[cn], linewidth=LINEWIDTH,
                 linestyle=style, color=f"C{i}")
plt.grid()
# plt.xscale('log')
# plt.tight_layout()
# plt.show()
# plt.savefig(FILE + ".png")
# plt.legend([r"\textup{Ячейка 1}",
#             r"\textup{Ячейка 2}",
#             r"\textup{Ячейка 3}",
#             r"\textup{Ячейка 4}",
#             r"\textup{Ячейка 5}"],
#            loc='upper center', bbox_to_anchor=(0.5, -0.3),
#            fancybox=True, shadow=True, ncol=2)
plt.xlabel(r"$t, \si{\micro\s}$")
plt.ylabel(r"$E, \si{\V\per\m}$")
tikzplotlib.save("trans.tikz", axis_width="10cm", axis_height="7cm")
plt.close()

plt.figure(figsize=FIGSIZE, dpi=DPI)
for name, style in [("trans_orig", "--"), ("trans_forced", "-")]:
    data = pd.read_csv(name + ".csv")
    for i, cn in enumerate(data.filter(regex="X_").columns):
        plt.plot(1/2856*data.time, data[cn], linewidth=LINEWIDTHPRES,
                 linestyle=style, color=f"C{i}")
plt.grid()
plt.xlabel(r"$t, \si{\micro\s}$")
plt.ylabel(r"$E, \si{\V\per\m}$")
code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3.8cm")
with open("trans_pres.tikz", "w") as f:
    for line in code.split('\n'):
        f.write(re.sub("legendplaceholder", r"I_{\\textup{п}}", line) + '\n')
        # if beginaxis.search(line) is not None:
        #     f.write("xtick distance=3,\n")
        if begintikz.search(line) is not None:
            f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()
