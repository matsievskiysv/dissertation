#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

LINEWIDTH = 5
FIGSIZE = (9, 5)
DPI = 300
FONTSIZE = 18
RANGE = slice(1,20)

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

begintikz = re.compile(r'begin\{tikzpicture\}')
beginaxis = re.compile(r'begin\{axis\}')

for name, label, scale in [("kdvm_e_mod", r"$|E|, \si{\MV\per\m}$", 1e6),
                           ("kdvm_e_phase", r"$\angle{E} \si{\degree}$", 1)]:

    data = pd.read_csv(name + ".csv", names=["N", "E"], skiprows=1)

    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.bar(data.N[RANGE], data.E[RANGE] / scale)
    plt.xlabel("n")
    plt.ylabel(label)
    plt.grid()
    tikzplotlib.save(name + ".tikz", axis_width="7cm", axis_height="5cm")
    plt.close()


    plt.figure(figsize=FIGSIZE, dpi=DPI)
    plt.bar(data.N[RANGE], data.E[RANGE] / scale)
    plt.xlabel("n")
    plt.ylabel(label)
    plt.grid()
    code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3cm")
    with open(name + "_pres.tikz", "w") as f:
        for line in code.split('\n'):
            f.write(re.sub("I_п", r"I_{\\textup{п}}", line) + '\n')
            if begintikz.search(line) is not None:
                f.write(r"\tikzstyle{every node}=[font=\scriptsize]" + '\n')

    plt.close()
