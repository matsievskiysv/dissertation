#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

LINEWIDTH = 5
FIGSIZE = (9, 5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

# {{{ n_l

n_l = pd.read_csv("bus_var_n_l.csv")

n_l["coup"] = (n_l.f3-n_l.f1)/n_l.f2*100
n_l["rsh"] = n_l["rsh"] / 1e6
n_l["rsh_eff"] = n_l["rsh_eff"] / 1e6

fig = plt.figure(figsize=FIGSIZE, dpi=DPI)
left = fig.add_subplot(111)
right = left.twinx()

left.plot(n_l.n_l, n_l["rsh_eff"], marker="o", color="C1", linestyle="--")
left.plot(n_l.n_l, n_l["rsh_eff"], marker="o", color="C0")
right.plot(n_l.n_l, n_l["Q0"], marker="o", color="C1", linestyle="--")

left.legend([r"legend1", r"legend2"],
            loc='lower center', bbox_to_anchor=(0.5, 0),
            fancybox=True, shadow=True, ncol=1)

left.set_xlabel(r"n_l, \si{\mm}")
left.set_ylabel(r"$r_{\textup{ш}}^{\textup{эфф}}, \si{\MOhm\per\m}$")
right.set_ylabel(r"$Q_0$")

left.grid()

code = tikzplotlib.get_tikz_code(axis_width="5cm", axis_height="7cm")
with open("bus_var_n_l.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(re.sub("legend2", r"$Q_0$",
                       re.sub("legend1",  r"$r_{\\textup{ш}}^{\\textup{эфф}}$", l)) + '\n')
plt.close()

# }}}

# {{{ cw_theta

cw_theta = pd.read_csv("bus_var_cw_theta.csv")

cw_theta["coup"] = (cw_theta.f3-cw_theta.f1)/cw_theta.f2*100
cw_theta["rsh"] = cw_theta["rsh"] / 1e6
cw_theta["rsh_eff"] = cw_theta["rsh_eff"] / 1e6

fig = plt.figure(figsize=FIGSIZE, dpi=DPI)
left = fig.add_subplot(111)
right = left.twinx()

left.plot(cw_theta.cw_theta, cw_theta["rsh_eff"], marker="o", color="C1", linestyle="--")
left.plot(cw_theta.cw_theta, cw_theta["rsh_eff"], marker="o", color="C0")
right.plot(cw_theta.cw_theta, cw_theta["Q0"], marker="o", color="C1", linestyle="--")

left.legend([r"legend1", r"legend2"])

left.set_xlabel(r"cw_theta, \si{\degree}")
left.set_ylabel(r"$r_{\textup{ш}}^{\textup{эфф}}, \si{\MOhm\per\m}$")
right.set_ylabel(r"$Q_0$")

left.grid()

code = tikzplotlib.get_tikz_code(axis_width="5cm", axis_height="7cm")
with open("bus_var_cw_theta.tikz", "w") as f:
    for l in code.split('\n'):
        f.write(re.sub("legend2", r"$Q_0$", re.sub("legend1",  r"$r_{\\textup{ш}}^{\\textup{эфф}}$", l)) + '\n')
plt.close()

# }}}
