#!/usr/bin/env bash

CURRDIR=$(pwd)
RED='\033[0;31m'
NC='\033[0m'

find "${CURRDIR}" -iname "*.py" -type f -print0 |
    while IFS= read -r -d '' file; do
	echo -e "${RED}Running $(basename ${file})${NC}"
	cd $(dirname "${file}")
	"${file}"
    done
