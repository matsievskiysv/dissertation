#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import tikzplotlib
import re

XLAB = r"L, \si{\mm}"
YLAB = r"$\hat E$"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

begintikz = re.compile(r'begin\{tikzpicture\}')
beginaxis = re.compile(r'begin\{axis\}')

amp = np.array([0.2, 0.6, 1])
pos1 = np.array([12, 36, 70])
pos2 = np.array([12, 34, 68])
length = np.array([15, 18, 40])

initial = np.genfromtxt("initial.tsv")
plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.bar(pos1, height=amp, width=length)
plt.plot(initial[:-8-13, 0],
         initial[13:-8, 1]/np.max(initial[:, 1]), color="C1",
         linewidth=LINEWIDTH)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
# plt.tight_layout()
# plt.savefig(FILE + ".png")
tikzplotlib.save("initial.tikz", axis_width="7cm", axis_height="5cm")
code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3cm")
with open("initial_pres.tikz", "w") as f:
    for line in code.split('\n'):
        f.write(re.sub("I_п", r"I_{\\textup{п}}", line) + '\n')
        if begintikz.search(line) is not None:
            f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()


tuned = np.genfromtxt("tuned.tsv")
plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.bar(pos2, height=amp, width=length)
plt.plot(tuned[-15:2:-1, 0],
         tuned[2:-15, 1]/np.max(tuned[:, 1]),
         linewidth=LINEWIDTH, color="C1")
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()

# plt.tight_layout()
# plt.savefig(FILE + ".png")
tikzplotlib.save("tuned.tikz", axis_width="7cm", axis_height="5cm")
code = tikzplotlib.get_tikz_code(axis_width="4.8cm", axis_height="3cm")
with open("tuned_pres.tikz", "w") as f:
    for line in code.split('\n'):
        f.write(re.sub("I_п", r"I_{\\textup{п}}", line) + '\n')
        if begintikz.search(line) is not None:
            f.write(r"\tikzstyle{every node}=[font=\footnotesize]" + '\n')
plt.close()
