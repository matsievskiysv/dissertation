#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
import re

FILE = "s12"
XLAB = r"f, \si{\MHz}"
YLAB = r"$S_{12}, \si{\dB}$"
LINEWIDTH = 5
FIGSIZE = (7, 5)
DPI = 300
FONTSIZE = 18

plt.rc('font', size=FONTSIZE)
plt.rc('axes', titlesize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('legend', fontsize=FONTSIZE)
plt.rc('figure', titlesize=FONTSIZE)

beginaxis = re.compile(r'begin\{axis\}')

data = pd.read_csv(FILE + ".csv", comment="#")
plt.figure(figsize=FIGSIZE, dpi=DPI)
plt.plot(data["freq"]/1e6, data["data_1"], linewidth=LINEWIDTH)
plt.xlabel(XLAB)
plt.ylabel(YLAB)
plt.grid()
plt.locator_params(axis="x", integer=True)
# plt.tight_layout()
# plt.savefig(FILE + ".png")
code = tikzplotlib.get_tikz_code(axis_width="10cm", axis_height="7cm")
with open(FILE + ".tikz", "w") as f:
    for l in code.split('\n'):
        f.write(l + '\n')
        if beginaxis.search(l) is not None:
            f.write("xtick distance=1,\n")
plt.close()
